import { AccessDeniedComponent } from './403/access-denied/access-denied.component';
import { NotFoundComponent } from './404/not-found/not-found.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./modules/layout/layout.module').then((m) => m.LayoutModule),
    pathMatch: 'prefix',
  },
  {
    path: 'auth',
    loadChildren: () => import('@auth/auth.module').then((m) => m.AuthModule),
  },

  {
    path: 'reset-password/:id',
    loadChildren: () => import('@auth/auth.module').then((m) => m.AuthModule),
  },

  // No content
  { path: 'access-denied', component: AccessDeniedComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

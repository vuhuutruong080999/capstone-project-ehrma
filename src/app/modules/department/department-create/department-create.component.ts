import { ApiUrl } from './../../shared/services/api-url/api-url';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-department-create',
  templateUrl: './department-create.component.html',
  styleUrls: ['./department-create.component.scss'],
})
export class DepartmentCreateComponent implements OnInit {
  @Input() id: string;
  @Input() data: any;
  isLoading = false;
  formGroup: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    public modalController: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.formGroup = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.maxLength(500)]],
      status: 1,
    });
    if (this.data) {
      this.id = this.data.id;
      this.formGroup.patchValue(this.data);
    }
    setTimeout(() => {}, 100);
  }

  get name() {
    return this.formGroup.get('name');
  }

  get description() {
    return this.formGroup.get('description');
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      return;
    }
    if (this.id) {
      //update
      this.clientState.isBusy = true;
      const payload = {
        id: this.id,
        ...this.formGroup.value,
      };
      this._reusableService
        .onUpdate(ApiUrl.departmentCreate, payload)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalController.dismiss({
              reload: true,
              response: res.content,
            });
            this._reusableService.onHandleSuccess(
              'Cập nhật phòng ban thành công'
            );
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    } else {
      this.clientState.isBusy = true;
      this._reusableService
        .onCreate(ApiUrl.departmentCreate, this.formGroup.value)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalController.dismiss(true);
            this._reusableService.onHandleSuccess(
              'Tạo mới phòng ban thành công'
            );
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalController.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalController.dismiss();
  }
}

import { DepartmentSelectUserComponent } from './department-select-user/department-select-user.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentDetailComponent } from './department-detail/department-detail.component';
import { DepartmentCreateComponent } from './department-create/department-create.component';
import { DepartmentMemberAddComponent } from './department-member-add/department-member-add.component';

@NgModule({
  declarations: [
    DepartmentListComponent,
    DepartmentDetailComponent,
    DepartmentCreateComponent,
    DepartmentMemberAddComponent,
    DepartmentSelectUserComponent,
  ],
  imports: [DepartmentRoutingModule, SharedModule],
})
export class DepartmentModule {}

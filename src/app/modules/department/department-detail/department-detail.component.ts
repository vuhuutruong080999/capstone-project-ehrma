import { DepartmentSelectUserComponent } from '@department/department-select-user/department-select-user.component';
import { ModalUserPreviewComponent } from './../../shared/components/modal-user-preview/modal-user-preview.component';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentCreateComponent } from '@department/department-create/department-create.component';
import {
  AlertController,
  ModalController,
  IonInfiniteScroll,
  NavController,
} from '@ionic/angular';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Configs } from '../../shared/common/configs/configs';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';
import { DepartmentMemberAddComponent } from '@department/department-member-add/department-member-add.component';

@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.scss'],
})
export class DepartmentDetailComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  model: any = {};
  labels = ['Tên phòng ban', 'Trạng thái'];
  id: string;
  deleteUrl = ApiUrl.departmentDelete;
  //User
  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên nhân viên' },
    { id: 'phone', name: 'Số điện thoại' },
    { id: 'email', name: 'Email' },
  ];
  listData: any;
  showColumns: string[];
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isManageDepartment: boolean = JwtTokenHelper.isManage(
    Configs.ManageDepartmentFeatureId
  );

  constructor(
    private _fb: FormBuilder,
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private modalCtrl: ModalController,
    private _navCtrl: NavController,
    private alertCtrl: AlertController,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });

    this.paginator._intl.itemsPerPageLabel = '';

    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
    } else {
      this.clientState.isBusy = true;

      this.onViewDetail();
    }
    this.createSearchForm();
    this.getUserInDepartment();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
      departmentId: this.id,
    });
  }

  ngAfterViewInit(): void {
    this._cd.detectChanges();
  }

  onViewDetail() {
    this._reusableService
      .onGetById(ApiUrl.departmentGetById, this.id)
      .subscribe(
        (res) => {
          this.model = res.content;
          this.clientState.isBusy = false;
        },

        (error) => {
          this.clientState.isBusy = false;
          this._router.navigate(['/department/list']);
        }
      );
  }
  getUserInDepartment(event?) {
    if (event) {
      event.target.complete();
    }

    Object.assign(this.searchForm.value, this.filter);
    this._reusableService
      .onSearch(ApiUrl.staffSearch, this.searchForm.value)
      .subscribe(
        (res) => {
          const { ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );
          if (window.innerWidth < Configs.MobileWidth) {
            if (this.filter.PageIndex == 1) {
              this.dataSource.data = res.content.users;
            } else {
              this.dataSource.data = this.dataSource.data.concat(
                res.content.users
              );
            }
          } else {
            this.dataSource.data = res.content.users;
          }
          this.pagingModel = paging;

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
          this._router.navigate(['/department/list']);
        }
      );
  }

  async onUpdate() {
    const modal = await this.modalCtrl.create({
      component: DepartmentCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-department-update',
      componentProps: {
        data: this.model,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.onViewDetail();
      }
    });
    return await modal.present();
  }

  async onDelete() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa phòng ban này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.departmentDelete, this.id)
              .subscribe(
                (res) => {
                  this.clientState.isBusy = false;
                  this._navCtrl.navigateBack('/department/list');
                  this._reusableService.onHandleSuccess(
                    'Xoá phòng ban thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  async addMember() {
    const modal = await this.modalCtrl.create({
      component: DepartmentSelectUserComponent,
      backdropDismiss: false,
      cssClass: 'modal-select-user',
      componentProps: {
        id: this.id,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.getUserInDepartment();
      }
    });
    return await modal.present();
  }
  //User

  onFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.filter.PageIndex = 1;
    this.getUserInDepartment();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  onViewDetailUser(element: any) {
    this._router.navigate(['/staff/detail/' + element.id]);
  }

  pageChange(event: any) {
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.getUserInDepartment();
  }

  onBack() {
    this._navCtrl.navigateBack('/department/list', { animated: false });
  }

  async onPresentPreviewUser(id: string) {
    if (!this.isManageDepartment) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ModalUserPreviewComponent,
      cssClass: 'modal-preview-user',
      componentProps: {
        id: id,
      },
    });

    return await modal.present();
  }

  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.getUserInDepartment(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

import { startWith, debounceTime, switchMap, map } from 'rxjs/operators';
import { ApiUrl } from 'src/app/modules/shared/services/api-url/api-url';
import { ClientState } from 'src/app/modules/shared/services/client/client-state';
import { ReusableService } from 'src/app/modules/shared/services/api/reusable.service';
import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  ModalController,
  AlertController,
  IonSearchbar,
  IonInfiniteScroll,
} from '@ionic/angular';
import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-department-select-user',
  templateUrl: './department-select-user.component.html',
  styleUrls: ['./department-select-user.component.scss'],
})
export class DepartmentSelectUserComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @ViewChild('infinityScroll') infiniteScroll: IonInfiniteScroll;

  @Input() id: string;
  @Input() data: any;

  createForm: FormGroup;
  filter: any = {};
  maxiumPage: number;
  filterRole: any;
  userList = [];
  selectedList = [];
  cloneSelectedList = [];
  roleList = [];
  maxiumRolePage: number;
  isSelectAll = false;
  selectedAllList = [];

  constructor(
    private _fb: FormBuilder,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.createSearchForm();
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;

    // this.selectedList = this.data || [];
    // this.cloneSelectedList = this.selectedList.slice();

    this.name.valueChanges
      .pipe(
        startWith(''),
        debounceTime(300),
        switchMap((value) =>
          this._reusableService
            .onSearch(ApiUrl.staffSearch, {
              PageIndex: 1,
              PageSize: 10,
              TextSearch: this.name.value,
            })
            .pipe(
              map((value) => {
                this.isSelectAll = false;
                this.infiniteScroll.disabled = false;
                this.maxiumPage = Math.ceil(
                  value.content.totalRecord / Configs.DefaultPageSize
                );
                return value.content.users;
              })
            )
        )
      )
      .subscribe((user) => {
        this.userList = user.filter((user) => {
          return user.department.id != this.id;
        });
      });
    this.onSearchRole();
  }

  createSearchForm() {
    this.createForm = this._fb.group({
      TextSearch: [''],
      RoleId: [''],
    });
  }

  get name() {
    return this.createForm.get('TextSearch');
  }

  get role() {
    return this.createForm.get('RoleId');
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService.onSearch(ApiUrl.staffSearch, this.filter).subscribe(
      (res) => {
        let { users, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );

        users = users.filter((user) => {
          return user.department.id != this.id;
        });

        if (this.filter.PageIndex == 1) {
          this.userList = users;
        } else {
          this.userList = this.userList.concat(users);
        }

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onSearchRole(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService.onSearch(ApiUrl.roleSearch, this.filter).subscribe(
      (res) => {
        let { roles, ...paging } = res.content;
        this.maxiumRolePage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.roleList = roles;
          } else {
            this.roleList = this.roleList.concat(roles);
          }
        } else {
          this.roleList = roles;
        }
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onSave() {
    if (!this.selectedList.length) {
      return;
    }
    this.clientState.isBusy = true;

    const payload = this.selectedList.map((item) => {
      return { userId: item.id, departmentId: this.id };
    });

    this._reusableService
      .onUpdate(ApiUrl.departmentAddMember, payload)
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss(true);
          this._reusableService.onHandleSuccess(
            'Thêm mới nhân viên thành công'
          );
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onFilter(event?: any) {
    this.filter.PageIndex = 1;
    this.clientState.isBusy = true;
    Object.assign(this.filter, this.createForm.value);
    this.onSearch();
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có muốn lưu thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.modalCtrl.dismiss({
                save: true,
                selectedList: this.selectedList,
              });
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onSearchUser(event: any) {
    this.filter.PageIndex = 1;
    this.filter.textSearch = event.detail.value;
    // this.onSearch();
  }

  onSelectAll() {
    this.isSelectAll = !this.isSelectAll;
    this.selectedAllList = this.userList;
    if (this.isSelectAll) {
      this.selectedAllList.forEach((user) => {
        if (!this.selectedList.includes(user)) {
          this.selectedList.push(user);
        }
      });
    } else {
      this.selectedList = this.selectedList.filter((selected) => {
        return !this.selectedAllList.some(
          (selectedAll) => selectedAll.id == selected.id
        );
      });
      this.selectedAllList = [];
    }
    this.cloneSelectedList = this.selectedList.slice();
  }

  onSelected(user: any) {
    let foundIndex = this.selectedList.findIndex(
      (selected) => selected.id == user.id
    );
    if (foundIndex == -1) {
      this.selectedList.push(user);
    } else {
      this.selectedList.splice(foundIndex, 1);
    }
    this.cloneSelectedList = this.selectedList.slice();
  }

  onLoadMoreRole() {
    if (
      this.filterRole.PageIndex <= this.maxiumRolePage &&
      this.roleList.length > 0
    ) {
      if (this.maxiumRolePage != 1) {
        this.filterRole.PageIndex = this.filterRole.PageIndex + 1;
        this.onSearchRole();
      }
    }
  }

  loadMoreData(event) {
    console.log('wokred herer');

    if (this.filter.PageIndex <= this.maxiumPage && this.userList.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;

        this.onSearch(event);
      }

      if (this.filter.PageIndex == this.maxiumPage) {
        console.log('worked disable');

        event.target.disabled = true;
      }
    }
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

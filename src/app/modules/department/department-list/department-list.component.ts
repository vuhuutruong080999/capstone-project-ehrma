import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { MatTableDataSource } from '@angular/material/table';
import { DepartmentCreateComponent } from './../department-create/department-create.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  AlertController,
  ModalController,
  IonInfiniteScroll,
} from '@ionic/angular';
import { Router } from '@angular/router';
import { Configs } from '../../shared/common/configs/configs';
import { MatPaginator } from '@angular/material/paginator';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.scss'],
})
export class DepartmentListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  searchForm: FormGroup;
  isMobile = false;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên Phòng ban' },
    { id: 'description', name: 'Mô tả' },
    { id: 'status', name: 'Trạng thái' },
  ];
  showColumns: string[];
  componentName = this.constructor.name;
  createForm: any;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  status = Configs.status;
  isManageDepartment: boolean = JwtTokenHelper.isManage(
    Configs.ManageDepartmentFeatureId
  );

  constructor(
    private _formBuilder: FormBuilder,
    public modalController: ModalController,
    private _router: Router,
    private _reusableSerivce: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController,
    private _reusableService: ReusableService
  ) {}

  ngOnInit() {
    this.paginator._intl.itemsPerPageLabel = '';

    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    if (this.isManageDepartment) {
      this.displayedColumns = [
        { id: 'name', name: 'Tên Phòng ban' },
        { id: 'description', name: 'Mô tả' },
        { id: 'status', name: 'Trạng thái' },
        { id: 'action', name: 'Thao tác' },
      ];
    }
    this.searchForm = this._formBuilder.group({
      textSearch: [''],
      status: [''],
    });
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }
    this.onSearch();
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = { ...this.filter, ...this.searchForm.value };
    this._reusableSerivce.onSearch(ApiUrl.departmentSearch, payload).subscribe(
      (res) => {
        let { departments, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          //mobile

          if (this.filter.PageIndex == 1) {
            this.dataSource.data = departments;
          } else {
            this.dataSource.data = this.dataSource.data.concat(departments);
          }
        } else {
          //web
          this.dataSource.data = departments;
        }
        this.pagingModel = paging;

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;

    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  async onDelete(element: any, index: number) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa phòng ban này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.departmentDelete, element.id)
              .subscribe(
                (res) => {
                  this.dataSource.data[index].status = 0;

                  this._reusableService.onHandleSuccess(
                    'Xoá phòng ban thành công'
                  );
                  this.clientState.isBusy = false;
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.filter.PageIndex = 1;
    this.paginator.pageIndex = 0;
    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }
    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;

    this.searchForm.reset();
    this.onFilter();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: DepartmentCreateComponent,
      cssClass: 'modal-department-create',
      backdropDismiss: false,
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.onSearch();
      }
    });
    return await modal.present();
  }

  goTo(href: string) {
    this._router.navigateByUrl(href);
  }

  onViewDetail(element: any) {
    if (element.status !== 0)
      this._router.navigate(['/department/detail/' + element.id], {
        state: element,
      });
  }

  async onUpdate(element: any, index: number) {
    const modal = await this.modalController.create({
      component: DepartmentCreateComponent,
      cssClass: 'modal-department-update',
      backdropDismiss: false,
      componentProps: {
        data: element,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.dataSource.data[index] = res.data.response;
        this.dataSource._updateChangeSubscription();
      }
    });

    return await modal.present();
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

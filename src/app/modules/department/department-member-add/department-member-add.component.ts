import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonSearchbar, ModalController, AlertController } from '@ionic/angular';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { DepartmentSelectUserComponent } from '@department/department-select-user/department-select-user.component';

@Component({
  selector: 'app-department-member-add',
  templateUrl: './department-member-add.component.html',
  styleUrls: ['./department-member-add.component.scss'],
})
export class DepartmentMemberAddComponent implements OnInit {
  @Input() id: string;
  @ViewChild('user', { static: true }) txtUser: ElementRef;
  @ViewChild('searchbar') searchbar: IonSearchbar;

  createForm: any;
  model: any;
  staffList = [];
  filter = new FilterNormalModel();
  maxiumPage = 0;
  staffInDepartment = [];
  selectedList = [];

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createAddForm();
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  createAddForm() {
    this.createForm = this._fb.group({
      users: ['', Validators.required],
    });
  }

  async onSelectUserModal() {
    this.users.markAsDirty();
    const modal = await this.modalCtrl.create({
      component: DepartmentSelectUserComponent,
      cssClass: 'modal-select-user',
      componentProps: {
        id: this.id,
        data: this.selectedList,
      },
      backdropDismiss: false,
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.save) {
        this.selectedList = res.data.selectedList;
        this.users.setValue(res.data.selectedList);
        this.txtUser.nativeElement.value = this.selectedList
          .map((item) => {
            return item.fullName;
          })
          .join(', ');
      }
    });
    return await modal.present();
  }

  get users() {
    return this.createForm.get('users');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;

    const payload = this.createForm.get('users').value.map((item) => {
      return { userId: item.id, departmentId: this.id };
    });

    this._reusableService
      .onUpdate(ApiUrl.departmentAddMember, payload)
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss(true);
          this._reusableService.onHandleSuccess(
            'Thêm mới nhân viên thành công'
          );
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

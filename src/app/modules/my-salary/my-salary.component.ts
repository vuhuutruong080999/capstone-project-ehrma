import { ApiUrl } from 'src/app/modules/shared/services/api-url/api-url';
import { ClientState } from 'src/app/modules/shared/services/client/client-state';
import { ReusableService } from 'src/app/modules/shared/services/api/reusable.service';
import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { JwtTokenHelper } from './../shared/common/jwt-token-helper/jwt-token-helper';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-salary',
  templateUrl: './my-salary.component.html',
  styleUrls: ['./my-salary.component.scss'],
})
export class MySalaryComponent implements OnInit {
  model: any = {};
  myId = JwtTokenHelper.userId;
  isMobile = false;
  labels = ['Lương', 'Loại lương'];
  salaryAdvance = [];
  constructor(
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
  }
  ionViewWillEnter() {
    this.onViewDetail();
  }
  doRefresh(event) {
    this.onViewDetail();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  onViewDetail() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.salary, { userId: this.myId })
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.model = res.content.salaries[0];

          this.loadSalaryAdvance();
        },
        () => {
          this.clientState.isBusy = false;
          this._router.navigate(['/payroll/list']);
        }
      );
  }

  loadSalaryAdvance() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.salary + '/advance', { userId: this.myId })
      .subscribe((res) => {
        this.clientState.isBusy;
        this.salaryAdvance = res.content.salaryAdvances;
      });
  }
}

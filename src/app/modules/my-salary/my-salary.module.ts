import { MySalaryComponent } from './my-salary.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { MySalaryRoutingModule } from './my-salary-routing.module';

@NgModule({
  declarations: [MySalaryComponent],
  imports: [SharedModule, MySalaryRoutingModule],
})
export class MySalaryModule {}

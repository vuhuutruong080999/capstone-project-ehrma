import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalaryRoutingModule } from './salary-routing.module';
import { SharedModule } from '../shared/shared.module';
import { SalaryListComponent } from './salary-list/salary-list.component';
import { SalaryDetailComponent } from './salary-detail/salary-detail.component';
import { SalaryCreateComponent } from './salary-create/salary-create.component';
import { SalaryAdvanceComponent } from './salary-advance/salary-advance.component';

@NgModule({
  declarations: [
    SalaryListComponent,
    SalaryDetailComponent,
    SalaryCreateComponent,
    SalaryAdvanceComponent,
  ],
  imports: [CommonModule, SalaryRoutingModule, SharedModule],
})
export class SalaryModule {}

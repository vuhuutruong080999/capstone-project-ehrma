import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalaryDetailComponent } from './salary-detail/salary-detail.component';
import { SalaryListComponent } from './salary-list/salary-list.component';

const routes: Routes = [
  { path: 'list', component: SalaryListComponent },
  { path: 'detail/:id', component: SalaryDetailComponent },
  { path: '', redirectTo: 'list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalaryRoutingModule {}

import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ModalController, AlertController } from '@ionic/angular';
import { ApiUrl } from 'src/app/modules/shared/services/api-url/api-url';
import { ReusableService } from 'src/app/modules/shared/services/api/reusable.service';
import { ClientState } from 'src/app/modules/shared/services/client/client-state';

@Component({
  selector: 'app-salary-advance',
  templateUrl: './salary-advance.component.html',
  styleUrls: ['./salary-advance.component.scss'],
})
export class SalaryAdvanceComponent implements OnInit {
  @Input() userId: any;
  data: any;
  updateForm: FormGroup;
  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.updateForm = this._fb.group({
      userId: this.userId,
      value: ['', Validators.required],
    });
  }

  onSubmit() {
    const payroll = {
      ...this.updateForm.value,
    };
    this.clientState.isBusy = true;
    this._reusableService
      .onCreate(ApiUrl.salary + '/advance', payroll)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.modalCtrl.dismiss(true);
        this._reusableService.onHandleSuccess(res.message);
      });
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

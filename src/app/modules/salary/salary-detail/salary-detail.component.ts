import { SalaryAdvanceComponent } from './../salary-advance/salary-advance.component';
import { SalaryCreateComponent } from '../salary-create/salary-create.component';
import { StaffSubsidyComponent } from './../../subsidy/staff-subsidy/staff-subsidy.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { Router, ActivatedRoute } from '@angular/router';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { ModalController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-salary-detail',
  templateUrl: './salary-detail.component.html',
  styleUrls: ['./salary-detail.component.scss'],
})
export class SalaryDetailComponent implements OnInit {
  id = '';
  model: any = {};
  isManagerPayroll = JwtTokenHelper.isManage(Configs.ManagePayrollFeatureId);
  isMobile = false;
  labels = ['Tên nhân viên', 'Lương'];
  myId = JwtTokenHelper.userId;
  staffName = 'Không';
  salaryAdvance = [];
  constructor(
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
  }
  ionViewWillEnter() {
    this._route.params.subscribe((params) => {
      this.id = params['id'];
      this.loadSalaryAdvance();
    });
    this.onViewDetail();
  }
  doRefresh(event) {
    this.onViewDetail();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  onViewDetail() {
    this._reusableService
      .onSearch(ApiUrl.salary, { userId: this.id })
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.model = res.content.salaries[0];
          this.loadSalaryAdvance();
        },
        () => {
          this.clientState.isBusy = false;
          this._router.navigate(['/payroll/list']);
        }
      );
  }
  loadSalaryAdvance() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.salary + '/advance', { userId: this.id })
      .subscribe((res) => {
        this.clientState.isBusy;
        this.salaryAdvance = res.content.salaryAdvances;
      });
  }
  async onUpdate() {
    const modal = await this.modalCtrl.create({
      component: SalaryCreateComponent,
      cssClass: 'modal-salary-create',
      backdropDismiss: false,
      componentProps: {
        id: this.model?.id,
        type: this.model?.type,
        oldValue: this.model?.value,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.onViewDetail();
      } else {
        this.clientState.isBusy = false;
      }
    });
    return await modal.present();
  }
  async subsidyStaff() {
    let subsidies = [];
    this.model.subsidies?.forEach((sub) => {
      subsidies.push(sub.id);
    });
    const modal = await this.modalCtrl.create({
      component: StaffSubsidyComponent,
      cssClass: 'modal-subsidy-staff',
      backdropDismiss: false,
      componentProps: {
        id: this.model.id,
        subsidy: subsidies,
      },
    });
    this.clientState.isBusy = true;
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.onViewDetail();
      } else {
        this.clientState.isBusy = false;
      }
    });
    return await modal.present();
  }
  onBack() {
    this._router.navigateByUrl('/salary/list');
  }
  async deleteSubsidy() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa hết trợ cấp của nhân viên này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onCreate(ApiUrl.subsidy + '/add-user-subsidies', {
                userId: this.model.id,
                subsidies: [],
              })
              .subscribe((res) => {
                this.clientState.isBusy = false;
                this.model.subsidies = [];
                this._reusableService.onHandleSuccess(
                  'Xoá hết trợ cấp thành công'
                );
              });
          },
        },
      ],
    });
    return await alert.present();
  }
  async onSalaryAdvance() {
    const modal = await this.modalCtrl.create({
      component: SalaryAdvanceComponent,
      backdropDismiss: false,
      cssClass: 'modal-4',
      componentProps: {
        userId: this.model.userId,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        this.loadSalaryAdvance();
      }
    });
    return await modal.present();
  }
}

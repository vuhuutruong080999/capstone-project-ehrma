import { ApiUrl } from '../../shared/services/api-url/api-url';
import {
  FormBuilder,
  Validators,
  FormGroup,
  AbstractControl,
} from '@angular/forms';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController, AlertController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { Configs } from '../../shared/common/configs/configs';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';

@Component({
  selector: 'app-salary-create',
  templateUrl: './salary-create.component.html',
  styleUrls: ['./salary-create.component.scss'],
})
export class SalaryCreateComponent implements OnInit {
  @Input() id: any;
  @Input() type: number;
  @Input() oldValue: number;
  @ViewChild('searchbar') searchbar: IonSearchbar;
  createForm: FormGroup;
  departmentUserList = [];
  filterUser = new FilterNormalModel();
  pagingModel = new PagingModel();
  maxiumPage = 0;
  pageSizeListUser = Configs.PageSizeList;
  pagingModelUser = new PagingModel();
  maxiumPageUser = 0;
  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createForm = this._fb.group({
      userId: [this.id, Validators.required],
      value: [this.oldValue, [Validators.required, this.lessThanZero]],
      type: this.type ? this.type : 1,
    });
    if (this.id) {
      this.createForm.get('userId').disable();
    } else {
      this.createForm.get('userId').enable();
    }
    this.loadUser();
  }

  get value() {
    return this.createForm.get('value');
  }
  get userId() {
    return this.createForm.get('userId');
  }

  get typeControl() {
    return this.createForm.get('type');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;
    const payload = {
      userId: this.createForm.get('userId').value,
      ...this.createForm.value,
    };

    this._reusableService.onCreate(ApiUrl.salary, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.modalCtrl.dismiss(true);
        this._reusableService.onHandleSuccess('Thêm mới mức lương thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  loadUser(event?) {
    this.clientState.isBusy = true;
    const payroll = {
      textSearch: event?.detail.value ? event.detail.value : null,
    };
    this._reusableService
      .onSearch(ApiUrl.departmentUserSearch, payroll)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        const { departments, ...paging } = res.content;
        this.maxiumPageUser = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.filterUser.PageIndex == 1) {
          this.departmentUserList = departments;
        } else {
          this.departmentUserList = this.departmentUserList.concat(departments);
        }
      });
  }
  onResetFilterUser() {
    if (!this.departmentUserList.length) {
      this.searchbar.value = '';
      this.filterUser.PageIndex = 1;
      this.filterUser.TextSearch = '';
      this.loadUser();
    }
  }
  onSearchUser(event: any) {
    this.filterUser.PageIndex = 1;
    this.filterUser.TextSearch = event.detail.value;
    this.loadUser();
  }
  onLoadMoreDataUser() {
    if (
      this.filterUser.PageIndex <= this.maxiumPage &&
      this.departmentUserList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filterUser.PageIndex = this.filterUser.PageIndex + 1;
        this.loadUser();
      }
    }
  }

  lessThanZero(control: AbstractControl) {
    if (isNaN(+control.value) || (control.value && control.value <= 0)) {
      return { lessThanZero: true };
    }
    return null;
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

import { SalaryCreateComponent } from '../salary-create/salary-create.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { JwtTokenHelper } from '../../shared/common/jwt-token-helper/jwt-token-helper';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-salary-list',
  templateUrl: './salary-list.component.html',
  styleUrls: ['./salary-list.component.scss'],
})
export class SalaryListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('MatPaginator') set paginator(paging: MatPaginator) {
    if (paging) {
      paging._intl.itemsPerPageLabel = '';
    }
  }
  isMobile = false;
  roleLevel = JwtTokenHelper.level;
  displayedColumns: any[] = [
    { id: 'staff', name: 'Nhân viên' },
    { id: 'value', name: 'Lương' },
    { id: 'type', name: 'Loại lương' },
    { id: 'action', name: 'Thao tác' },
  ];
  showColumns: string[] = [];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage = 0;
  isManagerPayroll = JwtTokenHelper.isManage(Configs.ManagePayrollFeatureId);
  transformCurrency = Configs.TransformCurrency;

  constructor(
    public modalController: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
  }
  ionViewWillEnter() {
    this.onSearch();
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.paginator.pageIndex = 0;
    this.filter.PageIndex = 1;
    this.onSearch();
  }
  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = {
      ...this.filter,
    };
    this._reusableService.onSearch(ApiUrl.salary, payload).subscribe(
      (res) => {
        const { salaries, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          //mobile

          if (this.filter.PageIndex == 1) {
            this.dataSource.data = salaries;
          } else {
            this.dataSource.data = this.dataSource.data.concat(salaries);
          }
        } else {
          //web
          this.dataSource.data = salaries;
        }
        this.pagingModel = paging;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onViewDetail(element: any) {
    this._router.navigate(['/salary/detail/' + element.id]);
  }

  async onUpdate(element: any) {
    this.clientState.isBusy = true;
    const modal = await this.modalController.create({
      component: SalaryCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-payroll-list-create',
      componentProps: {
        id: element.id,
        oldValue: element.salary.value,
        type: element.salary.type,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.onSearch();
      }
    });
    return await modal.present();
  }

  async onCreate() {
    const modal = await this.modalController.create({
      component: SalaryCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-payroll-list-create',
    });
    this.clientState.isBusy = true;
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.onSearch();
      }
    });
    return await modal.present();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;

    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

import { RequestPendingDetailComponent } from './request-pending-detail/request-pending-detail.component';
import { RequestPendingListComponent } from './request-pending-list/request-pending-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'list' },
  { path: 'list', component: RequestPendingListComponent },
  { path: 'detail/:id', component: RequestPendingDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestPendingRoutingModule {}

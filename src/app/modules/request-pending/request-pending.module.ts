import { RequestProcessComponent } from './request-process/request-process.component';
import { RequestPendingDetailComponent } from './request-pending-detail/request-pending-detail.component';
import { RequestPendingListComponent } from './request-pending-list/request-pending-list.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { RequestPendingRoutingModule } from './request-pending-routing.module';

@NgModule({
  declarations: [
    RequestPendingListComponent,
    RequestPendingDetailComponent,
    RequestProcessComponent,
  ],
  imports: [SharedModule, RequestPendingRoutingModule],
})
export class RequestPendingModule {}

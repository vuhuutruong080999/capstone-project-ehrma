import { ModalController, NavController } from '@ionic/angular';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { RequestProcessComponent } from './../request-process/request-process.component';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-request-pending-detail',
  templateUrl: './request-pending-detail.component.html',
  styleUrls: ['./request-pending-detail.component.scss'],
})
export class RequestPendingDetailComponent implements OnInit {
  model: any = {};
  isMobile = false;
  labels = ['Người yêu cầu', 'Trạng thái', 'Tiêu đề', 'Loại đơn', 'Ngày gửi'];
  id: string;

  isManageRequest = JwtTokenHelper.isManage(Configs.ManageRequestFeatureId);
  userId = JwtTokenHelper.userId;

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private modalCtrl: ModalController,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
      this.id = this.model.id;
    } else {
      this.clientState.isBusy = true;
      this._route.params.subscribe((params) => {
        this.id = params['id'];
      });
      this.onViewDetail();
    }
  }

  doRefresh(event) {
    this.onViewDetail();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  onViewDetail() {
    this._reusableService.onGetById(ApiUrl.requestGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/role/list']);
      }
    );
  }

  async onAcceptedRequest() {
    const modal = await this.modalCtrl.create({
      component: RequestProcessComponent,
      backdropDismiss: false,
      cssClass: 'modal-request-process',
      componentProps: {
        status: 2,
        id: this.id,
      },
    });
    modal.onDidDismiss().then(() => {
      this.clientState.isBusy = true;
      this.onViewDetail();
    });
    return await modal.present();
  }

  async onDeniedRequest() {
    const modal = await this.modalCtrl.create({
      component: RequestProcessComponent,
      backdropDismiss: false,
      cssClass: 'modal-request-process',
      componentProps: {
        status: 0,
        id: this.id,
      },
    });
    modal.onDidDismiss().then(() => {
      this.clientState.isBusy = true;
      this.onViewDetail();
    });
    return await modal.present();
  }

  onBack() {
    this._navCtrl.navigateBack('/request-pending/list', { animated: false });
  }
}

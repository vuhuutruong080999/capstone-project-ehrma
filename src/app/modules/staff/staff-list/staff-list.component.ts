import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { StaffUpdateComponent } from './../staff-update/staff-update.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { Router } from '@angular/router';
import { StaffCreateComponent } from './../staff-create/staff-create.component';
import {
  ModalController,
  AlertController,
  IonInfiniteScroll,
} from '@ionic/angular';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.scss'],
})
export class StaffListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên nhân viên' },
    { id: 'phone', name: 'Số điện thoại' },
    { id: 'email', name: 'Email' },
    { id: 'department', name: 'Phòng ban' },
    { id: 'status', name: 'Trạng thái' },
  ];
  listData: any;
  isMobile = false;
  showColumns: string[];
  roleList = [];
  departmentList = [];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  statusList = Configs.status;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _router: Router,
    public clientState: ClientState,
    private alertCtrl: AlertController,
    private _reusableService: ReusableService
  ) {}

  isManageStaff = JwtTokenHelper.isManage(Configs.ManageStaffFeatureId);

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    if (this.isManageStaff) {
      this.displayedColumns = [
        { id: 'name', name: 'Tên nhân viên' },
        { id: 'phone', name: 'Số điện thoại' },
        { id: 'email', name: 'Email' },
        { id: 'department', name: 'Phòng ban' },
        { id: 'status', name: 'Trạng thái' },
        { id: 'action', name: 'Thao tác' },
      ];
    }
    this.paginator._intl.itemsPerPageLabel = '';
    this.createSearchForm();
    this.loadFilter();
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }

    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
      Status: [''],
      departmentId: [''],
      roleId: [''],
    });
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    this.clientState.isBusy = true;
    this._reusableService.onSearch(ApiUrl.staffSearch, this.filter).subscribe(
      (res) => {
        let { users, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.dataSource.data = users;
          } else {
            this.dataSource.data = this.dataSource.data.concat(users);
          }
        } else {
          this.dataSource.data = users;
        }
        this.pagingModel = paging;

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  loadFilter() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.departmentSearch, { textSearch: '' })
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.departmentList = res.content.departments;
      });
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.roleSearch, { textSearch: '' })
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.roleList = res.content.roles;
      });
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.filter.PageIndex = 1;
    this.paginator.pageIndex = 0;

    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: StaffCreateComponent,
      cssClass: 'modal-staff-create',
      backdropDismiss: false,
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.onSearch();
      }
    });
    return await modal.present();
  }

  async onUpdate(element: any) {
    const modal = await this.modalCtrl.create({
      component: StaffUpdateComponent,
      cssClass: 'modal-staff-update',
      backdropDismiss: false,

      componentProps: {
        id: element.id,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        this.onSearch();
      }
    });

    return await modal.present();
  }

  onViewDetail(element: any) {
    if (this.isManageStaff) {
      this._router.navigate(['/staff/detail/' + element.id]);
    }
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;

    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  async onDelete(element: any) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa nhân viên này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.staffDelete, element.id)
              .subscribe(
                (res) => {
                  setTimeout(() => {
                    this.onSearch();
                  }, 500);
                  this._reusableService.onHandleSuccess(
                    'Xoá nhân viên thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

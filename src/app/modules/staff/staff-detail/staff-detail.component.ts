import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { StaffUpdateComponent } from './../staff-update/staff-update.component';
import { ClientState } from './../../shared/services/client/client-state';
import {
  ModalController,
  NavController,
  AlertController,
} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { getDataURLFromFile } from '../../layout/utils';
import { ChangePasswordComponent } from '@staff/change-password/change-password.component';
import { UserService } from '../../shared/services/api/user.service';
import { Configs } from '../../shared/common/configs/configs';

const acceptedImageTypes = ['image/png', 'image/jpeg'];
@Component({
  selector: 'app-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.scss'],
})
export class StaffDetailComponent implements OnInit {
  @ViewChild('imageInput') imageInput: ElementRef<HTMLInputElement>;
  avatarURL: string | ArrayBuffer;
  icon: File;
  model: any = {};
  id: any;
  deleteUrl = ApiUrl.staffDelete;
  isMobile = false;
  roleLevel = JwtTokenHelper.level;

  isManagerStaff = JwtTokenHelper.isManage(Configs.ManageStaffFeatureId);
  isManagerPayroll = JwtTokenHelper.isManage(Configs.ManagePayrollFeatureId);
  myId = JwtTokenHelper.userId;
  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    private _userService: UserService,
    public clientState: ClientState,
    private _router: Router,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });
    this.onViewDetail();
  }

  doRefresh(event) {
    this.onViewDetail();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  onViewDetail() {
    this.clientState.isBusy = true;
    this._reusableService.onGetById(ApiUrl.staffGetById, this.id).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.model = res.content;
        this.avatarURL = this.model.avatar;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/staff/list']);
      }
    );
  }
  async onUpdate() {
    const modal = await this.modalCtrl.create({
      component: StaffUpdateComponent,
      backdropDismiss: false,
      cssClass: 'modal-7',
      componentProps: {
        id: this.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) this.onViewDetail();
    });
    return await modal.present();
  }

  async onDelete() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa nhân viên này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.staffDelete, this.id)
              .subscribe(
                (res) => {
                  this.clientState.isBusy = false;
                  this._navCtrl.navigateBack('/staff/list');
                  this._reusableService.onHandleSuccess(
                    'Xoá nhân viên thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  async onChangePassword() {
    const modal = await this.modalCtrl.create({
      component: ChangePasswordComponent,
      backdropDismiss: false,
      cssClass: 'modal-change-password',
      componentProps: {
        id: this.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) this.onViewDetail();
    });
    return await modal.present();
  }

  async imageInputChange(event): Promise<void> {
    this.clientState._isBusy = true;
    let files: File = event.target.files;
    const file = files[0];
    if (!acceptedImageTypes.includes(file.type)) {
      return;
    }
    this.icon = file;
    const imageDataURL = await getDataURLFromFile(file);
    this.avatarURL = imageDataURL;
    this.model.avatar = this.icon;
    this.onSubmit();
  }

  onSubmit() {
    this.clientState.isBusy = true;

    const payload = {
      id: this.id,
      avatar: [this.model.avatar],
    };
    this._userService.avatarPut(ApiUrl.staffUpdate, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess(
          'Cập nhật ảnh đại diện thành công'
        );
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onBack() {
    this._navCtrl.navigateBack('/staff/list', { animated: false });
  }
}

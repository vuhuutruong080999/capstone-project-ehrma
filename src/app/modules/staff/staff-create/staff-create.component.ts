import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { DateAdapter } from 'angular-calendar';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import {
  FormBuilder,
  Validators,
  AbstractControl,
  FormArray,
} from '@angular/forms';
import { ModalController, IonSearchbar, AlertController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';

@Component({
  selector: 'app-staff-create',
  templateUrl: './staff-create.component.html',
  styleUrls: ['./staff-create.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class StaffCreateComponent implements OnInit {
  @ViewChild('departmentSearch') departmentSearch: IonSearchbar;
  @ViewChild('roleSearch') roleSearch: IonSearchbar;
  isLoading = false;
  createForm: any;
  model: any;
  genderList = Configs.GENDERS;
  roleList = [];
  departmentList = [];
  visibleConfirmPassword = false;
  visiblePassword = false;
  departmentFilter: any = {};
  roleFilter: any = {};
  maxiumPage = 0;
  maxiumPageRole = 0;

  defaultRole = '611d202db21b0b4eb787bb00';
  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createAddForm();
    this.departmentFilter.PageIndex = 1;
    this.departmentFilter.PageSize = Configs.DefaultPageSize;

    this.roleFilter.PageIndex = 1;
    this.roleFilter.PageSize = Configs.DefaultPageSize;
    this.loadDepartment();
    this.loadRole();
  }

  createAddForm() {
    this.createForm = this._fb.group({
      fullName: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50),
        ],
      ],
      doB: ['', this.checkYearOld],
      gender: [0, [Validators.required]],
      identityNumber: ['', [Validators.minLength(9), Validators.maxLength(12)]],
      phone: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(15),
        ],
      ],
      roleId: ['', [Validators.required]],
      departmentId: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.maxLength(255)]],
      address: ['', [Validators.minLength(5), Validators.maxLength(500)]],
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50),
          this.noWhitespaceValidator,
        ],
      ],
      advenInfo: this._fb.array([]),
    });
  }

  get fullName() {
    return this.createForm.get('fullName');
  }
  get doB() {
    return this.createForm.get('doB');
  }
  get gender() {
    return this.createForm.get('gender');
  }
  get identityNumber() {
    return this.createForm.get('identityNumber');
  }
  get phone() {
    return this.createForm.get('phone');
  }
  get roleId() {
    return this.createForm.get('roleId');
  }
  get department() {
    return this.createForm.get('departmentId');
  }
  get email() {
    return this.createForm.get('email');
  }
  get address() {
    return this.createForm.get('address');
  }
  get username() {
    return this.createForm.get('username');
  }
  get advanceInfo() {
    return this.createForm.get('advenInfo');
  }

  displayFnRole(role: any) {
    return role && role.name ? role.name : '';
  }

  displayFnDepartment(department: any) {
    return department && department.name ? department.name : '';
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;

    const payload = {
      ...this.createForm.value,
      roleId: this.roleId.value,
      departmentId: this.department.value,
    };

    if (this.createForm.get('doB').value) {
      const doB = moment(this.createForm.get('doB').value).format('yyyy-MM-DD');
      this.createForm.get('doB').setValue(doB);
    } else {
      delete payload.doB;
    }

    this._reusableService.onCreate(ApiUrl.staffSearch, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.modalCtrl.dismiss(true);
        this._reusableService.onHandleSuccess('Tạo mới nhân viên thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }

  loadDepartment(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = { ...this.departmentFilter, status: 1 };
    this._reusableService
      .onSearch(ApiUrl.departmentSearch, payload)
      .subscribe((res) => {
        let { departments, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.departmentFilter.PageIndex == 1) {
          this.departmentList = departments;
        } else {
          this.departmentList = this.departmentList.concat(departments);
        }
      });
  }
  onLoadMoreDepartment() {
    if (
      this.departmentFilter.PageIndex <= this.maxiumPage &&
      this.departmentList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.departmentFilter.PageIndex = this.departmentFilter.PageIndex + 1;
        this.loadDepartment(event);
      }
    }
  }
  onResetFilterDepartment() {
    if (!this.departmentList.length) {
      this.departmentSearch.value = '';
      this.departmentFilter.PageIndex = 1;
      this.departmentFilter.textSearch = '';
      this.loadDepartment();
    }
  }

  onSearchDepartment(event: any) {
    this.departmentFilter.PageIndex = 1;
    this.departmentFilter.textSearch = event.detail.value;
    this.loadDepartment();
  }

  loadRole(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = { ...this.roleFilter, status: 1 };
    this._reusableService
      .onSearch(ApiUrl.roleSearch, payload)
      .subscribe((res) => {
        let { roles, ...paging } = res.content;
        this.maxiumPageRole = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );

        if (this.roleFilter.PageIndex == 1) {
          this.roleList = roles;
        } else {
          this.roleList = this.roleList.concat(roles);
        }
      });
  }
  onLoadMoreRole() {
    if (
      this.roleFilter.PageIndex <= this.maxiumPage &&
      this.roleList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.roleFilter.PageIndex = this.roleFilter.PageIndex + 1;
        this.loadRole(event);
      }
    }
  }
  onResetFilterRole() {
    if (!this.roleList.length) {
      this.roleSearch.value = '';
      this.roleFilter.PageIndex = 1;
      this.roleFilter.textSearch = '';
      this.loadRole();
    }
  }
  onSearchRole(event: any) {
    this.roleFilter.PageIndex = 1;
    this.roleFilter.textSearch = event.detail.value;
    this.loadRole();
  }
  noWhitespaceValidator(control: AbstractControl) {
    if (
      (control.value as string).startsWith(' ') ||
      (control.value as string).endsWith(' ')
    ) {
      return { whitespace: true };
    } else {
      return null;
    }
  }

  requireMatch(control: AbstractControl) {
    const selection: any = control.value;
    if (typeof selection === 'string' && selection) {
      return { incorrect: true };
    }
    return null;
  }

  getMore() {
    return this._fb.group({
      name: ['', Validators.required],
      value: ['', Validators.required],
    });
  }

  addMore() {
    const control = <FormArray>this.createForm.controls['advenInfo'];
    control.push(this.getMore());
  }

  removeRow(i: number) {
    const control = <FormArray>this.createForm.controls['advenInfo'];
    control.removeAt(i);
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  checkYearOld(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (control.value) {
      let year = new Date(control.value).getFullYear();
      let currentYear = new Date().getFullYear();
      if (currentYear - year < 18) {
        return { yearLessThan18: true };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}

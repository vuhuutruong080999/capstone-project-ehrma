import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { StaffRoutingModule } from './staff-routing.module';
import { StaffListComponent } from './staff-list/staff-list.component';
import { StaffDetailComponent } from './staff-detail/staff-detail.component';
import { StaffCreateComponent } from './staff-create/staff-create.component';
import { StaffUpdateComponent } from './staff-update/staff-update.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
  declarations: [
    StaffListComponent,
    StaffDetailComponent,
    StaffCreateComponent,
    StaffUpdateComponent,
    ChangePasswordComponent,
  ],
  imports: [StaffRoutingModule, SharedModule],
})
export class StaffModule {}

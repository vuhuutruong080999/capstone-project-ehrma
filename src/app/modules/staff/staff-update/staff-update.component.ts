import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { IonSearchbar, ModalController, AlertController } from '@ionic/angular';

import * as moment from 'moment';
import { Configs } from '../../shared/common/configs/configs';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-staff-update',
  templateUrl: './staff-update.component.html',
  styleUrls: ['./staff-update.component.scss'],
})
export class StaffUpdateComponent implements OnInit {
  @ViewChild('departmentSearch') departmentSearch: IonSearchbar;
  @ViewChild('roleSearch') roleSearch: IonSearchbar;
  @Input() id: any;
  isLoading = false;
  updateForm: any;
  model: any;
  genderList = Configs.GENDERS;
  roleList = [];
  departmentList = [];
  count: number;
  avatar: string;
  departmentFilter: any = {};
  roleFilter: any = {};
  maxiumPage = 0;
  maxiumPageRole = 0;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.departmentFilter.PageIndex = 1;
    this.departmentFilter.PageSize = Configs.DefaultPageSize;
    this.roleFilter.PageIndex = 1;
    this.roleFilter.PageSize = Configs.DefaultPageSize;
    this.createAddForm();
  }

  ionViewWillEnter() {
    if (this.id) {
      this.clientState.isBusy = true;

      this._reusableService.onGetById(ApiUrl.staffGetById, this.id).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.model = res.content;
          this.avatar = this.model.avatar;
          this.updateForm.patchValue(this.model);
          //set doB = null
          if (this.model.doB == '0001-01-01T00:00:00+07:00') {
            this.updateForm.get('doB').setValue('');
          }

          this.updateForm
            .get('departmentId')
            .setValue(this.model.department.id);

          this.updateForm.get('roleId').setValue(this.model.role.id);
          this.model.advenInfo.forEach((data) => {
            this.setAdven(data.name, data.value);
          });
          this.count = this.updateForm.get('advenInfo').value.length;
          this.loadDepartment();
          this.loadRole();
        },
        (error) => {
          this.clientState.isBusy = false;
          this.onDismissModal();
        }
      );
    }
  }

  createAddForm() {
    this.updateForm = this._fb.group({
      fullName: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50),
        ],
      ],
      doB: [''],
      gender: ['', [Validators.required]],
      identityNumber: ['', [Validators.minLength(9), Validators.maxLength(12)]],
      phone: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(15),
        ],
      ],
      roleId: ['', [Validators.required]],
      departmentId: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.maxLength(255)]],
      address: ['', [Validators.minLength(5), Validators.maxLength(500)]],
      status: '',
      advenInfo: this._fb.array([]),
    });
  }

  get fullName() {
    return this.updateForm.get('fullName');
  }
  get doB() {
    return this.updateForm.get('doB');
  }
  get gender() {
    return this.updateForm.get('gender');
  }
  get identityNumber() {
    return this.updateForm.get('identityNumber');
  }
  get phone() {
    return this.updateForm.get('phone');
  }
  get roleId() {
    return this.updateForm.get('roleId');
  }
  get departmentId() {
    return this.updateForm.get('departmentId');
  }
  get email() {
    return this.updateForm.get('email');
  }
  get address() {
    return this.updateForm.get('address');
  }
  get username() {
    return this.updateForm.get('username');
  }

  get passwordGroup() {
    return this.updateForm.get('passwordGroup');
  }

  get password() {
    return this.updateForm.get('passwordGroup.password');
  }
  get confirmPassword() {
    return this.updateForm.get('passwordGroup.confirmPassword');
  }

  get advanceInfo() {
    return this.updateForm.get('advenInfo');
  }

  displayFnRole(role: any) {
    return role && role.name ? role.name : '';
  }

  displayFnDepartment(department: any) {
    return department && department.name ? department.name : '';
  }

  onSubmit() {
    if (this.updateForm.invalid) {
      return;
    }

    this.clientState.isBusy = true;

    const payload = {
      id: this.id,
      roleId: this.updateForm.get('roleId').value,
      departmentId: this.updateForm.get('departmentId').value,
      ...this.updateForm.value,
      file: this.avatar,
    };

    if (this.updateForm.get('doB').value) {
      const doB = moment(this.updateForm.get('doB').value).format('yyyy-MM-DD');
      this.updateForm.get('doB').setValue(doB);
    } else {
      delete payload.doB;
    }

    this._reusableService.onUpdate(ApiUrl.staffUpdate, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.modalCtrl.dismiss(true);
        this._reusableService.onHandleSuccess('Cập nhật nhân viên thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }

  noWhitespaceValidator(control: AbstractControl) {
    if (
      (control.value as string).startsWith(' ') ||
      (control.value as string).endsWith(' ')
    ) {
      return { whitespace: true };
    } else {
      return null;
    }
  }

  // create form more
  setAdven(name: string, value: string) {
    const control = <FormArray>this.updateForm.controls['advenInfo'];
    control.push(
      this._fb.group({
        name: [name, Validators.required],
        value: [value, Validators.required],
      })
    );
  }
  getMore() {
    return this._fb.group({
      name: ['', Validators.required],
      value: ['', Validators.required],
    });
  }

  loadDepartment(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = { ...this.departmentFilter, status: 1 };
    this._reusableService
      .onSearch(ApiUrl.departmentSearch, payload)
      .subscribe((res) => {
        let { departments, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.departmentFilter.PageIndex == 1) {
          this.departmentList = departments;
        } else {
          this.departmentList = this.departmentList.concat(departments);
        }
        this.departmentList = this.departmentList.filter(
          (department) => department.id !== this.model.department.id
        );
        this.departmentList.push(this.model.department);
      });
  }
  onLoadMoreDepartment() {
    if (
      this.departmentFilter.PageIndex <= this.maxiumPage &&
      this.departmentList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.departmentFilter.PageIndex = this.departmentFilter.PageIndex + 1;
        this.loadDepartment(event);
      }
    }
  }
  onResetFilterDepartment() {
    if (!this.departmentList.length) {
      this.departmentSearch.value = '';
      this.departmentFilter.PageIndex = 1;
      this.departmentFilter.textSearch = '';
      this.loadDepartment();
    }
  }
  onSearchDepartment(event: any) {
    this.departmentFilter.PageIndex = 1;
    this.departmentFilter.textSearch = event.detail.value;
    this.loadDepartment();
  }

  loadRole(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = { ...this.roleFilter, status: 1 };
    this._reusableService
      .onSearch(ApiUrl.roleSearch, payload)
      .subscribe((res) => {
        let { roles, ...paging } = res.content;
        this.maxiumPageRole = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.roleFilter.PageIndex == 1) {
          this.roleList = roles;
        } else {
          this.roleList = this.roleList.concat(roles);
        }
        this.roleList = this.roleList.filter(
          (role) => role.id !== this.model.role.id
        );
        this.roleList.push(this.model.role);
      });
  }
  onLoadMoreRole() {
    if (
      this.roleFilter.PageIndex <= this.maxiumPage &&
      this.roleList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.roleFilter.PageIndex = this.roleFilter.PageIndex + 1;
        this.loadRole(event);
      }
    }
  }
  onResetFilterRole() {
    if (!this.roleList.length) {
      this.roleSearch.value = '';
      this.roleFilter.PageIndex = 1;
      this.roleFilter.textSearch = '';
      this.loadRole();
    }
  }
  onSearchRole(event: any) {
    this.roleFilter.PageIndex = 1;
    this.roleFilter.textSearch = event.detail.value;
    this.loadRole();
  }
  addMore() {
    const control = <FormArray>this.updateForm.controls['advenInfo'];
    control.push(this.getMore());
  }

  removeRow(i: number) {
    const control = <FormArray>this.updateForm.controls['advenInfo'];
    control.removeAt(i);
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }
}

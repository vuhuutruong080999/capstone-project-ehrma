import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestRoutingModule } from './request-routing.module';
import { RequestListComponent } from './request-list/request-list.component';
import { RequestDetailComponent } from './request-detail/request-detail.component';
import { RequestCreateComponent } from './request-create/request-create.component';
import { RequestListTableComponent } from './request-list-table/request-list-table.component';

@NgModule({
  declarations: [
    RequestListComponent,
    RequestDetailComponent,
    RequestCreateComponent,
    RequestListTableComponent,
  ],
  imports: [CommonModule, RequestRoutingModule, SharedModule],
})
export class RequestModule {}

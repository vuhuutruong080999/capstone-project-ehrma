import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.scss'],
})
export class RequestDetailComponent implements OnInit {
  model: any = {};
  labels = [
    'Người yêu cầu',
    'Người xử lý',
    'Trạng thái',
    'Tiêu đề',
    'Loại đơn',
    'Ngày gửi',
  ];
  id: any;

  isManageRequest = JwtTokenHelper.isManage(Configs.ManageRequestFeatureId);
  userId = JwtTokenHelper.userId;

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });

    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
    } else {
      this.clientState.isBusy = true;
      this.onViewDetail();
    }
  }

  onViewDetail() {
    this._reusableService.onGetById(ApiUrl.requestGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/role/list']);
      }
    );
  }

  onBack() {
    this._navCtrl.navigateBack('/request/list', { animated: false });
  }
}

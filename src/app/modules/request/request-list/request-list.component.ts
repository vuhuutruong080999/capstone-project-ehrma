import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { RequestCreateComponent } from './../request-create/request-create.component';
import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  AfterViewChecked,
} from '@angular/core';
import { ModalController, IonInfiniteScroll } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss'],
})
export class RequestListComponent implements OnInit, AfterViewChecked {
  @ViewChild(IonInfiniteScroll, { static: false })
  infiniteScroll: IonInfiniteScroll;
  @ViewChild('MatPaginator') set paginator(paging: MatPaginator) {
    if (paging) {
      paging._intl.itemsPerPageLabel = '';
    }
  }
  // @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  segment: string = 'own';

  showColumns: string[];
  displayedColumns: any[] = [
    { id: 'title', name: 'Tiêu đề' },
    { id: 'categoryName', name: 'Thể loại' },
    { id: 'ownerName', name: 'Người xử lý' },
    { id: 'createdDate', name: 'Ngày gửi' },
    { id: 'status', name: 'Trạng thái' },
  ];
  searchForm: FormGroup;
  filter: any = {};

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  pageSizeList = Configs.PageSizeList;

  pagingPendingOwnModel = new PagingModel();
  maxiumPage: number;
  isMobile = false;

  reloadPending = false;

  pagingPendingModel = new PagingModel();
  pagingAcceptedModel = new PagingModel();
  pagingDeniedModel = new PagingModel();

  userId = JwtTokenHelper.userId;
  isAdmin = JwtTokenHelper.isAdmin;

  isManageRequest = JwtTokenHelper.isManage(Configs.ManageRequestFeatureId);

  responsePending: any;
  responseAccepted: any;
  responseDenined: any;

  totalPending: number;
  totalAccepted: number;
  totalDenined: number;

  constructor(
    public modalCtrl: ModalController,
    private _fb: FormBuilder,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    this.createSearchForm();
    this.clientState.isBusy = true;

    this.onSearchAll();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }

  ngAfterViewChecked() {
    this._cd.detectChanges();
  }

  doRefresh(event) {
    this.onSearchAll();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onSearchAll() {
    if (this.isManageRequest) {
      this.onSearchPending();
    }
    this.filter.Status = 1;

    this.onSearchPendingOwn();
    this.onSearchAccepted();
    this.onSearchDenied();
  }

  onSearchPendingOwn(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService
      .onSearch(ApiUrl.myRequestSearch, this.filter)
      .subscribe(
        (res) => {
          let { requests, ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );
          if (window.innerWidth < Configs.MobileWidth) {
            //mobile
            if (this.filter.PageIndex == 1) {
              this.dataSource.data = requests;
            } else {
              this.dataSource.data = this.dataSource.data.concat(requests);
            }
          } else {
            //web
            this.dataSource.data = requests;
          }
          this.pagingPendingOwnModel = paging;

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onSearchPending() {
    this._reusableService
      .onSearch(ApiUrl.requestSearch, { Status: 1 })
      .subscribe(
        (res) => {
          this.responsePending = res;
          this.totalPending = res.content.totalRecord;
          this.clientState.isBusy = false;
        },
        () => {
          this.clientState.isBusy = false;
        }
      );
  }

  onSearchAccepted() {
    this._reusableService
      .onSearch(ApiUrl.myRequestSearch, { Status: 2 })
      .subscribe(
        (res) => {
          this.responseAccepted = res;
          this.totalAccepted = res.content.totalRecord;
          this.clientState.isBusy = false;
        },
        () => {
          this.clientState.isBusy = false;
        }
      );
  }

  onSearchDenied() {
    this._reusableService
      .onSearch(ApiUrl.myRequestSearch, { Status: 0 })
      .subscribe(
        (res) => {
          this.responseDenined = res;
          this.totalDenined = res.content.totalRecord;
          this.clientState.isBusy = false;
        },
        () => {
          this.clientState.isBusy = false;
        }
      );
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.searchForm.reset();
    this.onFilter();
  }

  onFilter(event?: any) {
    this.filter.PageIndex = 1;
    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }

    this.onSearchPendingOwn();
  }

  onDecreaseCount() {
    this.totalPending = this.totalPending - 1;
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: RequestCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-request',
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }

        this.dataSource.data.unshift(res.data.response);
        if (this.dataSource.data.length > this.filter.PageSize) {
          this.dataSource.data.pop();
        }

        this.pagingPendingOwnModel.totalRecord =
          this.pagingPendingOwnModel.totalRecord + 1;
        this.maxiumPage = Math.ceil(
          this.pagingPendingOwnModel.totalRecord / Configs.DefaultPageSize
        );
        this.dataSource._updateChangeSubscription();
      }
    });
    return await modal.present();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearchPendingOwn();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/request/detail/' + element.id], {
      state: element,
    });
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearchPendingOwn(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

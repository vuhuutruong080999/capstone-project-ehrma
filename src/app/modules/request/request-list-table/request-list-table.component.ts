import { ApiUrl } from './../../shared/services/api-url/api-url';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-request-list-table',
  templateUrl: './request-list-table.component.html',
  styleUrls: ['./request-list-table.component.scss'],
})
export class RequestListTableComponent implements OnInit {
  @Input() data: any;
  @Input() status: number;
  @ViewChild('MatPaginator') set paginator(paging: MatPaginator) {
    if (paging) {
      paging._intl.itemsPerPageLabel = '';
    }
  }
  showColumns: string[];
  displayedColumns: any[] = [
    { id: 'title', name: 'Tiêu đề' },
    { id: 'categoryName', name: 'Loại đơn' },
    { id: 'acceptedName', name: 'Người duyệt' },
    { id: 'createdDate', name: 'Ngày gửi' },
  ];
  searchForm: FormGroup;
  filter = new FilterNormalModel();
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.filter.PageIndex = 1;
    }
    this.clientState.isBusy = true;

    this.createSearchForm();
    this.onSetData();
  }

  onSetData() {
    let { requests, ...paging } = this.data.content;
    this.maxiumPage = Math.ceil(paging.totalRecord / Configs.DefaultPageSize);
    if (window.innerWidth < Configs.MobileWidth) {
      //mobile

      if (this.filter.PageIndex == 1) {
        this.dataSource.data = requests;
      } else {
        this.dataSource.data = this.dataSource.data.concat(requests);
      }
    } else {
      //web
      this.dataSource.data = requests;
    }
    this.pagingModel = paging;
    this.clientState.isBusy = false;
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }

  doRefresh(event) {
    this.filter.PageIndex = 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService
      .onSearch(ApiUrl.requestSearch, { ...this.filter, Status: this.status })
      .subscribe(
        (res) => {
          let { requests, ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );
          if (window.innerWidth < Configs.MobileWidth) {
            //mobile

            if (this.filter.PageIndex == 1) {
              this.dataSource.data = requests;
            } else {
              this.dataSource.data = this.dataSource.data.concat(requests);
            }
          } else {
            //web
            this.dataSource.data = requests;
          }
          this.pagingModel = paging;
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onFilter(event?: any) {
    this.filter.PageIndex = 1;
    if (event) {
      this.filter.TextSearch = event.detail.value;
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.searchForm.reset();
    this.onFilter();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/request/detail/' + element.id]);
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

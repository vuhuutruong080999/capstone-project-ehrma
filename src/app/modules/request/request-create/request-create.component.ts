import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalController, IonSearchbar, AlertController } from '@ionic/angular';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-request-create',
  templateUrl: './request-create.component.html',
  styleUrls: ['./request-create.component.scss'],
})
export class RequestCreateComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @Input() id: any;

  isLoading = false;
  createForm: any;
  categoryList = [];
  userList = [];
  model: any;
  isCreate = false;
  filterCategory: any = {};
  filterUser = new FilterNormalModel();
  maxiumCategoryPage: number;
  maxiumUserPage: number;

  isManageRequest = JwtTokenHelper.isManage(Configs.ManageRequestFeatureId);

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.filterCategory.PageIndex = 1;
    this.filterCategory.PageSize = Configs.DefaultPageSize;

    this.createAddForm();
  }
  createAddForm() {
    this.createForm = this._fb.group({
      title: ['', [Validators.required, Validators.maxLength(100)]],
      categoryName: ['', [Validators.required]],
      user: ['', [Validators.required]],
      content: ['', [Validators.required, Validators.maxLength(500)]],
    });
    this.onGetCategory();
    this.onGetUsers();
  }

  onGetCategory(event?) {
    if (event) {
      event.target.complete();
    }
    this.filterCategory.type = 2;
    this._reusableService
      .onSearch(ApiUrl.categorySearch, this.filterCategory)
      .subscribe((res) => {
        let { categories, ...paging } = res.content;
        this.maxiumCategoryPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.filterCategory.PageIndex == 1) {
          this.categoryList = categories;
        } else {
          this.categoryList = this.categoryList.concat(categories);
        }
      });
  }

  onGetUsers(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService
      .onSearch(ApiUrl.staffApprover, this.filterUser)
      .subscribe((res) => {
        let { users, ...paging } = res.content;
        this.maxiumUserPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.filterUser.PageIndex == 1) {
          this.userList = users;
        } else {
          this.userList = this.userList.concat(users);
        }
      });
  }

  onSearchCategory(event: any) {
    this.filterCategory.PageIndex = 1;
    this.filterCategory.textSearch = event.detail.value;
    this.onGetCategory();
  }

  onSearchUser(event: any) {
    this.filterUser.PageIndex = 1;
    this.filterUser.TextSearch = event.detail.value;
    this.onGetUsers();
  }

  onResetFilterCategory() {
    if (!this.categoryList.length) {
      this.searchbar.value = '';
      this.filterCategory.PageIndex = 1;
      this.filterCategory.textSearch = '';
      this.onGetCategory();
    }
  }
  onResetFilterUser() {
    if (!this.categoryList.length) {
      this.searchbar.value = '';
      this.filterUser.PageIndex = 1;
      this.filterUser.TextSearch = '';
      this.onGetUsers();
    }
  }

  get title() {
    return this.createForm.get('title');
  }

  get categoryName() {
    return this.createForm.get('categoryName');
  }

  get content() {
    return this.createForm.get('content');
  }

  get user() {
    return this.createForm.get('user');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }

    this.clientState.isBusy = true;

    const payload = {
      title: this.title.value,
      content: this.content.value,
      categoryId: this.categoryName.value,
      ...this.user.value,
    };

    this._reusableService.onCreate(ApiUrl.requestCreate, payload).subscribe(
      (res) => {
        this.modalCtrl.dismiss({ reload: true, response: res.content });
        this.clientState.isBusy = false;

        this._reusableService.onHandleSuccess('Tạo mới yêu cầu thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onCreateCategory() {
    this.clientState.isBusy = true;
    const payload = {
      name: this.searchbar.value,
      type: 2,
    };
    this._reusableService.onCreate(ApiUrl.categoryCreate, payload).subscribe(
      (res) => {
        this.categoryList.unshift(res.content);

        this.categoryName.setValue(this.categoryList[0].id);
        this.searchbar.value = '';

        this._reusableService.onHandleSuccess(
          'Tạo mới thể loại yêu cầu thành công'
        );

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onLoadMoreCategory() {
    if (
      this.filterCategory.PageIndex <= this.maxiumCategoryPage &&
      this.categoryList.length > 0
    ) {
      if (this.maxiumCategoryPage != 1) {
        this.filterCategory.PageIndex = this.filterCategory.PageIndex + 1;
        this.onGetCategory(event);
      }
    }
  }

  onLoadMoreUser() {
    if (
      this.filterUser.PageIndex <= this.maxiumUserPage &&
      this.userList.length > 0
    ) {
      if (this.maxiumUserPage != 1) {
        this.filterUser.PageIndex = this.filterUser.PageIndex + 1;
        this.onGetUsers(event);
      }
    }
  }
  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

import { RequestListComponent } from './request-list/request-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequestDetailComponent } from './request-detail/request-detail.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'list' },

  { path: 'list', component: RequestListComponent },
  { path: 'detail/:id', component: RequestDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestRoutingModule {}

import { ApiUrl } from 'src/app/modules/shared/services/api-url/api-url';
import { ClientState } from 'src/app/modules/shared/services/client/client-state';
import { ReusableService } from 'src/app/modules/shared/services/api/reusable.service';
import { JwtTokenHelper } from './../shared/common/jwt-token-helper/jwt-token-helper';
import { PagingModel } from './../shared/models/api-response/api-response';
import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { FilterNormalModel } from './../shared/models/filter/filter.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-my-payroll',
  templateUrl: './my-payroll.component.html',
  styleUrls: ['./my-payroll.component.scss'],
})
export class MyPayrollComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: any[] = [
    { id: 'staff', name: 'Nhân viên' },
    { id: 'startDate', name: 'Ngày bắt đầu' },
    { id: 'endDate', name: 'Ngày kết thúc' },
    { id: 'creator', name: 'Người tạo' },
    { id: 'createdDate', name: 'Ngày tạo' },
    { id: 'status', name: 'Trạng thái' },
  ];
  showColumns: string[];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isManagerStaff = JwtTokenHelper.isManage(Configs.ManageStaffFeatureId);
  isManagerPayroll = JwtTokenHelper.isManage(Configs.ManagePayrollFeatureId);
  myId = JwtTokenHelper.userId;
  constructor(
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.paginator._intl.itemsPerPageLabel = '';
    this.onSearch();
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = {
      ...this.filter,
      userId: this.myId,
    };
    this._reusableService.onSearch(ApiUrl.payroll, payload).subscribe(
      (res) => {
        const { payrolls, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          //mobile

          if (this.filter.PageIndex == 1) {
            this.dataSource.data = payrolls;
          } else {
            this.dataSource.data = this.dataSource.data.concat(payrolls);
          }
        } else {
          //web
          this.dataSource.data = payrolls;
        }
        this.pagingModel = paging;

        this.clientState.isBusy = false;
      },
      () => {
        this.clientState.isBusy = false;
      }
    );
  }
  onViewDetailPayroll(element: any) {
    this._router.navigate(['/my-payroll/detail/' + element.id]);
  }
  pageChange(event: any) {
    this.clientState.isBusy = true;

    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }
  doRefreshPayroll(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

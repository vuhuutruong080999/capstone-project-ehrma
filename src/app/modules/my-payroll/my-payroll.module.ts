import { PayrollDetailComponent } from './payroll-detail/payroll-detail.component';
import { MyPayrollComponent } from './my-payroll.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { MyPayrollRoutingModule } from './my-payroll-routing.module';

@NgModule({
  declarations: [MyPayrollComponent, PayrollDetailComponent],
  imports: [SharedModule, MyPayrollRoutingModule],
})
export class MyPayrollModule {}

import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayrollRoutingModule } from './payroll-routing.module';
import { PayrollListComponent } from './payroll-list/payroll-list.component';
import { PayrollDetailComponent } from './payroll-detail/payroll-detail.component';
import { PayrollCreateComponent } from './payroll-create/payroll-create.component';
import { PayrollUpdateComponent } from './payroll-update/payroll-update.component';

@NgModule({
  declarations: [
    PayrollListComponent,
    PayrollDetailComponent,
    PayrollCreateComponent,
    PayrollUpdateComponent,
  ],
  imports: [PayrollRoutingModule, SharedModule],
})
export class PayrollModule {}

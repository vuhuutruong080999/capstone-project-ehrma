import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import {
  IonSearchbar,
  ModalController,
  NavController,
  AlertController,
} from '@ionic/angular';
import * as moment from 'moment';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-payroll-create',
  templateUrl: './payroll-create.component.html',
  styleUrls: ['./payroll-create.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi_VN' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class PayrollCreateComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  fulltimeStaff = [];
  parttimeStaff = [];
  staffListChange = [];
  isFulltime = this._fb.control(true);
  isLoading = false;
  createForm = this._fb.group({
    userId: ['', Validators.required],
    startDate: [
      '',
      [Validators.required, this.checkValidStartDate, this.checkValidDateNow],
    ],
    endDate: [
      '',
      [Validators.required, this.checkValidEndDate, this.checkValidDateNow],
    ],
  });
  multiple = this._fb.control(false);
  staffList = [];
  groups = [];
  filter = new FilterNormalModel();
  maxiumPage = 0;
  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    private navCtrl: NavController,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.multiple.valueChanges.subscribe(() => {
      this.createForm.get('userId').reset();
    });
    this.onGetDepartmentUser(this.isFulltime.value);
    this.createForm.get('endDate').valueChanges.subscribe((res) => {
      if (this.isFulltime.value) {
        const date = new Date(res);
        const monthChange = new Date(date).setMonth(date.getMonth() - 1);
        const dateChange = new Date(monthChange);
        const startDate = new Date(dateChange).setDate(date.getDate() + 1);

        this.createForm
          .get('startDate')
          .setValue(moment(startDate).format('yyyy-MM-DD'));
      }
    });
    this.isFulltime.valueChanges.subscribe((res) => {
      this.createForm.get('userId').reset();
      this.onGetDepartmentUser(res);
    });
  }
  onBack() {
    this.navCtrl.navigateBack('/payroll/list', { animated: false });
  }

  get userId() {
    return this.createForm.get('userId');
  }
  get startDate() {
    return this.createForm.get('startDate');
  }
  get endDate() {
    return this.createForm.get('endDate');
  }

  onResetFilter() {
    if (!this.staffList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.TextSearch = '';
      this.onGetDepartmentUser(this.isFulltime.value);
    }
  }

  onSearch(event: any) {
    this.filter.PageIndex = 1;
    this.filter.TextSearch = event.detail.value;
    this.onGetDepartmentUser(this.isFulltime.value);
  }

  checkValidStartDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate;
      const dateNow = moment(control.parent.controls['endDate'].value).format(
        'yyyy-MM-DD'
      );
      if (control.value) {
        startDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (startDate > dateNow) {
        return { startDateLessThanEndDate: true };
      }
    } else {
      return null;
    }
  }
  checkValidDateNow(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate;
      const dateNow = moment(Date.now()).format('yyyy-MM-DD');
      if (control.value) {
        startDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (startDate > dateNow) {
        return { lessThanDateNow: true };
      }
    } else {
      return null;
    }
  }
  onGetDepartmentUser(isFulltime: boolean) {
    const type = isFulltime ? 1 : 2;
    this.clientState.isBusy = true;
    const payload = { ...this.filter, SalaryType: type };
    this._reusableService
      .onSearch(ApiUrl.departmentUserSearch, payload)
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          let { departments, ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );

          if (this.filter.PageIndex == 1) {
            this.staffList = departments;
          } else {
            this.staffList = this.staffList.concat(departments);
          }
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }
  changeStaffType(res) {
    this.staffList = this.staffListChange;
    if (res) {
      this.staffList.forEach((department) => {
        department.users = department.users.filter((user) =>
          this.fulltimeStaff.includes(user.id)
        );
      });
    } else {
      this.staffList.forEach((department) => {
        department.users = department.users.filter((user) =>
          this.parttimeStaff.includes(user.id)
        );
      });
    }
  }
  onLoadMoreData() {
    if (this.filter.PageIndex <= this.maxiumPage && this.staffList.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onGetDepartmentUser(this.isFulltime.value);
      }
    }
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }

    this.clientState.isBusy = true;
    const payload = {
      startDate: moment(this.createForm.get('startDate').value).format(
        'yyyy-MM-DD'
      ),
      endDate: moment(this.createForm.get('endDate').value).format(
        'yyyy-MM-DD'
      ),
      userId: this.createForm.get('userId').value,
    };
    if (this.multiple.value) {
      this._reusableService
        .onCreate(ApiUrl.payroll + '/list', payload)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalCtrl.dismiss(true);
            this._reusableService.onHandleSuccess('Tạo mới sự kiện thành công');
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    } else {
      this._reusableService.onCreate(ApiUrl.payroll, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss(true);
          this._reusableService.onHandleSuccess('Tạo mới sự kiện thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    }
  }
  checkValidEndDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate, endDate;
      if (control.parent.controls['startDate'].value) {
        startDate = moment(control.parent.controls['startDate'].value).format(
          'yyyy-MM-DD'
        );
      }
      if (control.value) {
        endDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (endDate < startDate) {
        return { endDateLessThanStartDate: true };
      }
    } else {
      return null;
    }
  }
  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { PayrollUpdateComponent } from './../payroll-update/payroll-update.component';
import { PayrollCreateComponent } from './../payroll-create/payroll-create.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { MatTableDataSource } from '@angular/material/table';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import {
  ModalController,
  AlertController,
  IonInfiniteScroll,
  IonSearchbar,
} from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { MatPaginator } from '@angular/material/paginator';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';
import * as moment from 'moment';

@Component({
  selector: 'app-payroll-list',
  templateUrl: './payroll-list.component.html',
  styleUrls: ['./payroll-list.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi_VN' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class PayrollListComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  isMobile = false;
  roleLevel = JwtTokenHelper.level;
  searchForm = this._formBuilder.group({
    textSearch: '',
    startDateFilterValue: '',
    endDateFilterValue: '',
    statusFilter: '',
  });
  displayedColumns: any[] = [
    { id: 'staff', name: 'Nhân viên' },
    { id: 'startDate', name: 'Ngày bắt đầu' },
    { id: 'endDate', name: 'Ngày kết thúc' },
    { id: 'creator', name: 'Người tạo' },
    { id: 'createdDate', name: 'Ngày tạo' },
    { id: 'status', name: 'Trạng thái' },
    { id: 'action', name: 'Thao tác' },
  ];
  showColumns: string[] = [];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage = 0;
  statusName = Configs.PAYROLL_STATUS;
  isManagerPayroll = JwtTokenHelper.isManage(Configs.ManagePayrollFeatureId);
  constructor(
    private _formBuilder: FormBuilder,
    public modalController: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.paginator._intl.itemsPerPageLabel = '';
  }
  ionViewWillEnter() {
    this.onSearch();
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.filter.PageIndex = 1;
    this.paginator.pageIndex = 0;

    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }
    this.onSearch();
  }
  onResetFilter() {
    this.searchForm.reset();
    this.onFilter();
  }
  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    if (this.searchForm.get('startDateFilterValue').value) {
      const startDate = moment(
        this.searchForm.get('startDateFilterValue').value
      ).format('yyyy-MM-DD');
      this.searchForm.get('startDateFilterValue').setValue(startDate);
    }
    if (this.searchForm.get('endDateFilterValue').value) {
      const endDate = moment(
        this.searchForm.get('endDateFilterValue').value
      ).format('yyyy-MM-DD');
      this.searchForm.get('endDateFilterValue').setValue(endDate);
    }

    const payload = {
      ...this.filter,
      ...this.searchForm.value,
    };
    this._reusableService.onSearch(ApiUrl.payroll, payload).subscribe(
      (res) => {
        const { payrolls, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          //mobile

          if (this.filter.PageIndex == 1) {
            this.dataSource.data = payrolls;
          } else {
            this.dataSource.data = this.dataSource.data.concat(payrolls);
          }
        } else {
          //web
          this.dataSource.data = payrolls;
        }
        this.pagingModel = paging;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
  onViewDetail(element: any) {
    this._router.navigate(['/payroll/detail/' + element.id]);
  }
  async onUpdate(element: any) {
    this.clientState.isBusy = true;
    const modal = await this.modalController.create({
      component: PayrollUpdateComponent,
      cssClass: 'modal-6',
      backdropDismiss: false,
      componentProps: {
        id: element.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.onSearch();
      }
    });
    return await modal.present();
  }
  async onCreate() {
    const modal = await this.modalController.create({
      component: PayrollCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-payroll-create',
    });
    this.clientState.isBusy = true;
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.onSearch();
      }
    });
    return await modal.present();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;

    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }
  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
  async onDelete(element: any) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa phiếu lương này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.payroll, element.id)
              .subscribe(
                (res) => {
                  this.clientState.isBusy = false;
                  this._reusableService.onHandleSuccess(
                    'Xoá phiếu lương thành công'
                  );
                  this.onSearch();
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }
}

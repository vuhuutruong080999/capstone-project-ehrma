import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, OnInit, Input } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-payroll-update',
  templateUrl: './payroll-update.component.html',
  styleUrls: ['./payroll-update.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi_VN' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class PayrollUpdateComponent implements OnInit {
  @Input() id: any;
  data: any;
  updateForm: any = this._fb.group({
    addedSalaries: this._fb.array([]),
    substractedSalaries: this._fb.array([]),
  });
  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    this.loadData();
  }
  loadData() {
    this.clientState.isBusy = true;
    this._reusableService
      .onGetById(ApiUrl.payroll, this.id)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        const { payroll } = res.content;
        this.data = payroll;
        if (payroll.addedSalaries) {
          const add = Object.entries(payroll.addedSalaries);
          this.setAdd(add);
        }
        if (payroll.subtractedSalaries) {
          const sub = Object.entries(payroll.subtractedSalaries);
          this.setSub(sub);
        }
      });
  }

  setAdd(add: Array<any>) {
    const controlAdd = <FormArray>this.updateForm.controls['addedSalaries'];
    add.forEach((key) => {
      controlAdd.push(
        this._fb.group({
          name: [key[0], Validators.required],
          value: [key[1], Validators.required, this.lessThanZero],
        })
      );
    });
  }
  setSub(sub: Array<any>) {
    const controlSub = <FormArray>(
      this.updateForm.controls['substractedSalaries']
    );
    sub.forEach((key) => {
      controlSub.push(
        this._fb.group({
          name: [key[0], Validators.required],
          value: [key[1], Validators.required, this.lessThanZero],
        })
      );
    });
  }
  onSubmit() {
    let addedSalaries = {};
    let substractedSalaries = {};
    const listAdd = this.updateForm.get('addedSalaries').value;
    const listSub = this.updateForm.get('substractedSalaries').value;
    if (listAdd.length !== 0) {
      let name = [];
      let value = [];
      listAdd.forEach((add) => {
        name.push(add.name);
        value.push(add.value);
      });
      name.forEach((key, i) => {
        addedSalaries[key] = value[i];
      });
    }
    if (listSub.length !== 0) {
      let name = [];
      let value = [];
      listSub.forEach((add) => {
        name.push(add.name);
        value.push(add.value);
      });
      name.forEach((key, i) => {
        substractedSalaries[key] = value[i];
      });
    }
    const payroll = {
      id: this.id,
      addedSalaries: addedSalaries,
      substractedSalaries: substractedSalaries,
    };
    this.clientState.isBusy = true;
    this._reusableService.onUpdate(ApiUrl.payroll, payroll).subscribe((res) => {
      this.clientState.isBusy = false;

      this.modalCtrl.dismiss(true);
      this._reusableService.onHandleSuccess(res.message);
    });
  }
  onDismissModal() {
    this.modalCtrl.dismiss();
  }
  // create form more
  getMoreAddedSalaries() {
    return this._fb.group({
      name: ['', Validators.required],
      value: ['', Validators.required],
    });
  }

  addMoreAddedSalaries() {
    const control = <FormArray>this.updateForm.controls['addedSalaries'];
    control.push(this.getMoreAddedSalaries());
  }

  removeRowAddedSalaries(i: number) {
    const control = <FormArray>this.updateForm.controls['addedSalaries'];
    control.removeAt(i);
  }
  // create form more
  getMoreSubstractedSalaries() {
    return this._fb.group({
      name: ['', Validators.required],
      value: ['', Validators.required],
    });
  }

  addMoreSubstractedSalaries() {
    const control = <FormArray>this.updateForm.controls['substractedSalaries'];
    control.push(this.getMoreSubstractedSalaries());
  }

  removeRowSubstractedSalaries(i: number) {
    const control = <FormArray>this.updateForm.controls['substractedSalaries'];
    control.removeAt(i);
  }

  lessThanZero(control: AbstractControl) {
    if (isNaN(+control.value) || (control.value && control.value <= 0)) {
      return { lessThanZero: true };
    }
    return null;
  }
}

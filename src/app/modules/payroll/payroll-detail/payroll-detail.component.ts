import { PayrollService } from './../../shared/services/api/payroll.service';
import { PayrollUpdateComponent } from './../payroll-update/payroll-update.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import {
  IonInfiniteScroll,
  NavController,
  ModalController,
  AlertController,
} from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-payroll-detail',
  templateUrl: './payroll-detail.component.html',
  styleUrls: ['./payroll-detail.component.scss'],
})
export class PayrollDetailComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  model: any = {};
  roleLevel = JwtTokenHelper.level;
  labelsParttime = [
    'Tên nhân viên',
    'Lương',
    'Loại lương',
    'Có mặt',
    'Ngày bắt đầu',
    'Ngày kết thúc',
    'Người tạo',
  ];
  labelsFulltime = [
    'Tên nhân viên',
    'Lương',
    'Loại lương',
    'Có mặt',
    'Vắng mặt',
    'Ngày bắt đầu',
    'Ngày kết thúc',
    'Người tạo',
  ];
  totalSalaryAdvance = 0;
  isMobile = false;
  id: string;
  isManager = JwtTokenHelper.isManage(Configs.ManagePayrollFeatureId);
  deleteUrl = ApiUrl.payroll;
  add = [];
  subtract = [];
  transformCurrency = Configs.TransformCurrency;

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _navCtrl: NavController,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private _payrollService: PayrollService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });
    this.onViewDetail();
  }

  doRefresh(event) {
    this.onViewDetail();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  async onUpdate() {
    this.clientState.isBusy = true;
    const modal = await this.modalCtrl.create({
      component: PayrollUpdateComponent,
      cssClass: 'modal-6',
      componentProps: {
        id: this.id,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.infiniteScroll.disabled = false;
        }
        this.onViewDetail();
      }
    });
    return await modal.present();
  }
  async onDelete() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa phiếu lương này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService.onDelete(ApiUrl.payroll, this.id).subscribe(
              (res) => {
                this.clientState.isBusy = false;
                this._navCtrl.navigateBack('/payroll/list');
                this._reusableService.onHandleSuccess(
                  'Xoá phiếu lương thành công'
                );
              },
              (error) => {
                this.clientState.isBusy = false;
              }
            );
          },
        },
      ],
    });
    return await alert.present();
  }
  onViewDetail() {
    this._reusableService.onGetById(ApiUrl.payroll, this.id).subscribe(
      (res) => {
        this.model = res.content.payroll;
        this.clientState.isBusy = false;
        this.add = Object.entries(this.model.addedSalaries);
        this.subtract = Object.entries(this.model.subtractedSalaries);
        this.totalSalaryAdvance = this.model.totalSalaryAdvance;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/payroll/list']);
      }
    );
  }
  onBack() {
    if (this.roleLevel != 2) {
      this._navCtrl.navigateBack(`/staff/detail/${this.model.user?.id}`);
    } else this._navCtrl.navigateBack('/payroll/list', { animated: false });
  }
  onUpdateStatus(status: number) {
    this.clientState.isBusy = true;
    this._payrollService
      .onUpdateStatus(ApiUrl.payroll + `/${this.id}/status`, status)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.model = res.content;
        this._reusableService.onHandleSuccess(res.message);
      });
  }
}

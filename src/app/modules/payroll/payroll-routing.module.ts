import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PayrollDetailComponent } from './payroll-detail/payroll-detail.component';
import { PayrollListComponent } from './payroll-list/payroll-list.component';

const routes: Routes = [
  { path: 'list', component: PayrollListComponent },
  { path: 'detail/:id', component: PayrollDetailComponent },
  { path: '', redirectTo: 'list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayrollRoutingModule {}

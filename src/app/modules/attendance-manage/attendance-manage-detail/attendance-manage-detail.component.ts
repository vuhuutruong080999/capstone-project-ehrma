import { AttendanceManageCreateComponent } from './../attendance-manage-create/attendance-manage-create.component';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ClientState } from './../../shared/services/client/client-state';
import {
  Component,
  OnInit,
  ViewChild,
  AfterContentChecked,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-attendance-manage-detail',
  templateUrl: './attendance-manage-detail.component.html',
  styleUrls: ['./attendance-manage-detail.component.scss'],
})
export class AttendanceManageDetailComponent
  implements OnInit, AfterContentChecked, OnDestroy {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: false }) set paginator(
    content: MatPaginator
  ) {
    if (content) {
      content._intl.itemsPerPageLabel = '';
    }
  }

  displayedColumns: any[] = [{ id: 'name', name: 'Tên nhân viên' }];
  columnsToDisplay: any[] = [];
  filter: any = {};
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  searchForm: FormGroup;
  isMobile = false;
  showColumns: any[] = [];
  maxiumPage: number;
  userAttendances: any = [];
  listDate: any = [];
  isDoneInit = false;
  isFilter = false;

  constructor(
    public modalCtrl: ModalController,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _router: Router,
    public clientState: ClientState,
    private _reusableService: ReusableService,
    private _cd: ChangeDetectorRef
  ) {}

  id: string;
  eventModel: any = {};

  ngOnInit() {
    if (localStorage.getItem('attendance-manage-detail')) {
      localStorage.removeItem('attendance-manage-detail');
    }

    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    this.filter.PageIndex = 1;
    this.filter.PageSize = 100;
    this.filter.eventId = this.id;

    this.createSearchForm();

    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });
    this.clientState.isBusy = true;

    this.onGetAttendance();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }

  ngAfterContentChecked() {
    this._cd.detectChanges();
  }

  onGetAttendance(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService
      .onSearch(ApiUrl.attendanceEventGet + `/${this.id}`, this.filter)
      .subscribe(
        (res) => {
          this.eventModel.name = res.content.eventName;
          this.eventModel.startDate = res.content.startDate;
          this.eventModel.endDate = res.content.endDate;
          this.eventModel.status = res.content.status;
          this.eventModel.address = res.content.address;
          this.eventModel.processDate = res.content.processDate;
          this.eventModel.expectedEndDate = res.content.expectedEndDate;
          console.log(res.content);

          let { userAttendances, ...paging } = res.content;

          this.userAttendances = userAttendances;

          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );
          if (window.innerWidth < Configs.MobileWidth) {
            if (this.filter.PageIndex == 1) {
              this.dataSource.data = userAttendances;
            } else {
              this.dataSource.data = this.dataSource.data.concat(
                userAttendances
              );
            }
          } else {
            this.dataSource.data = userAttendances;
          }

          if (userAttendances.length && !this.isFilter) {
            this.getDateColumn();
          } else {
            this.isDoneInit = true;
          }

          this.pagingModel = {
            textSearch: res.content.textSearch,
            sortColumn: res.content.sortColumn,
            pageSize: res.content.pageSize,
            totalRecord: res.content.totalRecord,
            pageIndex: res.content.pageIndex,
          };
          console.log(this.pagingModel);

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onRefreshAttendance() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.attendanceEventGet + `/${this.id}`, this.filter)
      .subscribe(
        (res) => {
          let { userAttendances, ...paging } = res.content;
          this.userAttendances = userAttendances;

          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );

          if (window.innerWidth < Configs.MobileWidth) {
            if (this.filter.PageIndex == 1) {
              this.dataSource.data = userAttendances;
            } else {
              this.dataSource.data = this.dataSource.data.concat(
                userAttendances
              );
            }
          } else {
            this.dataSource.data = userAttendances;
          }
          this.pagingModel = paging;

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  getDateColumn() {
    for (const [key, value] of Object.entries(
      this.userAttendances[0].attendances
    )) {
      this.listDate.push({ id: key, name: key });
      this.displayedColumns.push({ id: key, name: key });
    }

    this.isDoneInit = true;
  }

  onGetValueDate(attendances: any, index: number): any {
    return Object.values(attendances)[index];
  }

  onFilter(event?: any) {
    this.isFilter = true;
    this.filter.PageIndex = 1;

    if (event) {
      this.filter.TextSearch = event.detail.value;
    } else {
      // this.clientState.isBusy = true;
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }

    this.onGetAttendance();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    // this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onGetAttendance();
  }

  loadMoreData(event) {}

  onViewDetail(row: any) {
    let isCheckIn = false;
    for (const [key, value] of Object.entries(row.attendances)) {
      if (value != 0) {
        isCheckIn = true;
        break;
      }
    }

    if (isCheckIn) {
      this._router.navigate([`/attendance-manage/user/${this.id}/${row.id}`]);
    } else {
      this._reusableService.onHandleFailed('Không tồn tại điểm danh nào');
    }
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: AttendanceManageCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-attendance',
      componentProps: {
        eventId: this.id,
        userAttendances: this.userAttendances,
        processDate: this.eventModel.processDate,
        endDate: this.eventModel.endDate,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.onRefreshAttendance();
      }
    });
    return await modal.present();
  }

  ngOnDestroy() {
    localStorage.removeItem('attendance-manage-detail');
  }
}

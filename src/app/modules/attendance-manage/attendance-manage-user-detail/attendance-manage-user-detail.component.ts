import { NavController } from '@ionic/angular';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ClientState } from './../../shared/services/client/client-state';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-attendance-manage-user-detail',
  templateUrl: './attendance-manage-user-detail.component.html',
  styleUrls: ['./attendance-manage-user-detail.component.scss'],
})
export class AttendanceManageUserDetailComponent implements OnInit {
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _navCtrl: NavController
  ) {}

  id: string = '';
  eventId: string = '';
  attendanceModel: any = [];
  isMobile = false;

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    this._route.params.subscribe((params) => {
      this.id = params['id'];
      this.eventId = params['eventId'];
    });

    this.onGetAttendance();
  }

  onGetAttendance() {
    this._reusableService
      .onSearch(ApiUrl.attendanceSearch, {
        UserId: this.id,
        EventId: this.eventId,
        SortColumn: 'CheckInDateTime',
      })
      .subscribe(
        (res) => {
          this.attendanceModel = res.content.attendances;
          console.log(this.attendanceModel);

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
          this._router.navigate([`/attendance-manage/detail/${this.eventId}`]);
        }
      );
  }

  onGetTotalTime() {
    this.attendanceModel.forEach((element) => {
      if (element.checkOut) {
        element.checkOutDateTime;
      }
    });
  }

  onBack() {
    this._navCtrl.navigateBack(`/attendance-manage/detail/${this.eventId}`, {
      animated: false,
    });
  }

  doRefresh(event) {
    this.onGetAttendance();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

import { AttendanceManageDetailComponent } from './attendance-manage-detail/attendance-manage-detail.component';
import { AttendanceManageListComponent } from './attendance-manage-list/attendance-manage-list.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttendanceManageUserDetailComponent } from './attendance-manage-user-detail/attendance-manage-user-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: AttendanceManageListComponent,
  },
  { path: 'detail/:id', component: AttendanceManageDetailComponent },
  {
    path: 'user/:eventId/:id',
    component: AttendanceManageUserDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendanceManageRoutingModule {}

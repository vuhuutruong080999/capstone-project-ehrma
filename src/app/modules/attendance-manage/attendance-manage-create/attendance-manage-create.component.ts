import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ModalController, IonSearchbar, AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { MatDatepicker } from '@angular/material/datepicker';
import * as moment from 'moment';

@Component({
  selector: 'app-attendance-manage-create',
  templateUrl: './attendance-manage-create.component.html',
  styleUrls: ['./attendance-manage-create.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi_VN' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class AttendanceManageCreateComponent implements OnInit {
  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;

  @Input() eventId: string;
  @Input() processDate: any;
  @Input() endDate: any;
  @ViewChild('searchbar') searchbar: IonSearchbar;

  checkOutList = Configs.CHECK_OUT_STATUS;
  createForm: any;
  userList = [];
  maxiumPage: number;
  filter: any = {};

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.filter.eventId = this.eventId;
    this.createAddForm();
    this.setRangeFilter();
    this.onGetUsers();
  }

  setRangeFilter() {
    this.rangeFilter = (date: Date): boolean => {
      const newDate = date || new Date();
      const newProcessDate = new Date(this.processDate);
      // newProcessDate.setHours(0, 0, 0, 0);
      const newEndDate = new Date();
      return newDate >= newProcessDate && newDate <= newEndDate;
    };
  }

  createAddForm() {
    this.createForm = this._fb.group({
      user: ['', [Validators.required]],
      checkInDateTime: ['', [Validators.required]],
      checkOut: [true, [Validators.required]],
    });
  }

  onGetUsers(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService
      .onSearch(ApiUrl.attendanceEventGet + `/${this.eventId}`, this.filter)
      .subscribe((res) => {
        let { userAttendances, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.filter.PageIndex == 1) {
          this.userList = userAttendances;
        } else {
          this.userList = this.userList.concat(userAttendances);
        }

        this.clientState.isBusy = false;
      });
  }

  onSearchUser(event: any) {
    this.filter.PageIndex = 1;
    this.filter.TextSearch = event.detail.value;
    this.onGetUsers();
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;

    let datePicker = moment(this.checkInDateTime.value).format('DD/MM/yyyy');

    let payload = {
      userId: this.user.value.id,
      eventId: this.eventId,
      checkInDateTime: moment(this.checkInDateTime.value).format('yyyy-MM-DD'),
      checkOut: true,
      checkOutDateTime: moment(this.checkInDateTime.value).format('yyyy-MM-DD'),
    };

    if (!this.checkOut.value) {
      delete payload.checkOut;
      delete payload.checkOutDateTime;
    }

    if (this.user.value.attendances[datePicker] == 0) {
      //create
      this._reusableService
        .onCreate(ApiUrl.attendanceCreate, payload)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalCtrl.dismiss({ reload: true });
            this._reusableService.onHandleSuccess(
              'Chỉnh sửa điểm danh thành công'
            );
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    } else if (this.user.value.attendances[datePicker] == 1) {
      //only check in => update checkout
      const payload = {
        userId: this.user.value.id,
        dateTime: moment(this.checkInDateTime.value).format('yyyy-MM-DD'),
      };

      this._reusableService
        .onUpdate(ApiUrl.attendanceUpdate, payload)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalCtrl.dismiss({ reload: true });
            this._reusableService.onHandleSuccess(
              'Chỉnh sửa điểm danh thành công'
            );
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    } else {
      this.modalCtrl.dismiss();
    }
  }

  get user() {
    return this.createForm.get('user');
  }

  get checkInDateTime() {
    return this.createForm.get('checkInDateTime');
  }

  get checkOut() {
    return this.createForm.get('checkOut');
  }

  onResetFilterUser() {
    if (!this.userList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.TextSearch = '';
      this.onGetUsers();
    }
  }

  onLoadMoreUser() {
    if (this.filter.PageIndex <= this.maxiumPage && this.userList.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onGetUsers(event);
      }
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  rangeFilter(date: Date): boolean {
    return true;
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

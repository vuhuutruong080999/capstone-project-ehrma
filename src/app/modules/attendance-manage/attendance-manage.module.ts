import { AttendanceManageCreateComponent } from './attendance-manage-create/attendance-manage-create.component';
import { AttendanceManageUserDetailComponent } from './attendance-manage-user-detail/attendance-manage-user-detail.component';
import { AttendanceManageDetailComponent } from './attendance-manage-detail/attendance-manage-detail.component';
import { AttendanceManageListComponent } from './attendance-manage-list/attendance-manage-list.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { AttendanceManageRoutingModule } from './attendance-manage-routing.module';

@NgModule({
  declarations: [
    AttendanceManageListComponent,
    AttendanceManageDetailComponent,
    AttendanceManageUserDetailComponent,
    AttendanceManageCreateComponent,
  ],
  imports: [SharedModule, AttendanceManageRoutingModule],
})
export class AttendanceManageModule {}

import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Router } from '@angular/router';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { MatTableDataSource } from '@angular/material/table';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  IonInfiniteScroll,
  ModalController,
  AlertController,
} from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import * as moment from 'moment';

@Component({
  selector: 'app-attendance-manage-list',
  templateUrl: './attendance-manage-list.component.html',
  styleUrls: ['./attendance-manage-list.component.scss'],
})
export class AttendanceManageListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên sự kiện' },
    { id: 'startDate', name: 'Ngày bắt đầu' },
    { id: 'endDate', name: 'Ngày kết thúc' },
    { id: 'status', name: 'Trạng thái' },
    { id: 'address', name: 'Địa điểm' },
  ];
  listData: any;

  showColumns: string[];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter: any = {};
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  status = Configs.EVENT_STATUS;
  maxiumPage: number;
  isMobile = false;

  isManageAttendance = JwtTokenHelper.isManage(
    Configs.ManageAttendanceFeatureId
  );

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    if (this.isManageAttendance) {
      this.displayedColumns = [
        { id: 'name', name: 'Tên sự kiện' },
        { id: 'startDate', name: 'Ngày bắt đầu' },
        { id: 'endDate', name: 'Ngày kết thúc' },
        { id: 'address', name: 'Địa điểm' },
        { id: 'status', name: 'Trạng thái' },
        { id: 'action', name: 'Thao tác' },
      ];
    }

    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.paginator._intl.itemsPerPageLabel = '';
    this.createSearchForm();
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }
    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
      StatusFilter: [''],
      startDateFilterValue: ['', [this.checkValidStartDate]],
      endDateFilterValue: ['', [this.checkValidEndDate]],
    });
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService.onSearch(ApiUrl.eventSearch, this.filter).subscribe(
      (res) => {
        let { events, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.dataSource.data = events;
          } else {
            this.dataSource.data = this.dataSource.data.concat(events);
          }
        } else {
          this.dataSource.data = events;
        }
        this.pagingModel = paging;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  get startDateFilterValue() {
    return this.searchForm.get('startDateFilterValue');
  }

  get endDateFilterValue() {
    return this.searchForm.get('endDateFilterValue');
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.paginator.pageIndex = 0;
    if (this.searchForm.get('startDateFilterValue').value) {
      const startDate = moment(
        this.searchForm.get('startDateFilterValue').value
      ).format('yyyy-MM-DD');
      this.searchForm.get('startDateFilterValue').setValue(startDate);
    }
    if (this.searchForm.get('endDateFilterValue').value) {
      const endDate = moment(
        this.searchForm.get('endDateFilterValue').value
      ).format('yyyy-MM-DD');
      this.searchForm.get('endDateFilterValue').setValue(endDate);
    }
    this.filter.PageIndex = 1;

    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  onUpdate(element: any) {
    this._router.navigate(['/event/update/' + element.id]);
  }

  onViewDetail(element: any) {
    if (element.status == 3 || element.status == 1) {
      this._router.navigate(['/attendance-manage/detail/' + element.id]);
    } else {
      this._reusableService.onHandleFailed('Sự kiện chưa diễn ra');
    }
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;

    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  async onDelete(element: any) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa sự kiện này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.eventDelete, element.id)
              .subscribe(
                (res) => {
                  this.onSearch();
                  this._reusableService.onHandleSuccess(
                    'Xoá sự kiện thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }
      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
  checkValidStartDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate;
      let endDate;
      if (control.parent.controls['endDateFilterValue'].value) {
        endDate = moment(
          control.parent.controls['endDateFilterValue'].value
        ).format('yyyy-MM-DD');
      }
      if (control.value) {
        startDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (startDate > endDate) {
        return { startDateLessThanEndDate: true };
      }
    } else {
      return null;
    }
  }

  checkValidEndDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate, endDate;
      if (control.parent.controls['startDateFilterValue'].value) {
        startDate = moment(
          control.parent.controls['startDateFilterValue'].value
        ).format('yyyy-MM-DD');
      }
      if (control.value) {
        endDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (endDate < startDate) {
        return { endDateLessThanStartDate: true };
      }
    } else {
      return null;
    }
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

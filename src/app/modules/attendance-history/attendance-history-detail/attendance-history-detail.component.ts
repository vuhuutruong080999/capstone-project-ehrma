import { ApiUrl } from './../../shared/services/api-url/api-url';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { NavController } from '@ionic/angular';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-attendance-history-detail',
  templateUrl: './attendance-history-detail.component.html',
  styleUrls: ['./attendance-history-detail.component.scss'],
})
export class AttendanceHistoryDetailComponent implements OnInit {
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _navCtrl: NavController
  ) {}

  id: string = '';
  eventId: string = '';
  attendanceModel: any = [];
  isMobile = false;

  userId = JwtTokenHelper.userId;

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    this._route.params.subscribe((params) => {
      this.eventId = params['id'];
    });

    this.onGetAttendance();
  }

  onGetAttendance() {
    this._reusableService
      .onSearch(ApiUrl.attendanceSearch, {
        UserId: this.id,
        EventId: this.eventId,
        SortColumn: 'CheckInDateTime',
      })
      .subscribe(
        (res) => {
          this.attendanceModel = res.content.attendances;
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
          this._router.navigate([`/attendance-manage/detail/${this.eventId}`]);
        }
      );
  }

  onBack() {
    this._navCtrl.navigateBack(`/attendance-manage/detail/${this.eventId}`, {
      animated: false,
    });
  }

  doRefresh(event) {
    this.onGetAttendance();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

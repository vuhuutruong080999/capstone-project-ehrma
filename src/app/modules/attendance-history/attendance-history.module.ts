import { SharedModule } from './../shared/shared.module';
import { AttendanceHistoryDetailComponent } from './attendance-history-detail/attendance-history-detail.component';
import { AttendanceHistoryListComponent } from './attendance-history-list/attendance-history-list.component';
import { NgModule } from '@angular/core';

import { AttendanceHistoryRoutingModule } from './attendance-history-routing.module';

@NgModule({
  declarations: [
    AttendanceHistoryListComponent,
    AttendanceHistoryDetailComponent,
  ],
  imports: [SharedModule, AttendanceHistoryRoutingModule],
})
export class AttendanceHistoryModule {}

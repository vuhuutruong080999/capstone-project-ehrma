import { AttendanceHistoryDetailComponent } from './attendance-history-detail/attendance-history-detail.component';
import { AttendanceHistoryListComponent } from './attendance-history-list/attendance-history-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: AttendanceHistoryListComponent,
  },
  {
    path: 'detail/:id',
    component: AttendanceHistoryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendanceHistoryRoutingModule {}

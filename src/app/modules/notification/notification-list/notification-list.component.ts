import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Router } from '@angular/router';
import { NotificationCreateComponent } from './../notification-create/notification-create.component';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { MatPaginator } from '@angular/material/paginator';
import {
  IonInfiniteScroll,
  ModalController,
  AlertController,
} from '@ionic/angular';
import { ClientState } from './../../shared/services/client/client-state';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss'],
})
export class NotificationListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: false }) set paginator(
    content: MatPaginator
  ) {
    if (content) {
      content._intl.itemsPerPageLabel = '';
    }
  }

  notificationList = [];
  searchForm: FormGroup;
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isMobile = false;

  isManageNotification = JwtTokenHelper.isManage(
    Configs.ManageNotificationFeatureId
  );
  constructor(
    private _fb: FormBuilder,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    public modalCtrl: ModalController,
    private _router: Router,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.createSearchForm();
    this.clientState.isBusy = true;
    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService
      .onSearch(ApiUrl.notificationSearch, this.filter)
      .subscribe(
        (res) => {
          let { notifications, ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );

          if (window.innerWidth < Configs.MobileWidth) {
            if (this.filter.PageIndex == 1) {
              this.notificationList = notifications;
            } else {
              this.notificationList = this.notificationList.concat(
                notifications
              );
            }
          } else {
            this.notificationList = notifications;
          }
          this.pagingModel = paging;

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.filter.PageIndex = 1;
    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/notification/detail/' + element.id], {
      state: element,
    });
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: NotificationCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-notification',
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.notificationList.unshift(res.data.response);

        if (this.notificationList.length > this.filter.PageSize) {
          this.notificationList.pop();
        }
        this.pagingModel.totalRecord = this.pagingModel.totalRecord + 1;
        this.maxiumPage = Math.ceil(
          this.pagingModel.totalRecord / Configs.DefaultPageSize
        );
      }
    });
    return await modal.present();
  }

  async onUpdate(element: any, index: number) {
    const modal = await this.modalCtrl.create({
      component: NotificationCreateComponent,
      cssClass: 'modal-notification',
      backdropDismiss: false,
      componentProps: {
        data: element,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.notificationList[index] = res.data.response;
      }
    });

    return await modal.present();
  }

  async onDelete(element, index: number) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa thông báo này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this._reusableService
              .onDelete(ApiUrl.notificationDelete, element.id)
              .subscribe(
                (res) => {
                  if (this.notificationList.length < this.filter.PageSize) {
                    this.notificationList.splice(index, 1);
                  } else {
                    this.clientState._isBusy = true;
                    this.onSearch();
                  }
                  this._reusableService.onHandleSuccess(
                    'Xoá thông báo thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.notificationList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { NotificationCreateComponent } from './../notification-create/notification-create.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { FormBuilder } from '@angular/forms';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ClientState } from './../../shared/services/client/client-state';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.scss'],
})
export class NotificationDetailComponent implements OnInit {
  id: any;
  model: any = {};
  deleteUrl = ApiUrl.notificationDelete;
  isMobile = false;

  isManageNotification = JwtTokenHelper.isManage(
    Configs.ManageNotificationFeatureId
  );

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private alertCtrl: AlertController,
    private _navCtrl: NavController,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });
    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
    } else {
      this.clientState.isBusy = true;
      this.onViewDetail();
    }
  }

  onViewDetail() {
    this._reusableService
      .onGetById(ApiUrl.notificationGetById, this.id)
      .subscribe(
        (res) => {
          this.model = res.content;
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
          this._router.navigate(['/notification/list']);
        }
      );
  }

  async onUpdate() {
    const modal = await this.modalCtrl.create({
      component: NotificationCreateComponent,
      cssClass: 'modal-notification',
      componentProps: {
        data: this.model,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.model = res.data.response;
      }
    });
    return await modal.present();
  }

  async onDelete() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa thông báo này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.notificationDelete, this.id)
              .subscribe(
                (res) => {
                  this.clientState.isBusy = false;
                  this._navCtrl.navigateBack('/notification/list');
                  this._reusableService.onHandleSuccess(
                    'Xoá thông báo thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  onBack() {
    this._navCtrl.navigateBack('/notification/list', { animated: false });
  }
}

import { CommonModule } from '@angular/common';
import { SharedModule } from './../shared/shared.module';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { NotificationDetailComponent } from './notification-detail/notification-detail.component';
import { NotificationCreateComponent } from './notification-create/notification-create.component';
import { NgModule } from '@angular/core';

import { NotificationRoutingModule } from './notification-routing.module';

@NgModule({
  declarations: [
    NotificationDetailComponent,
    NotificationListComponent,
    NotificationCreateComponent,
  ],
  imports: [CommonModule, NotificationRoutingModule, SharedModule],
})
export class NotificationModule {}

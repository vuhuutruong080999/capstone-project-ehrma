import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ModalController, AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-notification-create',
  templateUrl: './notification-create.component.html',
  styleUrls: ['./notification-create.component.scss'],
})
export class NotificationCreateComponent implements OnInit {
  @Input() data: any;
  isLoading = false;
  createForm: any;
  model: any;
  categoryList = [];

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createAddForm();
    if (this.data) {
      this.createForm.patchValue(this.data);
    }
  }

  createAddForm() {
    this.createForm = this._fb.group({
      title: ['', [Validators.required, Validators.maxLength(200)]],
      content: ['', [Validators.required, Validators.maxLength(1000)]],
    });
    setTimeout(() => {}, 100);
  }

  get title() {
    return this.createForm.get('title');
  }
  get content() {
    return this.createForm.get('content');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    if (this.data) {
      this.clientState.isBusy = true;

      const payload = {
        ...this.createForm.value,
        id: this.data.id,
      };
      this._reusableService
        .onUpdate(ApiUrl.notificationUpdate, payload)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalCtrl.dismiss({ reload: true, response: res.content });

            this._reusableService.onHandleSuccess(
              'Cập nhật thông báo thành công'
            );
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    } else {
      this.clientState.isBusy = true;

      this._reusableService
        .onCreate(ApiUrl.notificationCreate, this.createForm.value)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalCtrl.dismiss({ reload: true, response: res.content });

            this._reusableService.onHandleSuccess(
              'Tạo mới thông báo thành công'
            );
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

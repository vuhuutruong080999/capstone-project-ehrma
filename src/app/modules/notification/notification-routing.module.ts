import { NotificationDetailComponent } from './notification-detail/notification-detail.component';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { RoleDetailComponent } from './../role/role-detail/role-detail.component';
import { RoleListComponent } from './../role/role-list/role-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },

  {
    path: 'list',
    component: NotificationListComponent,
  },
  {
    path: 'detail/:id',
    component: NotificationDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationRoutingModule {}

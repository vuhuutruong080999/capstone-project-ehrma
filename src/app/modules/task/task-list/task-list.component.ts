import { ActivatedRoute } from '@angular/router';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { FormControl, FormBuilder } from '@angular/forms';
import { TaskCreateComponent } from './../task-create/task-create.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { Configs } from '../../shared/common/configs/configs';
@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit {
  tasks: Array<any> = [];
  events: Array<any> = [];
  filter: any = {};
  maxiumPage = 0;
  searchForm = this._fb.group({
    eventId: '',
    staffName: '',
    priority: '',
    staffNullable: null,
  });
  staffId = JwtTokenHelper.userId;
  myDepartment = JwtTokenHelper.departmentId;
  taskId = '';
  hideEventId = false;
  completeSubtask = [];
  roleLevel = JwtTokenHelper.level;
  isManagerEvent = JwtTokenHelper.isManage(Configs.ManageTaskFeatureId);
  isAdmin = JwtTokenHelper.isAdmin;
  myId = JwtTokenHelper.userId;
  eventNullable = this.isAdmin ? false : 1;
  typeTaskSelect = this.isAdmin ? 2 : 1;

  typeTask = this.isAdmin ? new FormControl(2) : new FormControl(1);
  constructor(
    private _fb: FormBuilder,
    public modalController: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.tasks = [];
    this.loadEvent();

    if (this._route.snapshot.queryParams['eid']) {
      this.searchForm
        .get('eventId')
        .setValue(this._route.snapshot.queryParams['eid']);
      this.loadTask();
    } else {
      this.loadTask();
    }
  }

  onFilter(event?: any) {
    this.filter.PageIndex = 1;
    this.searchTask();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.searchForm.reset();
    this.onFilter();
  }

  changeType() {
    this.searchForm.reset();
    this.filter.PageIndex = 1;
    this.taskId = null;
    this.maxiumPage = 0;
    if (this.typeTaskSelect == 2) {
      this.hideEventId = false;
      this.eventNullable = false;
    } else if (this.typeTaskSelect == 3) {
      this.hideEventId = true;
      this.eventNullable = true;
    } else if (this.typeTaskSelect == 1) {
      this.hideEventId = false;
      this.eventNullable = null;
    } else if (this.typeTaskSelect == 5) {
      this.hideEventId = false;
      this.eventNullable = null;
    } else if (this.typeTaskSelect == 4) {
      this.hideEventId = false;
      this.eventNullable = null;
    } else if (this.typeTaskSelect == 6) {
      this.hideEventId = false;
      this.eventNullable = null;
    }
    this.loadTask();
  }
  loadEvent() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.eventSearch + '/running-events', {
        textSearch: '',
      })
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.events = res.content.events;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  loadTask(event?) {
    if (event) {
      event.target.complete();
    }
    let staffId = undefined;
    if (!this.isManagerEvent && this.typeTaskSelect != 4) {
      staffId = this.staffId;
    }
    let eventId =
      this.searchForm.get('eventId').value === 1
        ? undefined
        : this.searchForm.get('eventId').value;
    const priority =
      this.searchForm.get('priority').value === 3
        ? undefined
        : this.searchForm.get('priority').value;
    const staffName = this.searchForm.get('staffName').value;
    const eventNullable =
      this.eventNullable === 1 ? undefined : this.eventNullable;
    let payload = {
      ...this.searchForm.value,
      eventNullable: eventNullable,
      ...this.filter,
      eventId: eventId,
      staffName: staffName,
      priority: priority,
      status: 2,
      staffId: staffId,
    };

    if (this.typeTaskSelect == 1) {
      payload = {
        ...payload,
        staffId: this.myId,
      };
    } else if (this.typeTaskSelect == 4) {
      payload = {
        ...payload,
        departmentNullable: false,
      };
    } else if (this.typeTaskSelect == 5) {
      payload = {
        ...payload,
        staffNullable: true,
      };
    }
    payload = { ...payload, status: 2 };
    this._reusableService.onSearch(ApiUrl.taskApi, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        let { tasks, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (tasks) {
          if (this.filter.PageIndex == 1) {
            this.tasks = tasks;
          } else {
            this.tasks = this.tasks.concat(tasks);
          }
        } else {
          this.tasks = [];
        }
        this.completeSubtask = [];
        this.tasks.forEach((task) => {
          this.completeSubtask.push(
            task.subTasks.filter((res) => res.status).length
          );
        });
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  reGetTask() {
    this.clientState.isBusy = true;
    let staffId = undefined;
    if (this.typeTaskSelect == 1 || !this.isManagerEvent) {
      staffId = this.staffId;
    }
    const pageSize = 10 * this.filter.PageIndex;
    const priority =
      this.searchForm.get('priority').value === 3
        ? undefined
        : this.searchForm.get('priority').value;
    const staffNullable =
      this.searchForm.get('staffNullable').value === 1
        ? null
        : this.searchForm.get('staffNullable').value;
    const eventId =
      this.searchForm.get('eventId').value === 1
        ? null
        : this.searchForm.get('eventId').value;
    const eventNullable = this.eventNullable === 1 ? null : this.eventNullable;
    let payload = {
      ...this.searchForm.value,
      eventNullable: eventNullable,
      PageSize: pageSize,
      staffNullable: staffNullable,
      eventId: eventId,
      priority: priority,
      staffId: staffId,
      status: 2,
    };
    if (this.typeTaskSelect === 1) {
      payload = {
        ...payload,
        PageSize: pageSize,
        staffId: this.myId,
      };
    } else if (this.typeTaskSelect === 4) {
      payload = {
        departmentNullable: false,
        ...payload,
        ...this.filter,
      };
    } else if (this.typeTaskSelect == 5) {
      payload = {
        ...payload,
        staffNullable: true,
      };
    }
    this._reusableService.onSearch(ApiUrl.taskApi, payload).subscribe((res) => {
      let { tasks, ...paging } = res.content;
      this.maxiumPage = Math.ceil(paging.totalRecord / Configs.DefaultPageSize);
      this.tasks = tasks;
      this.completeSubtask = [];
      this.tasks.forEach((task) => {
        this.completeSubtask.push(
          task.subTasks.filter((res) => res.status).length
        );
      });
    });
  }

  searchTask() {
    this.clientState.isBusy = true;
    let staffId = undefined;
    if (this.typeTaskSelect == 1 || !this.isManagerEvent) {
      staffId = this.staffId;
    }
    const priority =
      this.searchForm.get('priority').value === 3
        ? undefined
        : this.searchForm.get('priority').value;
    const eventId =
      this.searchForm.get('eventId').value === 1
        ? null
        : this.searchForm.get('eventId').value;
    const staffNullable =
      this.searchForm.get('staffNullable').value === 1
        ? null
        : this.searchForm.get('staffNullable').value;
    let payload;

    if (this.typeTaskSelect == 2) {
      payload = { eventNullable: false };
    } else if (this.typeTaskSelect == 1) {
      payload = {
        eventNullable: null,
        staffId: this.myId,
      };
    } else if (this.typeTaskSelect == 4) {
      payload = {
        eventNullable: null,
        departmentNullable: false,
      };
    } else if (this.typeTaskSelect == 5) {
      payload = { eventNullable: null, departmentNullable: null };
    } else if (this.typeTaskSelect == 6) {
    } else {
      payload = { eventNullable: true };
    }
    payload = {
      ...payload,
      status: 2,
      staffId: staffId,
      ...this.searchForm.value,
      eventId: eventId,
      priority: priority,
      staffNullable: staffNullable,
    };
    this._reusableService.onSearch(ApiUrl.taskApi, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        let { tasks } = res.content;
        this.tasks = tasks;
        this.completeSubtask = [];
        this.tasks.forEach((task) => {
          this.completeSubtask.push(
            task.subTasks.filter((res) => res.status).length
          );
        });
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  getDataByAll() {
    this.clientState.isBusy = true;
    this._reusableService.onSearch(ApiUrl.taskApi);
  }

  async createTask() {
    const modal = await this.modalController.create({
      component: TaskCreateComponent,
      cssClass: 'modal-task',
      backdropDismiss: false,
      componentProps: {
        eventId:
          this.searchForm.get('eventId').value === 1 ||
          this.searchForm.get('eventId').value === null
            ? null
            : this.searchForm.get('eventId').value,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.loadTask();
      }
    });
    return await modal.present();
  }

  closed() {
    // this.searchForm.reset();
    setTimeout(() => {
      this.reGetTask();
    }, 500);
    this.taskId = '';
  }

  reloadTask() {
    setTimeout(() => {
      this.reGetTask();
    }, 50);
  }

  taskDetail(taskId) {
    this.taskId = taskId;
  }

  onLoadMoreData() {
    if (this.filter.PageIndex <= this.maxiumPage && this.tasks.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.loadTask();
      }
    }
  }
}

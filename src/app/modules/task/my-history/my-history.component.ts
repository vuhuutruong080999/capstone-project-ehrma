import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-my-history',
  templateUrl: './my-history.component.html',
  styleUrls: ['./my-history.component.scss'],
})
export class MyHistoryComponent implements AfterContentChecked {
  isMobile = false;
  segment: string = 'completed';
  myId = JwtTokenHelper.userId;
  pagingCompletedModel = new PagingModel();
  pagingCancelModel = new PagingModel();
  refreshTable = false;
  constructor(
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.onSearchAll();
  }
  ngAfterContentChecked() {
    this._cd.detectChanges();
  }
  doRefresh(event) {
    this.refreshTable = true;
    this.onSearchAll(event);
  }
  stopRefresh(event) {
    this.refreshTable = event;
  }
  onSearchAll(event?) {
    this.onSearchCancel(event);
    this.onSearchCompleted(event);
  }
  onSearchCancel(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService
      .onSearch(ApiUrl.taskApi, { Status: 0, staffId: this.myId })
      .subscribe(
        (res) => {
          let { ...paging } = res.content;
          this.pagingCancelModel = paging;
          this.clientState.isBusy = false;
        },
        () => {
          this.clientState.isBusy = false;
        }
      );
  }

  onSearchCompleted(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService
      .onSearch(ApiUrl.taskApi, { Status: 1, staffId: this.myId })
      .subscribe(
        (res) => {
          let { ...paging } = res.content;
          this.pagingCompletedModel = paging;
          this.clientState.isBusy = false;
        },
        () => {
          this.clientState.isBusy = false;
        }
      );
  }
}

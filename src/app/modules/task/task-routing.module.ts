import { MyHistoryComponent } from './my-history/my-history.component';
import { TaskHistoryComponent } from './task-history/task-history.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskListComponent } from './task-list/task-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskHistoryDetailComponent } from './task-history-detail/task-history-detail.component';
import { TaskDetailLinkComponent } from './task-detail-link/task-detail-link.component';

const routes: Routes = [
  {
    path: 'list',
    component: TaskListComponent,
  },
  {
    path: 'history',
    component: TaskHistoryComponent,
  },
  {
    path: 'my-history',
    component: MyHistoryComponent,
  },

  { path: 'processing/detail/:id', component: TaskDetailLinkComponent },
  { path: 'history/detail/:id', component: TaskHistoryDetailComponent },
  { path: 'my-history/detail/:id', component: TaskHistoryDetailComponent },

  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TaskRoutingModule {}

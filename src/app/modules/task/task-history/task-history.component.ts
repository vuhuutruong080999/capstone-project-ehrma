import { ActivatedRoute } from '@angular/router';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { PagingModel } from '../../shared/models/api-response/api-response';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-task-history',
  templateUrl: './task-history.component.html',
  styleUrls: ['./task-history.component.scss'],
})
export class TaskHistoryComponent implements OnInit, AfterContentChecked {
  isMobile = false;
  segment: string = 'completed';

  pagingCompletedModel = new PagingModel();
  pagingCancelModel = new PagingModel();
  refreshTable = false;
  history = false;
  eventId: string;
  constructor(
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _cd: ChangeDetectorRef,
    private _route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    this.onSearchAll();
    if (this._route.snapshot.queryParams['eid']) {
      this.eventId = this._route.snapshot.queryParams['eid'];
    }
  }

  ngAfterContentChecked() {
    this._cd.detectChanges();
  }

  doRefresh(event) {
    this.refreshTable = true;
    this.onSearchAll(event);
  }

  stopRefresh(event) {
    this.refreshTable = event;
  }

  onSearchAll(event?) {
    this.onSearchCancel(event);
    this.onSearchCompleted(event);
  }

  onSearchCancel(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService.onSearch(ApiUrl.taskApi, { Status: 0 }).subscribe(
      (res) => {
        let { ...paging } = res.content;
        this.pagingCancelModel = paging;
        this.clientState.isBusy = false;
      },
      () => {
        this.clientState.isBusy = false;
      }
    );
  }

  onSearchCompleted(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService.onSearch(ApiUrl.taskApi, { Status: 1 }).subscribe(
      (res) => {
        let { ...paging } = res.content;
        this.pagingCompletedModel = paging;
        this.clientState.isBusy = false;
      },
      () => {
        this.clientState.isBusy = false;
      }
    );
  }
}

import { MatCheckboxChange } from '@angular/material/checkbox';
import { TaskUpdateComponent } from './../task-update/task-update.component';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { CommentService } from './../../shared/services/api/comment.service';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  ModalController,
  AlertController,
  NavController,
  IonSearchbar,
} from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { FormBuilder } from '@angular/forms';
import { TaskService } from '../../shared/services/api/task.service';
import { fiveMinutes } from '@task/pipes/five-minutes.pipe';
import { Configs } from '../../shared/common/configs/configs';
import { TaskCreateComponent } from '@task/task-create/task-create.component';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss'],
})
export class TaskDetailComponent
  implements OnInit, OnChanges, AfterContentChecked
{
  @Input() id: string;
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @Output() closeDetail = new EventEmitter<boolean>();
  @Output() reloadTask = new EventEmitter<boolean>();

  deleteUrl = ApiUrl.taskApi;

  isLoading = false;
  task;
  staffList = [];
  filter = new FilterNormalModel();
  maxiumPage = 0;
  departmentList = [];
  departmentGroups = [];
  comments = [];
  files = [];
  fileNameChoice = null;
  process = 0;
  file: File;
  fileControl = this._fb.control(null);
  formComment = this._fb.group({
    task: null,
    content: null,
  });
  showSubTasks = true;
  showComment = false;
  users = this._fb.control('');
  departments = this._fb.control('');
  roleLevel = JwtTokenHelper.level;
  userId = JwtTokenHelper.userId;
  myName = JwtTokenHelper.fullName;
  isManagerTask = JwtTokenHelper.isManage(Configs.ManageTaskFeatureId);
  showFiles = false;
  constructor(
    private _fb: FormBuilder,
    public modalController: ModalController,
    private _reusableService: ReusableService,
    private alertCtrl: AlertController,
    public clientState: ClientState,
    private _cd: ChangeDetectorRef,
    private _commentService: CommentService,
    private navCtrl: NavController,
    private _taskService: TaskService
  ) {
    this.users.valueChanges.subscribe((res) => {
      if (res) {
        this.changeMember(res);
      }
    });
    this.departments.valueChanges.subscribe((res) => {
      this.changeDepartment(res);
    });
  }

  ngOnInit() {}
  ngOnChanges() {
    this.loadData();
  }
  ngAfterContentChecked() {
    this._cd.detectChanges();
  }
  changeShowFiles() {
    if (this.files.length) this.showFiles = !this.showFiles;
  }
  loadData() {
    this.clientState.isBusy = true;
    this.task = {};
    this._reusableService.onGetById(ApiUrl.taskApi, this.id).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.task = res.content;
        this.formComment.get('task').setValue(this.task.id);
        this.loadComment(this.task.id);

        if (!this.task.departmentId) {
          this.loadDepartment();
        }
        if (this.task.status === 2) {
          this.getStaff(this.task.departmentId);
        }
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
  loadDepartment() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.departmentSearch, { textSearch: '' })
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.departmentList = res.content.departments;
        this.departmentGroups = this.departmentList.slice();
      });
  }
  loadComment(taskId: string) {
    this.clientState.isBusy = true;
    this.comments = [];
    this.files = [];
    this._reusableService
      .onSearch(ApiUrl.comment, { task: taskId })
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.comments = res.content.comments;
        const commentFile = this.comments.filter(
          (comment) => comment.files.length > 0
        );
        commentFile.forEach((file) => {
          this.files.push(file.files[0]);
        });
      });
  }
  changeDepartment(res) {
    const payload = {
      ...this.task,
      departmentId: res.id,
    };
    this.clientState.isBusy = true;
    this._reusableService.onUpdate(ApiUrl.taskApi, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.task = res.content;
      },
      () => {
        this.clientState.isBusy = false;
      }
    );
  }
  changeMember(data) {
    const payload = {
      taskId: this.task.id,
      userId: data.id,
    };
    this.clientState.isBusy = true;
    this._reusableService
      .onUpdate(ApiUrl.taskApi + `/${this.task.id}/assign-task`, payload)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        if (res.content) {
          this.task.staffName = data.fullname;
          this.task.staffId = data.id;
          this._reusableService.onHandleSuccess(res.message);
        } else {
          this._reusableService.onHandleFailed(res.message);
        }
      });
  }
  async selectStatus(status: number) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message:
        status === 1
          ? 'Bạn có chắc muốn hoàn thành này?'
          : 'Bạn có chắc muốn huỷ này?',
      buttons: [
        { text: 'Huỷ', role: 'cancel' },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            if (status === 1) {
              this._taskService
                .changeStatus(ApiUrl.taskApi + `/${this.task.id}/finish-task`)
                .subscribe(
                  (res) => {
                    this.clientState.isBusy = false;
                    if (res.content === true) {
                      this.task.status = status;
                      this.task.subTasks.forEach((res) => {
                        res.status = true;
                      });
                      this.formComment.get('content').disable();
                      this._reusableService.onHandleSuccess(res.message);
                      this.reloadTask.emit(true);
                    } else {
                      this._reusableService.onHandleFailed(res.message);
                    }
                  },
                  (error) => {
                    this.clientState.isBusy = false;
                  }
                );
            } else if (status === 0) {
              this._taskService
                .changeStatus(ApiUrl.taskApi + `/${this.task.id}/cancel-task`)
                .subscribe(
                  (res) => {
                    if (res.content) {
                      this.task.status = status;
                      this.clientState.isBusy = false;
                      this.formComment.get('content').disable();
                      this._reusableService.onHandleSuccess(res.message);
                      this.reloadTask.emit(true);
                    } else {
                      this._reusableService.onHandleFailed(res.message);
                      this.clientState.isBusy = false;
                    }
                  },
                  (error) => {
                    this.clientState.isBusy = false;
                  }
                );
            }
          },
        },
      ],
    });
    return await alert.present();
  }
  closing() {
    this.closeDetail.emit(true);
  }

  selectPriority(priority: number) {
    this.task.priority = priority;
    this.clientState.isBusy = true;
    this._reusableService.onUpdate(ApiUrl.taskApi, this.task).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.task.priority = priority;
        this.reloadTask.emit(true);
      },
      () => {
        this.clientState.isBusy = false;
      }
    );
  }
  async deleteComment(comment: any) {
    if (this.task.status !== 2) {
      this._reusableService.onHandleFailed('Công việc không đang xử lý');
      return;
    }
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xoá bình luận này?',
      buttons: [
        { text: 'Huỷ', role: 'cancel' },
        {
          text: 'Xác nhận',
          handler: () => {
            if (fiveMinutes(comment.createdDate)) {
              this._reusableService.onHandleFailed(
                'Không thể xoá bình luận quá 5 phút'
              );
            } else {
              this.clientState.isBusy = true;
              this._reusableService
                .onDelete(ApiUrl.comment, comment.id)
                .subscribe(
                  (res) => {
                    this.clientState.isBusy = false;
                    if (res.content) {
                      this.loadComment(this.task.id);
                      this._reusableService.onHandleSuccess(res.message);
                    } else {
                      this._reusableService.onHandleFailed(res.message);
                    }
                  },
                  (error) => {
                    this.clientState.isBusy = false;
                  }
                );
            }
          },
        },
      ],
    });
    return await alert.present();
  }
  async updateTask() {
    const modal = await this.modalController.create({
      component: TaskUpdateComponent,
      cssClass: 'modal-task-update',
      componentProps: {
        data: this.task,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
        }
        this.loadData();
        this.reloadTask.emit(true);
      }
    });
    return await modal.present();
  }
  async copyTask() {
    const modal = await this.modalController.create({
      component: TaskCreateComponent,
      cssClass: 'modal-task-update',
      componentProps: {
        data: this.task,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
        }
        this.reloadTask.emit(true);
      }
    });
    return await modal.present();
  }
  async deleteTask(id: number) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa công việc này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService.onDelete(ApiUrl.taskApi, id).subscribe(
              (res) => {
                this.clientState.isBusy = false;
                this._reusableService.onHandleSuccess(
                  'Xoá công việc thành công'
                );
                this.reloadTask.emit(true);
                this.closing();
              },
              (error) => {
                this.clientState.isBusy = false;
              }
            );
          },
        },
      ],
    });
    return await alert.present();
  }
  async changeInputFile(event): Promise<void> {
    const files: File = event.target.files;
    this.file = files[0];
    this.fileNameChoice = this.file.name;
  }
  removeFile() {
    this.file = null;
    this.fileNameChoice = null;
    this.fileControl.reset();
  }
  sendMessage(e) {
    e.stopPropagation();
    if (
      (this.formComment.get('content').value === null && !this.file) ||
      this.isLoading
    ) {
      return;
    }
    const payload = {
      ...this.formComment.value,
      files: [this.file ? this.file : null],
    };
    this.clientState.isBusy = true;
    this.isLoading = true;
    this._commentService
      .commentPut(ApiUrl.comment, payload)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.comments.unshift(res.content);
        if (res.content.files.length > 0) {
          this.files.unshift(res.content.files[0]);
        }
        this.formComment.get('content').reset();
        this.removeFile();
        this.isLoading = false;
      });
  }
  goFile(fileURL: string) {
    window.open(fileURL, '_blank');
  }

  onGoBack() {
    this.navCtrl.navigateBack(['task/history'], { animated: false });
  }
  getTask() {
    this.clientState.isBusy = true;
    const payload = {
      userId: this.userId,
      taskId: this.task.id,
    };
    this._reusableService
      .onUpdate(ApiUrl.taskApi + `/${this.task.id}/assign-task`, payload)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        if (res.content) {
          this.task.staffName = this.myName;
          this.task.staffId = this.userId;
          this._reusableService.onHandleSuccess(res.message);
        } else {
          this._reusableService.onHandleFailed(res.message);
        }
      });
  }
  getStaff(departmentId) {
    this.clientState.isBusy = true;
    let payload;
    if (this.task.eventId) {
      if (this.roleLevel == 0) {
        payload = {
          departmentId: departmentId,
          eventId: this.task.eventId,
          roleLevel: 0,
        };
      } else {
        payload = { departmentId: departmentId, eventId: this.task.eventId };
      }
    }
    if (this.roleLevel == 0) {
      payload = { ...payload, departmentId: departmentId, roleLevel: 0 };
    } else {
      payload = { ...payload, departmentId: departmentId, ...this.filter };
    }
    this._reusableService
      .onSearch(ApiUrl.departmentUserSearch, payload)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        let { departments, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );

        if (this.filter.PageIndex == 1) {
          this.staffList = departments;
        } else {
          this.staffList = this.staffList.concat(departments);
        }
      });
  }

  onResetFilter() {
    if (!this.staffList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.TextSearch = '';
      this.getStaff(this.task.departmentId);
    }
  }
  onLoadMoreData() {
    if (this.filter.PageIndex <= this.maxiumPage && this.staffList.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.getStaff(this.task.departmentId);
      }
    }
  }
  onSearch(event: any) {
    this.filter.PageIndex = 1;
    this.filter.TextSearch = event.detail.value;
    this.getStaff(this.task.departmentId);
  }
  changeSubTask(event: MatCheckboxChange, i: number) {
    this.task.subTasks[i].status = event.checked;
    this.clientState._isBusy = true;
    const payload = {
      subTasks: this.task.subTasks,
    };
    this._reusableService
      .onUpdate(ApiUrl.taskApi + `/${this.task.id}/processing-status`, payload)
      .subscribe((res) => {
        this.clientState._isBusy = false;
        this.reloadTask.emit(true);
        this.process = parseInt(
          new Intl.NumberFormat('en-IN', {
            maximumSignificantDigits: 2,
          }).format(
            (this.task.subTasks.filter((res) => res.status).length * 100) /
              this.task.subTasks.length
          )
        );
      });
  }
}

import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  FormArray,
} from '@angular/forms';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController, AlertController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.scss'],
})
export class TaskCreateComponent implements OnInit {
  @Input() eventId: string;
  @Input() staff: string;
  @Input() data: any;
  @ViewChild('staffSearchbar') staffSearchbar: IonSearchbar;
  @ViewChild('departmentSearch') departmentSearchBar: IonSearchbar;
  @ViewChild('eventSearch') eventSearchBar: IonSearchbar;
  formGroup: any;
  groups = [];
  department = [];
  groupDepartment = [];
  staffList = [];
  eventList = [];
  departmentList = [];
  groupEvent = [];
  equipments = [];
  selectedUserFullname = '';
  staffFilter = new FilterNormalModel();
  staffMaxiumPage = 0;
  departmentFilter = new FilterNormalModel();
  departmentMaxiumPage = 0;
  eventFilter = new FilterNormalModel();
  eventMaxiumPage = 0;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.departmentFilter.PageIndex = 1;
    this.departmentFilter.PageSize = Configs.DefaultPageSize;
    this.eventFilter.PageIndex = 1;
    this.eventFilter.PageSize = Configs.DefaultPageSize;
    this.staffFilter.PageIndex = 1;
    this.staffFilter.PageSize = Configs.DefaultPageSize;
    this.onCreateForm();
    console.log(this.formGroup.get('priority').value);

    this.formGroup.get('departmentId').valueChanges.subscribe((data) => {
      let check = true;
      this.staffList.forEach((department) => {
        department.users.forEach((user) => {
          if (user.id === this.formGroup.get('staffId').value) {
            check = false;
          }
        });
      });

      if (check) this.formGroup.get('staffId').reset();
      this.getStaff(data);
    });
    this.formGroup.get('eventId').valueChanges.subscribe((data) => {
      this.formGroup.get('staffId').reset();
      this.formGroup.get('departmentId').reset();
      this.eventId = data;
      this.getStaff();
      this.getEquipment(data);
    });
    this.formGroup.get('staffId').valueChanges.subscribe((data) => {
      this.staffList.forEach((department) => {
        department.users.forEach((user) => {
          if (user.id === data) this.selectedUserFullname = user.fullname;
        });
      });
    });

    this.getEvent();
    this.getStaff();
    this.getDepartment();
    this.data?.subTasks?.forEach((data) => {
      this.setSub(data.name, false);
    });
  }

  onCreateForm() {
    this.formGroup = this._fb.group({
      name: [
        this.data?.name || '',
        [Validators.required, Validators.maxLength(100)],
      ],
      eventId: [this.eventId || null],
      description: [
        this.data?.description || '',
        [Validators.required, Validators.maxLength(500)],
      ],
      deadline: ['', [Validators.required, this.checkDeadline]],
      priority: [
        this.data?.priority
          ? this.data?.priority
          : this.data?.priority === 0
          ? 0
          : '',
        Validators.required,
      ],
      staffId: [this.staff || null],
      departmentId: [null],
      subTasks: this._fb.array([]),
      equipments: this._fb.array([]),
    });

    setTimeout(() => {}, 150);
  }

  get name() {
    return this.formGroup.get('name');
  }
  get event() {
    return this.formGroup.get('eventId');
  }
  get description() {
    return this.formGroup.get('description');
  }
  get deadline() {
    return this.formGroup.get('deadline');
  }
  get priority() {
    return this.formGroup.get('priority');
  }
  get staffId() {
    return this.formGroup.get('staffId');
  }
  get departmentId() {
    return this.formGroup.get('departmentId');
  }
  getEquipment(eventId: string) {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.equipmentGetById, { eventId: eventId })
      .subscribe((res) => {
        this.clientState._isBusy = false;
        this.equipments = res.content.equipments;
      });
  }
  getStaff(departmentId?: string) {
    this.clientState.isBusy = true;
    const payload = {
      departmentId: departmentId,
      ...this.staffFilter,
      eventId: this.eventId,
    };
    this._reusableService
      .onSearch(ApiUrl.departmentUserSearch, payload)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        const { departments, ...paging } = res.content;
        this.staffMaxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.staffFilter.PageIndex == 1) {
          this.staffList = departments;
        } else {
          this.staffList = this.staffList.concat(departments);
        }
        this.staffList.forEach((department) => {
          department.users.forEach((user) => {
            if (user.id === this.staff)
              this.selectedUserFullname = user.fullname;
          });
        });
      });
  }

  getDepartment() {
    this.clientState.isBusy = true;
    const payload = { ...this.departmentFilter };
    this._reusableService
      .onSearch(ApiUrl.departmentSearch, payload)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        const { departments, ...paging } = res.content;
        this.departmentMaxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.departmentFilter.PageIndex == 1) {
          this.departmentList = departments;
        } else {
          this.departmentList = this.departmentList.concat(departments);
        }
      });
  }
  getEvent() {
    this.clientState.isBusy = true;
    const payload = { ...this.eventFilter };
    this._reusableService
      .onSearch(ApiUrl.eventSearch + '/running-events', payload)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        const { events, ...paging } = res.content;
        this.eventMaxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.eventFilter.PageIndex == 1) {
          this.eventList = events;
        } else {
          this.eventList = this.eventList.concat(events);
        }
      });
  }
  onSubmit() {
    if (this.formGroup.invalid) {
      return;
    }
    this.clientState.isBusy = true;
    const payload = {
      ...this.formGroup.value,
      status: 1,
    };

    this._reusableService.onCreate(ApiUrl.taskApi, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.modalCtrl.dismiss(true);
        this._reusableService.onHandleSuccess('Tạo công việc thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
  onSearchStaff(event: any) {
    this.staffFilter.PageIndex = 1;
    this.staffFilter.TextSearch = event.detail.value;
    this.getStaff(this.formGroup.get('departmentId').value);
  }
  onResetFilterStaff() {
    if (!this.staffList.length) {
      this.staffSearchbar.value = '';
      this.staffFilter.PageIndex = 1;
      this.staffFilter.TextSearch = '';
      this.getStaff(this.formGroup.get('departmentId').value);
    }
  }
  onLoadMoreDataStaff() {
    if (
      this.staffFilter.PageIndex <= this.staffMaxiumPage &&
      this.staffList.length > 0
    ) {
      if (this.staffMaxiumPage != 1) {
        this.staffFilter.PageIndex = this.staffFilter.PageIndex + 1;
        this.getStaff(this.formGroup.get('departmentId').value);
      }
    }
  }
  onSearchDepartment(event: any) {
    this.departmentFilter.PageIndex = 1;
    this.departmentFilter.TextSearch = event.detail.value;
    this.getDepartment();
  }
  onResetFilterDepartment() {
    if (!this.departmentList.length) {
      this.departmentSearchBar.value = '';
      this.departmentFilter.PageIndex = 1;
      this.departmentFilter.TextSearch = '';
      this.getStaff();
    }
  }
  onLoadMoreDataDepartment() {
    if (
      this.departmentFilter.PageIndex <= this.departmentMaxiumPage &&
      this.departmentList.length > 0
    ) {
      if (this.departmentMaxiumPage != 1) {
        this.departmentFilter.PageIndex = this.departmentFilter.PageIndex + 1;
        this.getDepartment();
      }
    }
  }
  onSearchEvent(event: any) {
    this.eventFilter.PageIndex = 1;
    this.eventFilter.TextSearch = event.detail.value;
    this.getEvent();
  }
  onResetFilterEvent() {
    if (!this.eventList.length) {
      this.eventSearchBar.value = '';
      this.eventFilter.PageIndex = 1;
      this.eventFilter.TextSearch = '';
      this.getEvent();
    }
  }
  onLoadMoreDataEvent() {
    if (
      this.eventFilter.PageIndex <= this.eventMaxiumPage &&
      this.eventList.length > 0
    ) {
      if (this.eventMaxiumPage != 1) {
        this.eventFilter.PageIndex = this.eventFilter.PageIndex + 1;
        this.getEvent();
      }
    }
  }

  checkDeadline(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      const deadline = control.value;
      const currentTime = new Date();
      if (deadline < currentTime) {
        return { invalidDate: true };
      }
    } else {
      return null;
    }
  }
  getMore() {
    return this._fb.group({
      status: [false],
      name: ['', Validators.required],
    });
  }

  addMore() {
    const control = <FormArray>this.formGroup.controls['subTasks'];
    control.push(this.getMore());
  }

  removeRow(i: number) {
    const control = <FormArray>this.formGroup.controls['subTasks'];
    control.removeAt(i);
  }
  getMoreEquipment() {
    return this._fb.group({
      quantity: ['', Validators.required],
      name: ['', Validators.required],
    });
  }

  addMoreEquipment() {
    const control = <FormArray>this.formGroup.controls['equipments'];
    control.push(this.getMoreEquipment());
  }

  removeRowEquipment(i: number) {
    const control = <FormArray>this.formGroup.controls['equipments'];
    control.removeAt(i);
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
  setSub(name: string, status: boolean) {
    const control = <FormArray>this.formGroup.controls['subTasks'];
    control.push(
      this._fb.group({
        name: [name, Validators.required],
        status: [status],
      })
    );
  }
  setEquipment(name: string, quantity: number) {
    const control = <FormArray>this.formGroup.controls['equipments'];
    control.push(
      this._fb.group({
        name: [name, Validators.required],
        quantity: [quantity, Validators.required],
      })
    );
  }
}

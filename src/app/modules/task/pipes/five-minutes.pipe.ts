import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'fiveminutes' })
export class FiveMinutes implements PipeTransform {
  transform(time: string): boolean {
    const timeParse = Date.parse(time);
    const nowTime = Date.now();
    const compare = nowTime - timeParse;

    if (compare > 5 * 60 * 1000) {
      return false;
    }
    return true;
  }
}

export function fiveMinutes(time: string): boolean {
  const timeParse = Date.parse(time);
  const nowTime = Date.now();
  const compare = nowTime - timeParse;

  if (compare > 5 * 60 * 1000) {
    return true;
  }
  return false;
}

import { element } from 'protractor';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { MatTableDataSource } from '@angular/material/table';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { IonSearchbar } from '@ionic/angular';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { Router } from '@angular/router';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-task-history-table',
  templateUrl: './task-history-table.component.html',
  styleUrls: ['./task-history-table.component.scss'],
})
export class TaskHistoryTableComponent implements OnInit, OnChanges {
  @Input() status: number;
  @Input() refresh: boolean;
  @Input() eventId: string;
  @Output() refreshed = new EventEmitter<boolean>();
  @ViewChild('eventSearch') eventSearchBar: IonSearchbar;
  @Input() myId: string;
  @ViewChild('searchbar') searchbar: IonSearchbar;

  @ViewChild('MatPaginator') set paginator(paging: MatPaginator) {
    if (paging) {
      paging._intl.itemsPerPageLabel = '';
    }
  }
  isMobile = false;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên công việc' },
    { id: 'staffName', name: 'Người thực hiện' },
    { id: 'priority', name: 'Độ ưu tiên' },
    { id: 'deadline', name: 'Hạn chót' },
    { id: 'eventName', name: 'Sự kiện' },
  ];

  searchForm: FormGroup;
  filter = new FilterNormalModel();
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage = 0;

  filterUser = new FilterNormalModel();
  pageSizeListUser = Configs.PageSizeList;
  pagingModelUser = new PagingModel();
  maxiumPageUser = 0;
  departmentUserList = [];
  showColumns: string[];
  eventList = [];
  eventFilter = new FilterNormalModel();
  eventMaxiumPage = 0;
  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnChanges(): void {
    if (this.refresh === true) {
      this.onSearchAll();
    }
  }

  ngOnInit(): void {
    this.eventFilter.PageIndex = 1;
    this.eventFilter.PageSize = Configs.DefaultPageSize;
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.createSearchForm();

    if (this.eventId) {
      this.searchForm.get('eventName').setValue(this.eventId);
    }
    this.onSearchAll();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
      eventName: [''],
      eventNullable: null,
      staffName: [''],
    });
  }

  onSearchAll() {
    this.refreshed.emit(false);
    if (window.innerWidth < Configs.MobileWidth) {
      this.filter.PageIndex = 1;
    }
    this.onSearch();
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    const eventNullable =
      this.searchForm.get('eventNullable').value === 1
        ? null
        : this.searchForm.get('eventNullable').value;
    let payload = {
      ...this.searchForm.value,
      ...this.filter,
      status: this.status,
      eventNullable: eventNullable,
    };
    if (this.myId) {
      payload = { ...payload, staffId: this.myId };
    }
    this._reusableService.onSearch(ApiUrl.taskApi, payload).subscribe(
      (res) => {
        let { tasks, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          //mobile
          if (this.filter.PageIndex == 1) {
            this.dataSource.data = tasks;
          } else {
            this.dataSource.data = this.dataSource.data.concat(tasks);
          }
        } else if (!tasks) {
          this.dataSource.data = [];
        } else {
          //web
          this.dataSource.data = tasks;
        }
        this.pagingModel = paging;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onFilter(event?: any) {
    this.filter.PageIndex = 1;
    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.searchForm.reset();
    this.onFilter();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }
  viewDetail(element) {
    if (this.myId) {
      this._router.navigate(['/task/my-history/detail', element.id], {
        state: element,
      });
    } else
      this._router.navigate(['/task/history/detail', element.id], {
        state: element,
      });
  }
  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
}

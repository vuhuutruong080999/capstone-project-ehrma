import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { TaskHistoryDetailComponent } from './task-history-detail/task-history-detail.component';
import { FiveMinutes } from './pipes/five-minutes.pipe';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { TaskRoutingModule } from './task-routing.module';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskCreateComponent } from './task-create/task-create.component';
import { MatDatetimepickerModule } from '@mat-datetimepicker/core';
import { MatMomentDatetimeModule } from '@mat-datetimepicker/moment';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TaskHistoryComponent } from './task-history/task-history.component';
import { TaskHistoryTableComponent } from './task-history-table/task-history-table.component';
import { MyHistoryComponent } from './my-history/my-history.component';
import { TaskUpdateComponent } from './task-update/task-update.component';
import { TaskDetailLinkComponent } from './task-detail-link/task-detail-link.component';

@NgModule({
  declarations: [
    TaskListComponent,
    TaskDetailComponent,
    TaskCreateComponent,
    TaskHistoryComponent,
    TaskHistoryTableComponent,
    FiveMinutes,
    MyHistoryComponent,
    TaskUpdateComponent,
    TaskHistoryDetailComponent,
    TaskDetailLinkComponent,
  ],
  imports: [
    TaskRoutingModule,
    SharedModule,
    MatMomentDatetimeModule,
    MatDatetimepickerModule,
    InfiniteScrollModule,
    RoundProgressModule,
  ],
  exports: [TaskCreateComponent],
})
export class TaskModule {}

import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { ActivatedRoute } from '@angular/router';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ModalController, NavController, IonSearchbar } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { FormControl, FormBuilder } from '@angular/forms';
import { TaskCreateComponent } from '@task/task-create/task-create.component';

@Component({
  selector: 'app-task-history-detail',
  templateUrl: './task-history-detail.component.html',
  styleUrls: ['./task-history-detail.component.scss'],
})
export class TaskHistoryDetailComponent
  implements OnInit, OnChanges, AfterContentChecked
{
  @Input() id: string;
  @ViewChild('searchbar') searchbar: IonSearchbar;
  isLoading = false;
  task;
  maxiumPage = 0;
  comments = [];
  files = [];
  dontShow = undefined;
  fileNameChoice = null;
  file: File;
  process = 0;
  showSubTasks = true;
  isManagerTask = JwtTokenHelper.isManage(Configs.ManageTaskFeatureId);
  showComment = false;
  fileControl = this._fb.control(null);
  formComment = this._fb.group({
    task: null,
    content: null,
  });
  fromFinish = false;
  users = this._fb.control('');
  departments = this._fb.control('');
  roleLevel = JwtTokenHelper.level;
  userId = JwtTokenHelper.userId;
  myName = JwtTokenHelper.fullName;
  showFiles = false;
  constructor(
    private _fb: FormBuilder,
    public modalController: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('id') !== null) {
      this.id = this.route.snapshot.paramMap.get('id');
      this.loadData();
    }
  }
  ngOnChanges() {
    this.loadData();
  }
  ngAfterContentChecked() {
    this._cd.detectChanges();
  }
  changeShowFiles() {
    if (this.files.length) this.showFiles = !this.showFiles;
  }
  loadData() {
    this.clientState.isBusy = true;
    this.task = {};
    this._reusableService.onGetById(ApiUrl.taskApi, this.id).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.task = res.content;
        if (this.task.subTasks.length) {
          this.process = parseInt(
            new Intl.NumberFormat('en-IN', {
              maximumSignificantDigits: 2,
            }).format(
              (this.task.subTasks.filter((res) => res.status).length * 100) /
                this.task.subTasks.length
            )
          );
        } else if (this.task.status === 1) {
          this.process = 100;
        } else {
          this.process = 0;
        }
        this.formComment.get('content').disable();
        this.formComment.get('content').enable();
        this.formComment.get('task').setValue(this.task.id);
        this.loadComment(this.task.id);
      },
      () => {
        this.clientState.isBusy = false;
      }
    );
  }
  loadComment(taskId: string) {
    this.clientState.isBusy = true;
    this.comments = [];
    this.files = [];
    this._reusableService
      .onSearch(ApiUrl.comment, { task: taskId })
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.comments = res.content.comments;
        const commentFile = this.comments.filter(
          (comment) => comment.files.length > 0
        );
        commentFile.forEach((file) => {
          this.files.push(file.files[0]);
        });
      });
  }
  async copyTask() {
    const modal = await this.modalController.create({
      component: TaskCreateComponent,
      cssClass: 'modal-task-update',
      componentProps: {
        data: this.task,
      },
    });
    return await modal.present();
  }
  goFile(fileURL: string) {
    window.open(fileURL, '_blank');
  }

  onGoBack() {
    this.navCtrl.navigateBack(['task/history'], { animated: false });
  }
}

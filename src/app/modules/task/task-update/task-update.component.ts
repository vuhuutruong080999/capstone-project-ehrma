import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  AbstractControl,
  FormArray,
} from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-task-update',
  templateUrl: './task-update.component.html',
  styleUrls: ['./task-update.component.scss'],
})
export class TaskUpdateComponent implements OnInit {
  @Input() data: any;
  updateForm: any = this._fb.group({
    name: ['', Validators.required],
    deadline: ['', [Validators.required, this.checkDeadline]],
    description: ['', [Validators.maxLength(500)]],
    priority: [],
    subTasks: this._fb.array([]),
    equipments: this._fb.array([]),
  });
  constructor(
    private _fb: FormBuilder,
    public modalController: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    this.updateForm.patchValue(this.data);

    this.data.subTasks?.forEach((data) => {
      this.setSub(data.name, data.status);
    });
    this.data.equipments?.forEach((data) => {
      this.setEquipment(data.name, data.quantity);
    });
  }
  get name() {
    return this.updateForm.get('name');
  }
  get deadline() {
    return this.updateForm.get('deadline');
  }
  get description() {
    return this.updateForm.get('description');
  }
  onSubmit() {
    if (this.updateForm.valid) {
      this.clientState.isBusy = true;
      const payload = { ...this.data, ...this.updateForm.value };
      this._reusableService
        .onUpdate(ApiUrl.taskApi, payload)
        .subscribe((res) => {
          this.clientState.isBusy = false;
          if (res.content) {
            this._reusableService.onHandleSuccess(res.message);
          } else {
            this._reusableService.onHandleFailed(res.message);
          }
          this.modalController.dismiss(true);
        });
    }
  }

  onDismissModal() {
    this.modalController.dismiss();
  }
  checkDeadline(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      const deadline = control.value;
      const currentTime = new Date();
      if (deadline < currentTime) {
        return { invalidDate: true };
      }
    } else {
      return null;
    }
  }
  getMore() {
    return this._fb.group({
      status: [false],
      name: ['', Validators.required],
    });
  }

  addMore() {
    const control = <FormArray>this.updateForm.controls['subTasks'];
    control.push(this.getMore());
  }

  removeRow(i: number) {
    const control = <FormArray>this.updateForm.controls['subTasks'];
    control.removeAt(i);
  }
  setSub(name: string, status: boolean) {
    const control = <FormArray>this.updateForm.controls['subTasks'];
    control.push(
      this._fb.group({
        name: [name, Validators.required],
        status: [status],
      })
    );
  }
  getMoreEquipment() {
    return this._fb.group({
      quantity: ['', Validators.required],
      name: ['', Validators.required],
    });
  }

  addMoreEquipment() {
    const control = <FormArray>this.updateForm.controls['equipments'];
    control.push(this.getMoreEquipment());
  }

  removeRowEquipment(i: number) {
    const control = <FormArray>this.updateForm.controls['equipments'];
    control.removeAt(i);
  }
  setEquipment(name: string, quantity: number) {
    const control = <FormArray>this.updateForm.controls['equipments'];
    control.push(
      this._fb.group({
        name: [name, Validators.required],
        quantity: [quantity, Validators.required],
      })
    );
  }
}

import { RoleGuard } from './../../core/authentication/role.guard';
import { AuthenticationGuard } from './../../core/authentication/authentication.guard';
import { LayoutComponent } from './layout.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Configs } from '../shared/common/configs/configs';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'home-m',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManageEventFeatureId],
        },

        loadChildren: () =>
          import('../manager-event-modules/home-m/home-m.module').then(
            (m) => m.HomeMModule
          ),
      },
      {
        path: 'home',
        canActivate: [AuthenticationGuard],

        loadChildren: () =>
          import('@home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'home',
        canActivate: [AuthenticationGuard],

        loadChildren: () =>
          import('@home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'report-event',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManageReportEventFeatureId],
        },
        loadChildren: () =>
          import('../report-event/report-event.module').then(
            (m) => m.ReportEventModule
          ),
      },
      {
        path: 'notification',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('../notification/notification.module').then(
            (m) => m.NotificationModule
          ),
      },
      {
        path: 'department',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('@department/department.module').then(
            (m) => m.DepartmentModule
          ),
      },
      {
        path: 'category',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
        },
        loadChildren: () =>
          import('../category/category.module').then((m) => m.CategoryModule),
      },
      {
        path: 'event',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManageEventFeatureId],
        },
        loadChildren: () =>
          import('@event/event.module').then((m) => m.EventModule),
      },
      {
        path: 'participate-event',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('../participate-event/participate-event.module').then(
            (m) => m.ParticipateEventModule
          ),
      },
      {
        path: 'payroll',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManagePayrollFeatureId],
        },
        loadChildren: () =>
          import('@payroll/payroll.module').then((m) => m.PayrollModule),
      },
      {
        path: 'salary',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManagePayrollFeatureId],
        },
        loadChildren: () =>
          import('@salary/salary.module').then((m) => m.SalaryModule),
      },
      {
        path: 'my-salary',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('../my-salary/my-salary.module').then((m) => m.MySalaryModule),
      },
      {
        path: 'my-payroll',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('../my-payroll/my-payroll.module').then(
            (m) => m.MyPayrollModule
          ),
      },

      {
        path: 'role',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('@role/role.module').then((m) => m.RoleModule),
      },
      {
        path: 'staff',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('@staff/staff.module').then((m) => m.StaffModule),
      },
      {
        path: 'subsidy',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManagePayrollFeatureId],
        },
        loadChildren: () =>
          import('@subsidy/subsidy.module').then((m) => m.SubsidyModule),
      },
      {
        path: 'task',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('@task/task.module').then((m) => m.TaskModule),
      },

      {
        path: 'attendance-manage',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [
            Configs.ManageEventFeatureId,
            Configs.ManageStaffFeatureId,
          ],
        },
        loadChildren: () =>
          import('../attendance-manage/attendance-manage.module').then(
            (m) => m.AttendanceManageModule
          ),
      },
      {
        path: 'attendance-history',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('../attendance-history/attendance-history.module').then(
            (m) => m.AttendanceHistoryModule
          ),
      },
      {
        path: 'request',
        canActivate: [AuthenticationGuard],
        loadChildren: () =>
          import('@request/request.module').then((m) => m.RequestModule),
      },
      {
        path: 'request-pending-a',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
        },
        loadChildren: () =>
          import(
            '../admin-modules/request-pending-a/request-pending-a.module'
          ).then((m) => m.RequestPendingAModule),
      },
      {
        path: 'request-pending',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManageRequestFeatureId],
        },
        loadChildren: () =>
          import('../request-pending/request-pending.module').then(
            (m) => m.RequestPendingModule
          ),
      },
      {
        path: 'request-history-a',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
        },
        loadChildren: () =>
          import(
            '../admin-modules/request-history-a/request-history-a.module'
          ).then((m) => m.RequestHistoryAModule),
      },
      {
        path: 'request-history',
        canActivate: [RoleGuard],
        data: {
          expectedId: [Configs.adminRoleId],
          expectedFeatures: [Configs.ManageRequestFeatureId],
        },
        loadChildren: () =>
          import('../request-history/request-history.module').then(
            (m) => m.RequestHistoryModule
          ),
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}

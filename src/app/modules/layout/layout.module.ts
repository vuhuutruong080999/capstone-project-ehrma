import { ProgressBarModule } from './components/progress-bar/progress-bar.module';
import { LayoutComponent } from './layout.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { LayoutRoutingModule } from './layout-routing.module';

@NgModule({
  declarations: [LayoutComponent],
  imports: [SharedModule, ProgressBarModule, LayoutRoutingModule],
})
export class LayoutModule {}

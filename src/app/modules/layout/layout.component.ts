import { Configs } from './../shared/common/configs/configs';
import { JwtTokenHelper } from './../shared/common/jwt-token-helper/jwt-token-helper';
import { animations } from './../shared/animations/animation';
import { Router } from '@angular/router';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: animations,
})
export class LayoutComponent implements OnInit {
  @ViewChild('tp', { static: true }) template: ElementRef;

  featuresToken: any[] = JwtTokenHelper.features;
  userId: string = JwtTokenHelper.userId;
  nameOpen = [];
  active: number = 0;
  sideBars: any[] = JSON.parse(JSON.stringify(Configs.sideBars));
  sideBarAnimation = [];
  refresh = true;
  isAdmin = JwtTokenHelper.isAdmin;
  constructor(private _router: Router, public menu: MenuController) {}

  ngOnInit() {
    if (this.isAdmin) {
      this.sideBars = Configs.sideBarAdmin;
    } else {
      this.onGetSideBar();
    }
  }

  onGetSideBar() {
    if (this.featuresToken) {
      this.sideBars.forEach((sidebar) => {
        if (!sidebar.isShow && this.featuresToken.indexOf(sidebar.id) != -1) {
          sidebar.isShow = true;
        } //end check sidebar exist

        if (sidebar.child) {
          sidebar.child.map((child) => {
            if (this.featuresToken.indexOf(child.id) != -1) {
              return (child.isShow = true);
            }
            return child;
          });
        } //end with side bar child exist
      });
    }

    this.getChild();
  }
  checkOpenTab(name: string): boolean {
    let check = false;

    this.nameOpen.forEach((open) => {
      if (open === name) check = true;
    });
    return check;
  }
  openTab(name: string, index: number, parent: boolean) {
    this.active = index;
    let checkAdd = true;
    if (parent || this.refresh) {
      this.nameOpen.forEach((open) => {
        if (open === name) checkAdd = false;
      });
      if (checkAdd) {
        this.nameOpen.push(name);
      } else {
        this.nameOpen = this.nameOpen.filter((open) => open !== name);
      }
      this.refresh = false;
    }
  }

  getChild() {
    const currentChild = this._router.url.split('/')[1];
    for (let i = 0; i < this.sideBars.length; i++) {
      const tmpChild = this.sideBars[i].child?.find(
        (child) => child.routerlink.split('/')[1] === currentChild
      );
      if (
        tmpChild ||
        currentChild === this.sideBars[i].routerlink?.split('/')[1]
      ) {
        this.openTab(this.sideBars[i].name, i, false);
        break;
      }
    }
  }
  router(link: string) {
    if (link) {
      this._router.navigateByUrl(link);
      if (window.matchMedia('(max-width: 768px)').matches) {
        this.menu.toggle('menu-content');
      }
    }
  }
}

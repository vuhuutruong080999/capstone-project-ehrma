import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-nav-bar-custome',
  templateUrl: './nav-bar-custome.component.html',
  styleUrls: ['./nav-bar-custome.component.scss'],
})
export class NavBarCustomeComponent implements OnInit {
  @Input() title = '';

  constructor(private _location: Location) {}

  ngOnInit() {}

  onBack() {
    this._location.back();
  }
}

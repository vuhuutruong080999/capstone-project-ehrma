import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-nav-bar-modal',
  templateUrl: './nav-bar-modal.component.html',
  styleUrls: ['./nav-bar-modal.component.scss'],
})
export class NavBarModalComponent {
  @Input() title = '';

  @Output() onBackEvent = new EventEmitter<any>();
  @Output() onActionEvent = new EventEmitter<any>();

  constructor() {}

  onBack() {
    this.onBackEvent.emit(event);
  }

  onAction() {
    this.onActionEvent.emit(event);
  }
}

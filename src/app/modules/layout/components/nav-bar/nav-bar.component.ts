import { ModalUserInfoComponent } from './../../../shared/components/modal-user-info/modal-user-info.component';
import { ModalController } from '@ionic/angular';
import { ClientState } from './../../../shared/services/client/client-state';
import { JwtTokenHelper } from './../../../shared/common/jwt-token-helper/jwt-token-helper';
import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { SIZE_TO_MEDIA } from '@ionic/core/dist/collection/utils/media';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  @ViewChild('searchBar') set searchBar(searchbar: ElementRef) {
    if (searchbar) {
      searchbar.nativeElement.focus();
    }
  }

  @Input() title = '';
  @Input() isSearch = false;
  @Input() isToggleSearch = false;
  @Output() onSearchEvent = new EventEmitter<any>();
  avatar = JwtTokenHelper.avatar;
  fullName = JwtTokenHelper.fullName;

  constructor(
    private _router: Router,
    public clientState: ClientState,
    private _modalCtrl: ModalController
  ) {}

  ngOnInit() {}

  toggleMenu() {
    const splitPane = document.querySelector('ion-split-pane');

    if (
      window.matchMedia(SIZE_TO_MEDIA[splitPane.when] || splitPane.when).matches
    )
      splitPane.classList.toggle('split-pane-visible');
  }

  openSearch() {
    this.isToggleSearch = true;
  }

  onSearch(event) {
    this.onSearchEvent.emit(event);
  }

  async onPresentModalInfo() {
    const modal = await this._modalCtrl.create({
      component: ModalUserInfoComponent,
      cssClass: 'modal-user-info',
    });
    return await modal.present();
  }

  logOut() {
    localStorage.clear();
    this.clientState._isBusy = false;
    this._router.navigateByUrl('/auth');
  }
}

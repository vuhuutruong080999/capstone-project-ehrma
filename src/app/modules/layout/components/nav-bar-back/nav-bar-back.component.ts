import { NavController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-nav-bar-back',
  templateUrl: './nav-bar-back.component.html',
  styleUrls: ['./nav-bar-back.component.scss'],
})
export class NavBarBackComponent implements OnInit {
  @Input() title = '';

  constructor(private navCtrl: NavController, private _location: Location) {}

  ngOnInit() {}

  onBack() {
    // this.navCtrl.pop();
    this._location.back();
  }
}

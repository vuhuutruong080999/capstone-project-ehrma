import {
  Component,
  OnInit,
  OnDestroy,
  HostBinding,
  ViewEncapsulation,
} from '@angular/core';
import { Subject } from 'rxjs';
import { ProgressBarService } from './progress-bar.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProgressBarComponent implements OnInit, OnDestroy {
  @HostBinding('class')
  classes = 'app-loading-navigate-indicator';

  bufferValue: number;
  mode: 'determinate' | 'indeterminate' | 'buffer' | 'query';
  value: number;
  visible: boolean;

  private _unsubscribeAll: Subject<any>;

  constructor(private _progressBarService: ProgressBarService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this._progressBarService.bufferValue
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((bufferValue) => {
        this.bufferValue = bufferValue;
      });

    this._progressBarService.mode
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((mode) => {
        this.mode = mode;
      });

    this._progressBarService.value
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((value) => {
        this.value = value;
      });

    this._progressBarService.visible
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((visible) => {
        this.visible = visible;
      });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';

import { ProgressBarComponent } from './progress-bar.component';

@NgModule({
  imports: [SharedModule],
  declarations: [ProgressBarComponent],
  exports: [ProgressBarComponent],
})
export class ProgressBarModule {}

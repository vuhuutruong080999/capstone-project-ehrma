import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { ClientState } from './../../shared/services/client/client-state';
import { RoleCreateComponent } from './../role-create/role-create.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  ModalController,
  AlertController,
  IonInfiniteScroll,
} from '@ionic/angular';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { Configs } from '../../shared/common/configs/configs';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss'],
})
export class RoleListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  checkWidth: any;
  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên chức vụ' },
    { id: 'level', name: 'Thể loại' },
    { id: 'status', name: 'Trạng thái' },
  ];
  listData: any;

  showColumns: string[];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isMobile = false;

  statusList = Configs.status;

  toggleSearch = false;
  isManageRole = JwtTokenHelper.isManage(Configs.ManageRoleFeatureId);

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    if (this.isManageRole) {
      this.displayedColumns = [
        { id: 'name', name: 'Tên chức vụ' },
        { id: 'level', name: 'Thể loại' },
        { id: 'status', name: 'Trạng thái' },
        { id: 'action', name: 'Thao tác' },
      ];
    }

    this.paginator._intl.itemsPerPageLabel = '';

    this.createSearchForm();
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }

    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
      Status: [''],
    });
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService.onSearch(ApiUrl.roleSearch, this.filter).subscribe(
      (res) => {
        let { roles, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.dataSource.data = roles;
          } else {
            this.dataSource.data = this.dataSource.data.concat(roles);
          }
        } else {
          this.dataSource.data = roles;
        }
        this.pagingModel = paging;

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.paginator.pageIndex = 0;
    this.filter.PageIndex = 1;

    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: RoleCreateComponent,
      cssClass: 'modal-role',
      backdropDismiss: false,
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }

        this.dataSource.data.unshift(res.data.response);
        if (this.dataSource.data.length > this.filter.PageSize) {
          this.dataSource.data.pop();
        }

        this.pagingModel.totalRecord = this.pagingModel.totalRecord + 1;
        this.maxiumPage = Math.ceil(
          this.pagingModel.totalRecord / Configs.DefaultPageSize
        );
        this.dataSource._updateChangeSubscription();
      }
    });
    return await modal.present();
  }

  async onUpdate(element: any, index: number) {
    const modal = await this.modalCtrl.create({
      component: RoleCreateComponent,
      cssClass: 'modal-role',
      backdropDismiss: false,
      componentProps: {
        data: element,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.dataSource.data[index] = res.data.response;
        this.dataSource._updateChangeSubscription();
      }
    });

    return await modal.present();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/role/detail/' + element.id], {
      state: element,
    });
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  async onDelete(element: any, index: number) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa chức vụ này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this._reusableService
              .onDelete(ApiUrl.roleDelete, element.id)
              .subscribe(
                (res) => {
                  this.dataSource.data[index].status = 0;

                  this._reusableService.onHandleSuccess(
                    'Xoá chức vụ thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

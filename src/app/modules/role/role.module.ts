import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { RoleRoutingModule } from './role-routing.module';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleSelectUserComponent } from './role-select-user/role-select-user.component';

@NgModule({
  declarations: [
    RoleListComponent,
    RoleDetailComponent,
    RoleCreateComponent,
    RoleSelectUserComponent,
  ],
  imports: [RoleRoutingModule, SharedModule],
})
export class RoleModule {}

import { RoleSelectUserComponent } from './../role-select-user/role-select-user.component';
import { ModalUserPreviewComponent } from './../../shared/components/modal-user-preview/modal-user-preview.component';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import {
  ModalController,
  IonInfiniteScroll,
  NavController,
  AlertController,
} from '@ionic/angular';
import { RoleCreateComponent } from './../role-create/role-create.component';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.scss'],
})
export class RoleDetailComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  model: any = {};
  features: any = [];
  id: string;
  deleteUrl = ApiUrl.roleDelete;
  filter: any = {};
  searchForm: FormGroup;
  showColumns: string[];
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên nhân viên' },
    { id: 'phone', name: 'Số điện thoại' },
    { id: 'email', name: 'Email' },
    { id: 'department', name: 'Phòng ban' },
  ];
  maxiumPage: number;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  isManageRole: boolean = JwtTokenHelper.isManage(Configs.ManageRoleFeatureId);

  constructor(
    private _fb: FormBuilder,
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private modalCtrl: ModalController,
    private _navCtrl: NavController,
    private alertCtrl: AlertController,
    private _router: Router
  ) {}

  ngOnInit() {
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.paginator._intl.itemsPerPageLabel = '';

    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });

    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
      this.features = this.onGetListGroup(this.model.features, 'type');
    } else {
      this.clientState.isBusy = true;
      this.onViewDetail();
    }

    this.createSearchForm();
    this.onSearchStaff();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }

  onViewDetail() {
    this._reusableService.onGetById(ApiUrl.roleGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        this.features = this.onGetListGroup(res.content.features, 'type');
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/role/list']);
      }
    );
  }

  onSearchStaff(event?) {
    this.clientState.isBusy = true;

    if (event) {
      event.target.complete();
    }

    this.filter.RoleId = this.id;
    this._reusableService.onSearch(ApiUrl.staffSearch, this.filter).subscribe(
      (res) => {
        let { users, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.dataSource.data = users;
          } else {
            this.dataSource.data = this.dataSource.data.concat(users);
          }
        } else {
          this.dataSource.data = users;
        }
        this.pagingModel = paging;

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    Object.assign(this.filter, this.searchForm.value);
    this.filter.PageIndex = 1;
    this.onSearchStaff();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearchStaff();
  }

  onGetListGroup(data: any, groupBy: any) {
    let key;
    let listGroup = data.reduce((acc, obj) => {
      key = obj[groupBy];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);

      return acc;
    }, []);

    let returnArr = [];
    for (const [key, value] of Object.entries(listGroup)) {
      let arr = { name: key, value: value };
      returnArr.push(arr);
    }

    return returnArr;
  }

  async onUpdate() {
    const modal = await this.modalCtrl.create({
      component: RoleCreateComponent,
      cssClass: 'modal-role',
      componentProps: {
        data: this.model,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.model = res.data.response;
        this.features = this.onGetListGroup(this.model.features, 'type');
      }
    });
    return await modal.present();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: RoleSelectUserComponent,
      backdropDismiss: false,
      cssClass: 'modal-select-user',
      componentProps: {
        id: this.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;

        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }

        this.onSearchStaff();
      }
    });
    return await modal.present();
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearchStaff(event);
      }
      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }

  async onDelete() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa chức vụ này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.roleDelete, this.id)
              .subscribe(
                (res) => {
                  this.clientState.isBusy = false;
                  this._navCtrl.navigateBack('/role/list');
                  this._reusableService.onHandleSuccess(
                    'Xoá chức vụ thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  async onPresentPreviewUser(id: string) {
    if (!this.isManageRole) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ModalUserPreviewComponent,
      cssClass: 'modal-preview-user',
      componentProps: {
        id: id,
      },
    });

    return await modal.present();
  }

  onBack() {
    this._navCtrl.navigateBack('/role/list', { animated: false });
  }
}

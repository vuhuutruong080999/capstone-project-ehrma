import { ClientState } from './../../shared/services/client/client-state';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormArray,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { ModalController, AlertController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.scss'],
})
export class RoleCreateComponent implements OnInit {
  @Input() data: any;

  isLoading = false;
  createForm: any;
  model: any;
  listFeatures = Configs.LIST_FEATURES;
  roleTypeList = Configs.ROLE_TYPE;
  statusList = Configs.status;
  id: string;

  subStaffCount = 0;
  subEventCount = 0;
  subOtherCount = 0;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createAddForm();

    if (this.data) {
      this.id = this.data.id;
      this.model = this.data;
      this.name.setValue(this.model.name);
      this.level.setValue(this.model.level);
      this.status.setValue(this.model.status);
      this.description.setValue(this.model.description);
      if (this.model.features) {
        this.patchValueFeatures(this.model.features);
      }
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  createAddForm() {
    this.createForm = this._fb.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      status: [''],
      level: ['', Validators.required],
      description: ['', [Validators.maxLength(500)]],
      features: new FormArray([]),
    });
    setTimeout(() => {}, 100);
    this.patchValues();
  }

  patchValues() {
    const formArray = this.createForm.get('features') as FormArray;

    this.listFeatures.forEach((item) => {
      item.feature.forEach((element) => {
        formArray.push(
          new FormGroup({
            id: new FormControl(element.id),
            name: new FormControl(element.name),
            type: new FormControl(element.type),
            checked: new FormControl(element.checked),
          })
        );
      });
    });
  }

  //patch value

  get name() {
    return this.createForm.get('name');
  }

  get status() {
    return this.createForm.get('status');
  }

  get level() {
    return this.createForm.get('level');
  }

  get description() {
    return this.createForm.get('description');
  }

  get arrFeatures() {
    return this.createForm.get('features');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }

    let selectedFeature = this.createForm.value.features.filter(
      (f) => f.checked
    );

    //remove checked field
    let newSelectedFeature = selectedFeature.map(
      ({ checked, ...feature }) => feature
    );

    if (this.id) {
      this.clientState.isBusy = true;
      const payload = {
        id: this.id,
        name: this.name.value,
        status: this.status.value,
        level: this.level.value,
        description: this.description.value,
        features: newSelectedFeature,
      };
      this._reusableService.onUpdate(ApiUrl.roleUpdate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess('Cập nhật chức vụ thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    } else {
      this.clientState.isBusy = true;

      const payload: any = {
        name: this.name.value,
        description: this.description.value,
        level: this.level.value,
        features: newSelectedFeature,
      };
      this._reusableService.onCreate(ApiUrl.roleCreate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess('Tạo mới chức vụ thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    }
  }

  patchValueFeatures(features: any) {
    this.arrFeatures.controls.forEach((element, i) => {
      features.forEach((sub) => {
        if (this.arrFeatures.controls[i].value.id == sub.id) {
          if (sub.type == 'Nhân sự') {
            this.subStaffCount++;
          } else if (sub.type == 'Sự kiện') {
            this.subEventCount++;
          } else {
            this.subOtherCount++;
          }

          this.arrFeatures.controls[i].setValue({
            id: sub.id,
            name: sub.name,
            type: sub.type,
            checked: true,
          });
        }
      });
    });
  }

  onToggleAll(event: MatCheckboxChange, label: string, index: number) {
    this.arrFeatures.markAsDirty();

    if (event.checked) {
      switch (index) {
        case 0:
          this.subStaffCount = 5;
          break;
        case 1:
          this.subEventCount = 4;
          break;
        case 2:
          this.subOtherCount = 3;
          break;
      }

      this.arrFeatures.controls.forEach((element, i) => {
        if (this.arrFeatures.controls[i].value.type == label) {
          this.arrFeatures.controls[i].setValue({
            id: this.arrFeatures.controls[i].value.id,
            name: this.arrFeatures.controls[i].value.name,
            type: this.arrFeatures.controls[i].value.type,
            checked: true,
          });
        }
      });
    } else {
      switch (index) {
        case 0:
          this.subStaffCount = 0;
          break;
        case 1:
          this.subEventCount = 0;
          break;
        case 2:
          this.subOtherCount = 0;
          break;
      }

      this.arrFeatures.controls.forEach((element, i) => {
        if (this.arrFeatures.controls[i].value.type == label) {
          this.arrFeatures.controls[i].setValue({
            id: this.arrFeatures.controls[i].value.id,
            name: this.arrFeatures.controls[i].value.name,
            type: this.arrFeatures.controls[i].value.type,
            checked: false,
          });
        }
      });
    }
  }

  isChecked(index: number) {
    if (
      (index == 0 && this.subStaffCount == 5) ||
      (index == 1 && this.subEventCount == 4) ||
      (index == 2 && this.subOtherCount == 3)
    ) {
      return true;
    }
    return false;
  }

  onToggle(event: MatCheckboxChange, index: number) {
    if (event.checked) {
      switch (index) {
        case 0:
          this.subStaffCount++;
          break;
        case 1:
          this.subEventCount++;
          break;
        case 2:
          this.subOtherCount++;
          break;
      }
    } else {
      switch (index) {
        case 0:
          this.subStaffCount--;
          break;
        case 1:
          this.subEventCount--;
          break;
        case 2:
          this.subOtherCount--;
          break;
      }
    }
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

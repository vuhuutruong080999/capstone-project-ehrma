import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ModalController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Configs } from './../../shared/common/configs/configs';
import { Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.scss'],
})
export class CategoryCreateComponent implements OnInit {
  @Input() id: any;
  isLoading = false;
  createForm: any;
  model: any;
  types = Configs.CATEGORY_TYPE;
  roleList: Observable<any>;
  departmentList: Observable<any>;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createAddForm();
  }

  ionViewWillEnter() {
    if (this.id) {
      this.clientState.isBusy = true;

      this._reusableService
        .onGetById(ApiUrl.categoryGetById, this.id)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.model = res.content;
            this.createForm.patchValue(this.model);
          },
          (error) => {
            this.clientState.isBusy = false;
            this.onDismissModal();
          }
        );
    }
  }

  createAddForm() {
    this.createForm = this._fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(50),
        ],
      ],
      type: ['', [Validators.required]],
    });
  }

  get name() {
    return this.createForm.get('name');
  }
  get type() {
    return this.createForm.get('type');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    if (this.id) {
      //update
      this.clientState.isBusy = true;
      const payload = {
        id: this.id,
        name: this.name.value,
      };

      this._reusableService.onUpdate(ApiUrl.categoryCreate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess('Cập nhật thể loại thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    } else {
      this.clientState.isBusy = true;

      this._reusableService
        .onCreate(ApiUrl.categoryCreate, this.createForm.value)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.modalCtrl.dismiss({ reload: true, response: res.content });
            this._reusableService.onHandleSuccess(
              'Tạo mới thể loại thành công'
            );
          },
          (error) => {
            this.clientState.isBusy = false;
          }
        );
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

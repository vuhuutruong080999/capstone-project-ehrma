import { CategoryCreateComponent } from './category-create/category-create.component';
import { SharedModule } from './../shared/shared.module';
import { CategoryListComponent } from './category-list/category-list.component';
import { NgModule } from '@angular/core';

import { CategoryRoutingModule } from './category-routing.module';

@NgModule({
  declarations: [CategoryListComponent, CategoryCreateComponent],
  imports: [CategoryRoutingModule, SharedModule],
})
export class CategoryModule {}

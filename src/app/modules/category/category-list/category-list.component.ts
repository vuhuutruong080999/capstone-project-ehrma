import { CategoryCreateComponent } from './../category-create/category-create.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ClientState } from './../../shared/services/client/client-state';
import {
  ModalController,
  AlertController,
  IonInfiniteScroll,
} from '@ionic/angular';
import { MatPaginator } from '@angular/material/paginator';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { MatTableDataSource } from '@angular/material/table';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { CategoryService } from '../../shared/services/api/category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
})
export class CategoryListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên thể loại' },
    { id: 'type', name: 'Tính năng' },
    { id: 'action', name: 'Thao tác' },
  ];
  listData = [];
  isMobile = false;
  showColumns = [];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  types = Configs.CATEGORY_TYPE;
  maxiumPage = 0;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    public clientState: ClientState,
    private alertCtrl: AlertController,
    private _reusableService: ReusableService,
    private _categoryService: CategoryService
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.paginator._intl.itemsPerPageLabel = '';
    this.createSearchForm();
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }

    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
      type: [''],
    });
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService
      .onSearch(ApiUrl.categorySearch, this.filter)
      .subscribe(
        (res) => {
          let { categories, ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );
          if (window.innerWidth < Configs.MobileWidth) {
            if (this.filter.PageIndex == 1) {
              this.dataSource.data = categories;
            } else {
              this.dataSource.data = this.dataSource.data.concat(categories);
            }
          } else {
            this.dataSource.data = categories;
          }
          this.pagingModel = paging;

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.filter.PageIndex = 1;
    this.paginator.pageIndex = 0;
    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: CategoryCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-category-create',
    });
    modal.onDidDismiss().then((res) => {
      if (res.data.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }

        this.dataSource.data.unshift(res.data.response);
        if (this.dataSource.data.length > this.filter.PageSize) {
          this.dataSource.data.pop();
        }

        this.pagingModel.totalRecord = this.pagingModel.totalRecord + 1;
        this.maxiumPage = Math.ceil(
          this.pagingModel.totalRecord / Configs.DefaultPageSize
        );
        this.dataSource._updateChangeSubscription();
      }
    });
    return await modal.present();
  }

  async onUpdate(element: any, index: number) {
    const modal = await this.modalCtrl.create({
      component: CategoryCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-category-update',
      componentProps: {
        id: element.id,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.dataSource.data[index] = res.data.response;
        this.dataSource._updateChangeSubscription();
      }
    });

    return await modal.present();
  }

  pageChange(event: any) {
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  async onDelete(element: any) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa thể loại này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._categoryService
              .onDelete(ApiUrl.categoryDelete + `/${element.id}`, element.type)
              .subscribe(
                (res) => {
                  this.onSearch();
                  this._reusableService.onHandleSuccess(
                    'Xoá thể loại thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

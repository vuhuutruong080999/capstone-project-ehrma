import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as Chart from 'chart.js';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-year',
  templateUrl: './year.component.html',
  styleUrls: ['./year.component.scss'],
})
export class YearComponent implements OnInit {
  @ViewChild('eventChart') private eventChartElement: ElementRef;
  @ViewChild('budgetYearChart') private budgetYearChartElement: ElementRef;
  eventChart: any;
  reportYear: any;
  countEvent = [];
  budgetYearChart: any;
  yearControl = this._fb.control(2021);
  labelsMonth = [
    'Tháng 1',
    'Tháng 2',
    'Tháng 3',
    'Tháng 4',
    'Tháng 5',
    'Tháng 6',
    'Tháng 7',
    'Tháng 8',
    'Tháng 9',
    'Tháng 10',
    'Tháng 11',
    'Tháng 12',
  ];
  constructor(
    private _fb: FormBuilder,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {
    this.yearControl.valueChanges.subscribe(() => this.getReportYear());
  }

  ngOnInit(): void {
    this.getReportYear();
  }
  getReportYear() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.report + `/${this.yearControl.value}`)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.reportYear = res.content;
        let numberOfOnScheduleEvent = 0;
        let numberOfNotOnScheduleEvent = 0;
        let numberOfCancelEvent = 0;
        this.countEvent = [];
        this.reportYear.numberOfOnScheduleEvent.forEach((event) => {
          numberOfOnScheduleEvent += event;
        });
        this.reportYear.numberOfNotOnScheduleEvent.forEach((event) => {
          numberOfNotOnScheduleEvent += event;
        });
        this.reportYear.numberOfCancelEvent.forEach((event) => {
          numberOfCancelEvent += event;
        });

        this.countEvent.push(numberOfOnScheduleEvent);
        this.countEvent.push(numberOfNotOnScheduleEvent);
        this.countEvent.push(numberOfCancelEvent);
        this.createEventChart();
        this.createBudgetYearChart();
      });
  }
  createEventChart() {
    if (this.eventChart) {
      this.eventChart.chart.destroy();
    }
    this.eventChart = new Chart(this.eventChartElement.nativeElement, {
      type: 'bar',
      options: {
        layout: {
          padding: 20,
        },
        title: {
          display: true,
          text: `Sự kiện trong năm ${this.yearControl.value}`,
        },
        responsive: true,
        scales: {
          xAxes: [
            {
              stacked: true,
            },
          ],

          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
                stepSize: 1,
              },
            },
          ],
        },
      },
      data: {
        labels: this.labelsMonth,
        datasets: [
          {
            label: 'Hoàn thành đúng tiến độ',
            backgroundColor: '#57A1E5',
            data: this.reportYear.numberOfOnScheduleEvent,
          },
          {
            label: 'Hoàn thành chậm tiến độ',
            backgroundColor: '#F7CF6B',
            data: this.reportYear.numberOfNotOnScheduleEvent,
          },
          {
            label: 'Đã huỷ',
            backgroundColor: '#ED6D85',
            data: this.reportYear.numberOfCancelEvent,
          },
        ],
      },
    });
  }
  createBudgetYearChart() {
    if (this.budgetYearChart) {
      this.budgetYearChart.chart.destroy();
    }
    this.budgetYearChart = new Chart(
      this.budgetYearChartElement.nativeElement,
      {
        type: 'line',
        data: {
          labels: this.labelsMonth,
          datasets: [
            {
              label: 'Thu',
              data: this.reportYear.totalIncome,
              borderColor: '#57A1E5',
              fill: false,
            },
            {
              label: 'Chi',
              data: this.reportYear.totalExpense,
              borderColor: '#ED6D85',
              fill: false,
            },
            {
              label: 'Tổng',
              data: this.reportYear.totalBudget,
              borderColor: '#4bc0c0',
              fill: false,
            },
          ],
        },
        options: {
          layout: {
            padding: 20,
          },
          title: {
            display: true,
            text: `Thu chi năm ${this.yearControl.value}`,
          },
        },
      }
    );
  }
}

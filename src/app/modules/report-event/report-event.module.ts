import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { ChartsModule } from 'ng2-charts';
import { ReportEventComponent } from './report-event.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportEventRoutingModule } from './report-event-routing.module';
import { YearComponent } from './year/year.component';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  declarations: [ReportEventComponent, YearComponent, DetailComponent],
  imports: [
    CommonModule,
    ReportEventRoutingModule,
    SharedModule,
    ChartsModule,
    RoundProgressModule,
  ],
})
export class ReportEventModule {}

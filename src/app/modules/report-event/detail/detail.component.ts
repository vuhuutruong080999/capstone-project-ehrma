import { FormBuilder } from '@angular/forms';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import * as Chart from 'chart.js';
import * as moment from 'moment';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi_VN' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class DetailComponent implements OnInit {
  @ViewChild('budgetChart') private budgetChartElement: ElementRef;
  @ViewChild('staffChart') private staffChartElement: ElementRef;
  @ViewChild('taskChart') private taskChartElement: ElementRef;
  searchForm = this._fb.group({ fromDate: '', toDate: '' });
  eventId = this._fb.control(1);
  budgetChart: any;
  staffChart: any;
  taskChart: any;
  expenses = [];
  incomes = [];
  eventName = [];
  userCount = [];
  users = [];
  completeTask = [];
  cancelTask = [];
  data: any;

  constructor(
    private _fb: FormBuilder,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {
    this.eventId.valueChanges.subscribe(() => {
      this.createTaskChart();
    });
  }

  ngOnInit(): void {
    this.onSearch();
  }
  resetFilter() {
    this.searchForm.reset();
  }
  onSearch() {
    let fromDate;
    if (this.searchForm.get('fromDate').value) {
      fromDate = moment(this.searchForm.get('fromDate').value).format(
        'yyyy-MM-DDTHH:mm'
      );
    }
    let toDate;
    if (this.searchForm.get('toDate').value)
      toDate = moment(this.searchForm.get('toDate').value).format(
        'yyyy-MM-DDTHH:mm'
      );
    const payload = { fromDate: fromDate, toDate: toDate };
    this.clientState.isBusy = true;
    this.expenses = [];
    this.incomes = [];
    this.eventName = [];
    this._reusableService.onSearch(ApiUrl.report, payload).subscribe((res) => {
      this.clientState.isBusy = false;
      this.data = res.content;

      this.data.events?.forEach((res) => {
        this.expenses.push(res.totalExpense);
        this.incomes.push(res.totalIncome);
        this.eventName.push(res.name);
        this.userCount.push(res.users.length);
      });
      this.createBudgetChart();
      this.createStaffChart();
      this.createTaskChart();
    });
  }
  createBudgetChart() {
    if (this.budgetChart) {
      this.budgetChart.chart.destroy();
    }
    this.budgetChart = new Chart(this.budgetChartElement.nativeElement, {
      type: 'bar',
      options: {
        layout: {
          padding: 20,
        },
        title: {
          display: true,
          text: 'Thu chi trong sự kiện',
        },
        tooltips: {
          callbacks: {
            label: (tooltipItems, data) => {
              return (
                data.datasets[tooltipItems.datasetIndex].data[
                  tooltipItems.index
                ] + 'đ'
              );
            },
          },
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        },
      },
      data: {
        labels: this.eventName,
        datasets: [
          {
            label: 'Thu',
            data: this.incomes,
            backgroundColor: '#57A1E5',
          },
          {
            label: 'Chi',
            data: this.expenses,
            backgroundColor: '#ED6D85',
          },
        ],
      },
    });
  }
  createStaffChart() {
    if (this.staffChart) {
      this.staffChart.chart.destroy();
    }
    this.staffChart = new Chart(this.staffChartElement.nativeElement, {
      type: 'bar',
      data: {
        labels: this.eventName,
        datasets: [
          {
            label: 'Nhân viên',
            data: this.userCount,
            backgroundColor: '#5bc0de',
          },
        ],
      },
      options: {
        layout: {
          padding: 20,
        },
        title: {
          display: true,
          text: 'Nhân viên trong sự kiện',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                stepSize: 1,
              },
            },
          ],
        },
      },
    });
  }
  createTaskChart() {
    if (this.taskChart) {
      this.taskChart.chart.destroy();
    }
    this.completeTask = [];
    this.cancelTask = [];
    this.users = [];
    let label = [];
    let text = '';
    if (this.eventId.value == 1) {
      this.data.events.forEach((event) => {
        let totalSuccess = 0;
        let totalCancel = 0;
        event.users.forEach((user) => {
          totalSuccess += user.numberOfFinishTask;
          totalCancel += user.numberOfCancelTask;
        });
        this.completeTask.push(totalSuccess);
        this.cancelTask.push(totalCancel);
      });
      label = this.eventName;
      text = 'Công việc trong sự kiện';
    } else {
      const eventChoice = this.data.events.filter(
        (event) => event.id === this.eventId.value
      )[0];
      eventChoice.users.forEach((user) => {
        this.users.push(user.fullName);
        this.completeTask.push(user.numberOfFinishTask);
        this.cancelTask.push(user.numberOfCancelTask);
      });
      label = this.users;
      text = 'Công việc của nhân viên';
    }
    this.taskChart = new Chart(this.taskChartElement.nativeElement, {
      type: 'bar',
      data: {
        labels: label,
        datasets: [
          {
            label: 'Đã hoàn thành',
            data: this.completeTask,
            backgroundColor: '#57A1E5',
          },
          {
            label: 'Đã huỷ',
            data: this.cancelTask,
            backgroundColor: '#ED6D85',
          },
        ],
      },
      options: {
        layout: {
          padding: 20,
        },
        title: {
          display: true,
          text: text,
        },
        responsive: true,
        scales: {
          xAxes: [
            {
              stacked: true,
            },
          ],

          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
                stepSize: 1,
              },
            },
          ],
        },
      },
    });
  }
}

import { EventSelectUserComponent } from '@event/event-select-user/event-select-user.component';
import { EquipmentListComponent } from './equipment-list/equipment-list.component';
import { BudgetListComponent } from './budget-list/budget-list.component';
import { EventFinishComponent } from './event-finish/event-finish.component';
import { EventMemberAddComponent } from './event-member-add/event-member-add.component';
import { EquipmentCreateComponent } from './equipment-create/equipment-create.component';
import { BudgetCreateComponent } from './budget-create/budget-create.component';
import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';

import { EventRoutingModule } from './event-routing.module';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventCreateComponent } from './event-create/event-create.component';
import { AgmCoreModule } from '@agm/core';
import { EventStaffListComponent } from './event-staff-list/event-staff-list.component';

@NgModule({
  declarations: [
    EventListComponent,
    EventDetailComponent,
    EventCreateComponent,
    BudgetCreateComponent,
    EquipmentCreateComponent,
    EventMemberAddComponent,
    EventFinishComponent,
    BudgetListComponent,
    EquipmentListComponent,
    EventStaffListComponent,
    EventSelectUserComponent,
  ],
  imports: [
    SharedModule,
    EventRoutingModule,
    AgmCoreModule.forRoot({
      //ehrma cap 1
      // apiKey: 'AIzaSyCYtWq7blwEljJMd9eXNgjjWpU0VR32dWE',

      // ehrma-10
      // apiKey: 'AIzaSyAi4IMU-yrGEYqa6bcBRfozt2t8rdTyBxY',

      //ehrma cap 2
      apiKey: 'AIzaSyBfgyyd0S1U0qy4Ddbg14zPn2n8IGSTDak',

      //ehrma cap 3
      // apiKey: 'AIzaSyDRSPudoW6QDavmB7rxyFtYxSZPP2WtOhw',
      libraries: ['places'],
      language: 'vi',
    }),
  ],
})
export class EventModule {}

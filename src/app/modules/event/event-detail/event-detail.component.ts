import { EventCreateComponent } from './../event-create/event-create.component';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { AttendanceModel } from './../../shared/models/attendance/attendance.model';
import { EventFinishComponent } from './../event-finish/event-finish.component';
import { EventService } from './../../shared/services/api/event.service';
import { FormGroup } from '@angular/forms';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { ClientState } from './../../shared/services/client/client-state';
import { ActivatedRoute, Router } from '@angular/router';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss'],
})
export class EventDetailComponent implements OnInit {
  userId = JwtTokenHelper.userId;
  id: string;
  segment = 'detail';
  model: any = {};
  modelReportEvent: any = {};
  labels = [
    'Tên sự kiện',
    'Địa điểm',
    'Ngày diễn ra',
    'Dự kiến kết thúc',
    'Trạng thái',
  ];
  showColumns: string[];
  searchForm: FormGroup;
  deleteUrl = ApiUrl.eventDelete;
  transformCurrency = Configs.TransformCurrency;
  pageSizeList = Configs.PageSizeList;

  isToggleAttendance = false;
  attendanceModel: AttendanceModel = {
    id: null,
    isCheckIn: false,
    isCheckOut: false,
    checkInTime: null,
    checkOutTime: null,
  };
  isCheckIn = false;
  isCheckOut = false;
  isManageEvent = JwtTokenHelper.isManage(Configs.ManageEventFeatureId);
  isManageEquipment = JwtTokenHelper.isManage(Configs.ManageEquipmentFeatureId);
  isManageBudget = JwtTokenHelper.isManage(Configs.ManageBudgetFeatureId);
  isAdmin = JwtTokenHelper.isAdmin;
  r;

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private _geolocation: Geolocation,
    private _eventService: EventService,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });

    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
      if (
        this.model.status == 3 &&
        this.model.users?.indexOf(this.userId) != -1 &&
        !this.isAdmin
      ) {
        this.isToggleAttendance = true;
        this.onCheckAttendance();
      }
    } else {
      this.clientState.isBusy = true;
      this.onViewEventDetail();
    }

    this.onGetReportEvent();
  }

  onViewEventDetail() {
    this.clientState.isBusy = true;

    this._reusableService.onGetById(ApiUrl.eventGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        if (
          this.model.status == 3 &&
          this.model.users.indexOf(this.userId) != -1
        ) {
          this.isToggleAttendance = true;
          this.onCheckAttendance();
        } else {
          this.clientState.isBusy = false;
        }
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/event/list']);
      }
    );
  }

  onGetReportEvent() {
    this._reusableService.onGetById(ApiUrl.reportProcessing, this.id).subscribe(
      (res) => {
        this.modelReportEvent = res.content;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/event/list']);
      }
    );
  }

  onCheckAttendance() {
    this._reusableService.onSearch(ApiUrl.attendanceTodayGet).subscribe(
      (res) => {
        this.attendanceModel.id = res.content.id;
        this.attendanceModel.isCheckIn = res.content.isCheckIn;
        this.attendanceModel.isCheckOut = res.content.isCheckOut;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  async onDelete() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa sự kiện này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;
            this._reusableService
              .onDelete(ApiUrl.eventDelete, this.id)
              .subscribe(
                (res) => {
                  this.clientState.isBusy = false;
                  this._navCtrl.navigateBack('/event/list');
                  this._reusableService.onHandleSuccess(
                    'Xoá sự kiện thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  onBack() {
    this._navCtrl.navigateBack('/event/list', { animated: false });
  }

  async onUpdate() {
    const modal = await this.modalCtrl.create({
      component: EventCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-event',
      componentProps: {
        data: this.model,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.model = res.data.response;
      }
    });

    return await modal.present();
  }

  async onEventProcessing() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn bắt đầu sự kiện này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            const payload = {
              status: 3,
            };
            this.clientState.isBusy = true;
            this._reusableService
              .onUpdate(
                ApiUrl.eventProcessingStatus + `/${this.id}/processing-status`,
                payload
              )
              .subscribe(
                (res) => {
                  this.clientState.isBusy = false;

                  this.model.status = 3;
                  this._reusableService.onHandleSuccess(
                    'Bắt đầu sự kiện thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  async onEventFinish() {
    const modal = await this.modalCtrl.create({
      component: EventFinishComponent,
      backdropDismiss: false,
      cssClass: 'modal-event-finish',
      componentProps: {
        id: this.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.model = res.data.response;
      }
    });
    return await modal.present();
  }

  async onClickAttendance() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: this.attendanceModel.isCheckIn
        ? 'Bạn có chắc muốn tan ca? '
        : 'Bạn có chắc muốn vào ca? ',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.onProcessAttendance();
          },
        },
      ],
    });
    return await alert.present();
  }

  onProcessAttendance() {
    this.clientState.isBusy = true;
    this._geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
      })
      .then((res) => {
        //current location
        const p1 = {
          lat: res.coords.latitude,
          lng: res.coords.longitude,
        };

        this._reusableService
          .onSearch(ApiUrl.eventLocations + `/${this.userId}/locations`, {
            userId: this.userId,
          })
          .subscribe(
            (res) => {
              let isValidRange = false;

              res.content.locations.forEach((item) => {
                const location = item.split(',');
                const p2 = {
                  lat: +location[0],
                  lng: +location[1],
                };
                const distance = this._eventService.getDistance(p1, p2);

                if (distance < Configs.DistanceCheckIn) {
                  isValidRange = true;
                  return;
                }
              });

              if (isValidRange) {
                if (
                  !this.attendanceModel.isCheckIn &&
                  !this.attendanceModel.isCheckOut
                ) {
                  this.onCheckIn(p1);
                } else {
                  this.onCheckOut(p1);
                }
              } else {
                this.clientState.isBusy = false;
                this._reusableService.onHandleFailed('Điểm danh thất bại.');
              }
            },
            (error) => {
              this.clientState.isBusy = false;
            }
          );
      })
      .catch((error) => {
        let message = '';
        if (error.code == 1) {
          message =
            'Không lấy được vị trí hiện tại, vui lòng cho phép ứng dụng truy cập vị trí.';
        } else if (
          !this.attendanceModel.isCheckIn &&
          !this.attendanceModel.isCheckOut
        ) {
          message = 'Vào ca thất bại, vui lòng thử lại sau.';
        } else if (this.attendanceModel.isCheckOut) {
          message = 'Tan ca thất bại, vui lòng thử lại sau.';
        }

        this._reusableService.onHandleFailed(message);

        this.clientState.isBusy = false;
      });
  }

  onCheckIn(location: any) {
    const payload = {
      userId: this.userId,
      eventId: this.id,
      optionCheckIn: location.lat + ',' + location.lng,
    };

    this._reusableService.onCreate(ApiUrl.attendanceCreate, payload).subscribe(
      (res) => {
        this.attendanceModel.id = res.content.id;
        this.attendanceModel.isCheckIn = true;
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess('Vào ca thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onCheckOut(location: any) {
    const payload = {
      id: this.attendanceModel.id,
      optionCheckOut: location.lat + ',' + location.lng,
    };

    this._reusableService.onUpdate(ApiUrl.attendancePut, payload).subscribe(
      (res) => {
        this.attendanceModel.isCheckOut = true;
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess('Tan ca thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  //reload when adding new member to event
  onReloadEvent(event: any) {
    if (event) {
      this.clientState.isBusy = true;

      this._reusableService.onGetById(ApiUrl.eventGetById, this.id).subscribe(
        (res) => {
          this.model = res.content;
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
          this._router.navigate(['/event/list']);
        }
      );
    }
  }

  onViewTotalTask() {
    this._router.navigate(['/event/list']);
  }
}

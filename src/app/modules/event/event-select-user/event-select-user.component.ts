import { ApiUrl } from 'src/app/modules/shared/services/api-url/api-url';
import { ClientState } from 'src/app/modules/shared/services/client/client-state';
import { ReusableService } from 'src/app/modules/shared/services/api/reusable.service';
import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  ModalController,
  AlertController,
  IonSearchbar,
  IonInfiniteScroll,
} from '@ionic/angular';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { startWith, debounceTime, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-event-select-user',
  templateUrl: './event-select-user.component.html',
  styleUrls: ['./event-select-user.component.scss'],
})
export class EventSelectUserComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @ViewChild(IonInfiniteScroll, { static: false })
  infiniteScroll: IonInfiniteScroll;

  @Input() id: string;
  @Input() data: any;

  createForm: FormGroup;
  filter: any = {};
  maxiumPage: number;
  filterDepartment: any;
  filterRole: any;
  userList = [];
  selectedList = [];
  cloneSelectedList = [];
  departmentList = [];
  roleList = [];
  maxiumDepartmentPage: number;
  maxiumRolePage: number;
  isSelectAll = false;
  selectedAllList = [];

  constructor(
    private _fb: FormBuilder,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.createSearchForm();
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;

    // this.selectedList = this.data || [];
    // this.cloneSelectedList = this.selectedList.slice();

    this.name.valueChanges
      .pipe(
        startWith(''),
        debounceTime(300),
        switchMap((value) =>
          this._reusableService
            .onSearch(ApiUrl.staffSearch, {
              DepartmentId: this.department.value,
              RoleId: this.role.value,
              PageIndex: 1,
              PageSize: 10,
              TextSearch: this.name.value,
            })
            .pipe(
              map((value) => {
                this.maxiumPage = Math.ceil(
                  value.content.totalRecord / Configs.DefaultPageSize
                );
                this.filter.PageIndex = 1;
                this.infiniteScroll.complete();
                console.log(this.maxiumPage);

                return value.content.users;
              })
            )
        )
      )
      .subscribe((user) => {
        this.isSelectAll = false;
        console.log(this.infiniteScroll);
        console.log(this.infiniteScroll.disabled);

        this.infiniteScroll.disabled = false;

        this.userList = user.filter((user) => {
          return !this.data.some((userEvent) => user.id == userEvent.id);
        });
      });
    this.onSearchDepartment();
    this.onSearchRole();
  }

  createSearchForm() {
    this.createForm = this._fb.group({
      TextSearch: [''],
      DepartmentId: [''],
      RoleId: [''],
    });
  }

  get name() {
    return this.createForm.get('TextSearch');
  }
  get department() {
    return this.createForm.get('DepartmentId');
  }
  get role() {
    return this.createForm.get('RoleId');
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService.onSearch(ApiUrl.staffSearch, this.filter).subscribe(
      (res) => {
        let { users, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );

        if (this.filter.PageIndex == 1) {
          this.userList = users;
        } else {
          this.userList = this.userList.concat(users);
        }

        this.userList = this.userList.filter((user) => {
          return !this.data.some((userEvent) => user.id == userEvent.id);
        });

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onSearchDepartment(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService
      .onSearch(ApiUrl.departmentSearch, this.filter)
      .subscribe(
        (res) => {
          let { departments, ...paging } = res.content;
          this.maxiumDepartmentPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );
          if (window.innerWidth < Configs.MobileWidth) {
            if (this.filter.PageIndex == 1) {
              this.departmentList = departments;
            } else {
              this.departmentList = this.departmentList.concat(departments);
            }
          } else {
            this.departmentList = departments;
          }

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onSearchRole(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService.onSearch(ApiUrl.roleSearch, this.filter).subscribe(
      (res) => {
        let { roles, ...paging } = res.content;
        this.maxiumRolePage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.roleList = roles;
          } else {
            this.roleList = this.roleList.concat(roles);
          }
        } else {
          this.roleList = roles;
        }
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onSave() {
    if (!this.selectedList.length) {
      return;
    }

    const payload = this.selectedList.map((item) => {
      return item.id;
    });

    this.clientState.isBusy = true;
    this._reusableService
      .onUpdate(ApiUrl.eventAddUser + `/${this.id}/users`, { users: payload })
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess(
            'Thêm mới nhân viên thành công'
          );
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onFilter(event?: any) {
    // this.infiniteScroll.disabled = false;
    this.filter.PageIndex = 1;
    this.clientState.isBusy = true;
    Object.assign(this.filter, this.createForm.value);
    this.onSearch();
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có muốn lưu thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.modalCtrl.dismiss({
                save: true,
                selectedList: this.selectedList,
              });
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onSearchUser(event: any) {
    this.filter.PageIndex = 1;
    this.filter.textSearch = event.detail.value;
    // this.onSearch();
  }

  onSelectAll() {
    this.isSelectAll = !this.isSelectAll;
    this.selectedAllList = this.userList;
    if (this.isSelectAll) {
      this.selectedAllList.forEach((user) => {
        if (!this.selectedList.includes(user)) {
          this.selectedList.push(user);
        }
      });
    } else {
      this.selectedList = this.selectedList.filter((selected) => {
        return !this.selectedAllList.some(
          (selectedAll) => selectedAll.id == selected.id
        );
      });
      this.selectedAllList = [];
    }
    this.cloneSelectedList = this.selectedList.slice();
  }

  onSelected(user: any) {
    let foundIndex = this.selectedList.findIndex(
      (selected) => selected.id == user.id
    );
    if (foundIndex == -1) {
      this.selectedList.push(user);
    } else {
      this.selectedList.splice(foundIndex, 1);
    }
    this.cloneSelectedList = this.selectedList.slice();
  }

  onLoadMoreDepartment() {
    if (
      this.filterDepartment.PageIndex <= this.maxiumDepartmentPage &&
      this.departmentList.length > 0
    ) {
      if (this.maxiumDepartmentPage != 1) {
        this.filterDepartment.PageIndex = this.filterDepartment.PageIndex + 1;
        this.onSearchDepartment();
      }
    }
  }

  onLoadMoreRole() {
    if (
      this.filterRole.PageIndex <= this.maxiumRolePage &&
      this.roleList.length > 0
    ) {
      if (this.maxiumRolePage != 1) {
        this.filterRole.PageIndex = this.filterRole.PageIndex + 1;
        this.onSearchRole();
      }
    }
  }

  loadMoreData(event) {
    console.log('worked load more data');

    if (this.filter?.PageIndex <= this.maxiumPage && this.userList.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;

        this.onSearch(event);
      }

      if (this.filter.PageIndex == this.maxiumPage) {
        console.log('worked disable');

        event.target.disabled = true;
        console.log(this.infiniteScroll.disabled);
      }
    }
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

import { EventSelectUserComponent } from '@event/event-select-user/event-select-user.component';
import { ModalUserPreviewComponent } from './../../shared/components/modal-user-preview/modal-user-preview.component';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { ClientState } from './../../shared/services/client/client-state';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewChild,
  Input,
  EventEmitter,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import {
  ModalController,
  AlertController,
  IonInfiniteScroll,
  NavController,
} from '@ionic/angular';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { Configs } from '../../shared/common/configs/configs';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';

@Component({
  selector: 'app-event-staff-list',
  templateUrl: './event-staff-list.component.html',
  styleUrls: ['./event-staff-list.component.scss'],
})
export class EventStaffListComponent implements OnInit {
  @Input() eventId: string;
  @Input() eventModel: any;
  @Output() reload: EventEmitter<any> = new EventEmitter();
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  checkWidth: any;
  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên nhân viên' },
    { id: 'phone', name: 'Số điện thoại' },
    { id: 'department', name: 'Phòng ban' },
    { id: 'action', name: 'Thao tác' },
  ];
  listData: any;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isMobile = false;
  showColumns: string[];

  statusList = Configs.status;

  toggleSearch = false;
  isManageEvent = JwtTokenHelper.isManage(Configs.ManageEventFeatureId);

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _navCtrl: NavController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      // this.infiniteScroll.disabled = false;
      this.isMobile = true;
      this.filter.PageIndex = 1;
    }
    this.paginator._intl.itemsPerPageLabel = '';
    // this.clientState.isBusy = true;
    this.createSearchForm();
    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService
      .onSearch(ApiUrl.eventSearch + `/${this.eventId}/users`, this.filter)
      .subscribe(
        (res) => {
          let { users, ...paging } = res.content;
          if (window.innerWidth < Configs.MobileWidth) {
            this.maxiumPage = Math.ceil(
              paging.totalRecord / Configs.DefaultPageSize
            );
            if (this.filter.PageIndex == 1) {
              this.dataSource.data = users;
            } else {
              this.dataSource.data = this.dataSource.data.concat(users);
            }
          } else {
            this.dataSource.data = users;
            this.pagingModel = paging;
          }
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.paginator.pageIndex = 0;
    this.filter.PageIndex = 1;

    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    // this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: EventSelectUserComponent,
      backdropDismiss: false,
      cssClass: 'modal-select-user',
      componentProps: {
        data: this.eventModel.users,
        id: this.eventId,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }
        this.reload.emit(true);

        this.onSearch();
      }
    });
    this.clientState.isBusy = true;
    return await modal.present();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  async onDeleteMember(element: any) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa nhân viên này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;

            this._reusableService
              .onUpdate(
                ApiUrl.eventRemoveUser + `/${this.eventId}/remove-user`,
                { userId: element.id }
              )
              .subscribe(
                (res) => {
                  this.onSearch();
                  this.reload.emit(true);

                  this._reusableService.onHandleSuccess(
                    'Xoá nhân viên thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }

  async onPresentPreviewUser(id: string) {
    if (!this.isManageEvent) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ModalUserPreviewComponent,
      cssClass: 'modal-preview-user',
      componentProps: {
        id: id,
      },
    });

    return await modal.present();
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onBack() {
    this._navCtrl.navigateBack('/event/list', { animated: false });
  }
}

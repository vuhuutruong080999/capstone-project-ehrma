import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { IonSearchbar, ModalController, AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';

@Component({
  selector: 'app-event-member-add',
  templateUrl: './event-member-add.component.html',
  styleUrls: ['./event-member-add.component.scss'],
})
export class EventMemberAddComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @Input() id: string;
  @Input() eventModel: any;
  isLoading = false;
  createForm: any;
  model: any;
  staffList = [];
  staffInEvent = [];
  filter = new FilterNormalModel();
  maxiumPage = 0;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createAddForm();
    // this.clientState.isBusy = true;

    this.onGetStaff();
  }

  createAddForm() {
    this.createForm = this._fb.group({
      users: ['', Validators.required],
    });
  }

  get users() {
    return this.createForm.get('users');
  }

  getUser() {
    return this.createForm.get('staff');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;
    this._reusableService
      .onUpdate(ApiUrl.eventAddUser + `/${this.id}/users`, {
        users: this.users.value,
      })
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess(
            'Thêm mới nhân viên thành công'
          );
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onGetStaff() {
    const payload = { ...this.filter };
    this._reusableService
      .onSearch(ApiUrl.departmentUserSearch, payload)
      .subscribe(
        (res) => {
          let { departments, ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );

          let isExist = false;

          departments = departments.filter((department) => {
            department.users = department.users.filter((user) => {
              isExist = this.eventModel.users.some(
                (userEvent) => user.id == userEvent
              );
              if (!isExist) {
                return user;
              }
              return false;
            });
            if (!department.users.length) {
              return false;
            }
            return department;
          });

          if (this.filter.PageIndex == 1) {
            this.staffList = departments;
          } else {
            this.staffList = this.staffList.concat(departments);
          }
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onResetFilter() {
    if (!this.staffList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.TextSearch = '';
      this.onGetStaff();
    }
  }
  onSearch(event: any) {
    this.filter.PageIndex = 1;
    this.filter.TextSearch = event.detail.value;
    this.onGetStaff();
  }

  onLoadMoreData() {
    if (this.filter.PageIndex <= this.maxiumPage && this.staffList.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onGetStaff();
      }
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

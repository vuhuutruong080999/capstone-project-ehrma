import { BudgetCreateComponent } from './../budget-create/budget-create.component';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, OnInit, Input } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { _MatTabGroupBase } from '@angular/material/tabs';

@Component({
  selector: 'app-budget-list',
  templateUrl: './budget-list.component.html',
  styleUrls: ['./budget-list.component.scss'],
})
export class BudgetListComponent implements OnInit {
  @Input() eventId: any;

  budgetList = [];
  totalExpense: number = 0;
  totalIncome: number = 0;
  expenseList = [];
  incomeList = [];
  openIncomeId: string;
  openExpenseId: string;

  constructor(
    private modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    this.onSearch();
  }

  onSearch() {
    this._reusableService
      .onSearch(ApiUrl.budgetSearch, {
        EventId: this.eventId,
      })
      .subscribe(
        (res) => {
          this.budgetList = res.content.budgets;
          this.onFilterBudget(this.budgetList);
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onFilterBudget(budgetList: any) {
    budgetList.forEach((budget) => {
      if (budget.expense) {
        this.totalExpense += budget.price;
        this.expenseList.push(budget);
      } else {
        this.totalIncome += budget.price;
        this.incomeList.push(budget);
      }
    });
  }

  toggleIncomeSub(budgetId: string) {
    if (budgetId != this.openIncomeId) {
      this.openIncomeId = budgetId;
    } else {
      this.openIncomeId = '';
    }
  }
  toggleExpenseSub(budgetId: string) {
    if (budgetId != this.openExpenseId) {
      this.openExpenseId = budgetId;
    } else {
      this.openExpenseId = '';
    }
  }

  async presentBudgetModal(budgetId?) {
    const modal = await this.modalCtrl.create({
      component: BudgetCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-budget',
      componentProps: {
        eventId: this.eventId,
        budgetId: budgetId,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (res.data?.response.expense) {
          this.expenseList.push(res.data.response);
          this.calculateBudgetChange(true);
        } else {
          this.incomeList.push(res.data.response);
          this.calculateBudgetChange();
        }
      }
    });
    return await modal.present();
  }

  async onDelete(budgetId: string, index: number, isExpense = false) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa chi tiêu này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this._reusableService
              .onDelete(ApiUrl.budgetDelete, budgetId)
              .subscribe(
                (res) => {
                  if (isExpense) {
                    this.expenseList.splice(index, 1);
                    this.calculateBudgetChange(true);
                  } else {
                    this.incomeList.splice(index, 1);
                    this.calculateBudgetChange();
                  }

                  this._reusableService.onHandleSuccess(
                    'Xoá chi tiêu thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  async onUpdate(budgetId: string, index: number, isExpense = false) {
    const modal = await this.modalCtrl.create({
      component: BudgetCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-budget',
      componentProps: {
        eventId: this.eventId,
        budgetId: budgetId,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data.reload) {
        if (isExpense) {
          this.expenseList[index] = res.data.response;
          this.calculateBudgetChange(true);
        } else {
          this.incomeList[index] = res.data.response;
          this.calculateBudgetChange();
        }
      }
    });
    return await modal.present();
  }

  calculateBudgetChange(isExpense = false) {
    if (isExpense) {
      this.totalExpense = this.expenseList.reduce((acc, cur) => {
        return acc + cur.price;
      }, 0);
    } else {
      this.totalIncome = this.incomeList.reduce((acc, cur) => {
        return acc + cur.price;
      }, 0);
    }
  }

  onGetListGroup(data: any, groupBy: any) {
    if (data?.length) {
      let key;
      let listGroup = data.reduce((acc, obj) => {
        key = obj[groupBy];
        if (!acc[key]) {
          acc[key] = [];
        }
        acc[key].push(obj);
        return acc;
      }, []);

      let returnArr = [];
      for (const [key, value] of Object.entries(listGroup)) {
        let expense = Object.values(value).reduce((acc, cur) => {
          if (cur.expense) {
            return acc + cur.price;
          }
          return acc;
        }, 0);

        let income = Object.values(value).reduce((acc, cur) => {
          if (cur.expense == false) {
            return acc + cur.price;
          }
          return acc;
        }, 0);

        let arr = { name: key, value: value, expense: expense, income: income };
        returnArr.push(arr);
      }
      return returnArr;
    }
    return data ? data : null;
  }

  onBack() {
    this._navCtrl.navigateBack('/event/list', { animated: false });
  }
}

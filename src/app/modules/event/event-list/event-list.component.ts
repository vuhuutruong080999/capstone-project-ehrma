import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { EventFinishComponent } from './../event-finish/event-finish.component';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';

import { ApiUrl } from './../../shared/services/api-url/api-url';
import { EventCreateComponent } from './../event-create/event-create.component';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { Router } from '@angular/router';
import {
  ModalController,
  AlertController,
  IonInfiniteScroll,
} from '@ionic/angular';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as moment from 'moment';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi_VN' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class EventListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên sự kiện' },
    { id: 'startDate', name: 'Ngày bắt đầu' },
    { id: 'endDate', name: 'Ngày kết thúc' },
    { id: 'status', name: 'Trạng thái' },
    { id: 'address', name: 'Địa điểm' },
  ];
  listData: any;

  showColumns: string[];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter: any = {};
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  status = Configs.EVENT_STATUS;
  maxiumPage: number;
  isMobile = false;
  isManageEvent: boolean = JwtTokenHelper.isManage(
    Configs.ManageEventFeatureId
  );

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    if (this.isManageEvent) {
      this.displayedColumns = [
        { id: 'name', name: 'Tên sự kiện' },
        { id: 'startDate', name: 'Ngày bắt đầu' },
        { id: 'endDate', name: 'Ngày kết thúc' },
        { id: 'address', name: 'Địa điểm' },
        { id: 'status', name: 'Trạng thái' },
        { id: 'action', name: 'Thao tác' },
      ];
    }
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.paginator._intl.itemsPerPageLabel = '';
    this.createSearchForm();
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }
    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
      StatusFilter: [''],
      startDateFilterValue: ['', [this.checkValidStartDate]],
      endDateFilterValue: ['', [this.checkValidEndDate]],
    });
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService.onSearch(ApiUrl.eventSearch, this.filter).subscribe(
      (res) => {
        let { events, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.dataSource.data = events;
          } else {
            this.dataSource.data = this.dataSource.data.concat(events);
          }
        } else {
          this.dataSource.data = events;
        }
        this.pagingModel = paging;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  get startDateFilterValue() {
    return this.searchForm.get('startDateFilterValue');
  }

  get endDateFilterValue() {
    return this.searchForm.get('endDateFilterValue');
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.paginator.pageIndex = 0;
    if (this.searchForm.get('startDateFilterValue').value) {
      const startDate = moment(
        this.searchForm.get('startDateFilterValue').value
      ).format('yyyy-MM-DD');
      this.searchForm.get('startDateFilterValue').setValue(startDate);
    }
    if (this.searchForm.get('endDateFilterValue').value) {
      const endDate = moment(
        this.searchForm.get('endDateFilterValue').value
      ).format('yyyy-MM-DD');
      this.searchForm.get('endDateFilterValue').setValue(endDate);
    }
    this.filter.PageIndex = 1;

    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }
    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  async onUpdate(element: any, index: number) {
    const modal = await this.modalCtrl.create({
      component: EventCreateComponent,
      cssClass: 'modal-event',
      backdropDismiss: false,
      componentProps: {
        data: element,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.dataSource.data[index] = res.data.response;
        this.dataSource._updateChangeSubscription();
      }
    });

    return await modal.present();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/event/detail/' + element.id], { state: element });
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;

    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  async onDelete(element: any, index: number) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa sự kiện này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.clientState.isBusy = true;

            this._reusableService
              .onDelete(ApiUrl.eventDelete, element.id)
              .subscribe(
                (res) => {
                  if (element.status == 3) {
                    this.dataSource.data[index].status = 0;
                    this.clientState.isBusy = false;
                  } else {
                    this.onSearch();
                  }

                  this._reusableService.onHandleSuccess(
                    'Xoá sự kiện thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  loadMoreData(event) {
    if (
      window.innerWidth < Configs.MobileWidth &&
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }
      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }
  checkValidStartDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate;
      let endDate;
      if (control.parent.controls['endDateFilterValue'].value) {
        endDate = moment(
          control.parent.controls['endDateFilterValue'].value
        ).format('yyyy-MM-DD');
      }
      if (control.value) {
        startDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (startDate > endDate) {
        return { startDateLessThanEndDate: true };
      }
    } else {
      return null;
    }
  }

  checkValidEndDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate, endDate;
      if (control.parent.controls['startDateFilterValue'].value) {
        startDate = moment(
          control.parent.controls['startDateFilterValue'].value
        ).format('yyyy-MM-DD');
      }
      if (control.value) {
        endDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (endDate < startDate) {
        return { endDateLessThanStartDate: true };
      }
    } else {
      return null;
    }
  }

  async onEventFinish(element) {
    const modal = await this.modalCtrl.create({
      component: EventFinishComponent,
      backdropDismiss: false,
      cssClass: 'modal-event-finish',
      componentProps: {
        id: element.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        this.onSearch();
      }
    });
    return await modal.present();
  }

  async onEventProcessing(element) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn bắt đầu sự kiện này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            const payload = {
              status: 3,
            };
            this.clientState.isBusy = true;
            this._reusableService
              .onUpdate(
                ApiUrl.eventProcessingStatus +
                  `/${element.id}/processing-status`,
                payload
              )
              .subscribe(
                (res) => {
                  this.onSearch();
                  this._reusableService.onHandleSuccess(
                    'Bắt đầu sự kiện thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: EventCreateComponent,
      cssClass: 'modal-event',
      backdropDismiss: false,
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }

        this.dataSource.data.unshift(res.data.response);
        if (this.dataSource.data.length > this.filter.PageSize) {
          this.dataSource.data.pop();
        }

        this.pagingModel.totalRecord = this.pagingModel.totalRecord + 1;
        this.maxiumPage = Math.ceil(
          this.pagingModel.totalRecord / Configs.DefaultPageSize
        );
        this.dataSource._updateChangeSubscription();
      }
    });
    return await modal.present();
  }

  async onCopyEvent(element: any) {
    const modal = await this.modalCtrl.create({
      component: EventCreateComponent,
      cssClass: 'modal-event',
      backdropDismiss: false,
      componentProps: {
        copy: true,
        dataCopy: element,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
          this.infiniteScroll.disabled = false;
        }

        this.dataSource.data.unshift(res.data.response);
        if (this.dataSource.data.length > this.filter.PageSize) {
          this.dataSource.data.pop();
        }

        this.pagingModel.totalRecord = this.pagingModel.totalRecord + 1;
        this.maxiumPage = Math.ceil(
          this.pagingModel.totalRecord / Configs.DefaultPageSize
        );
        this.dataSource._updateChangeSubscription();
      }
    });
    return await modal.present();
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

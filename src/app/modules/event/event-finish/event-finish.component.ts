import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ModalController, IonSearchbar, AlertController } from '@ionic/angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-event-finish',
  templateUrl: './event-finish.component.html',
  styleUrls: ['./event-finish.component.scss'],
})
export class EventFinishComponent implements OnInit {
  @Input() id: any;
  @ViewChild('searchbar') searchbar: IonSearchbar;

  isLoading = false;
  createForm: any;
  model: any;
  cashierList: Observable<any>;
  filter = new FilterNormalModel();
  maxiumPage: number;

  staffList = [];

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.createAddForm();
    this.onGetStaff();
  }

  ionViewWillEnter() {
    if (!this.id) {
      this.onDismissModal();
    }
  }

  createAddForm() {
    this.createForm = this._fb.group({
      cashier: ['', [Validators.required]],
      note: ['', [Validators.maxLength(500)]],
    });
    setTimeout(() => {}, 100);
  }

  get cashier() {
    return this.createForm.get('cashier');
  }

  get note() {
    return this.createForm.get('note');
  }

  onGetStaff() {
    this._reusableService
      .onSearch(ApiUrl.departmentUserSearch, this.filter)
      .subscribe(
        (res) => {
          let { departments, ...paging } = res.content;
          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );

          if (this.filter.PageIndex == 1) {
            this.staffList = departments;
          } else {
            this.staffList = this.staffList.concat(departments);
          }
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }

    this.clientState.isBusy = true;
    const payload = {
      note: this.note.value,
      cashier: this.cashier.value,
    };

    this._reusableService
      .onUpdate(ApiUrl.eventFinish + `/${this.id}/finished-status`, payload)
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess('Kết thúc sự kiện thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onResetFilter() {
    if (!this.staffList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.TextSearch = '';
      this.onGetStaff();
    }
  }

  onSearch(event: any) {
    this.filter.PageIndex = 1;
    this.filter.TextSearch = event.detail.value;
    this.onGetStaff();
  }

  onLoadMoreData() {
    if (this.filter.PageIndex <= this.maxiumPage && this.staffList.length > 0) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onGetStaff();
      }
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

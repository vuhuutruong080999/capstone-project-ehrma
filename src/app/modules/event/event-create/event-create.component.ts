import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ModalController, IonSearchbar, AlertController } from '@ionic/angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  AfterViewInit,
  ChangeDetectorRef,
} from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import * as moment from 'moment';
import { EventSelectUserComponent } from '@event/event-select-user/event-select-user.component';

@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi_VN' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: Configs.DATE_FORMATS },
  ],
})
export class EventCreateComponent implements OnInit {
  @Input() data: any;
  @Input() dataCopy: any;
  @Input() copy: boolean = false;
  @ViewChild('searchAddress', { static: true }) searchAddress: ElementRef;
  @ViewChild('searchbar') searchbar: IonSearchbar;

  isLoading = false;
  createForm: any;
  autocompleteService: google.maps.places.AutocompleteService;
  latitude: number;
  longitude: number;
  autocomplete: any;
  detail: any;
  statusList = Configs.EVENT_STATUS;
  location: string = '';

  maxiumPage: number = 0;

  options = {
    componentRestrictions: { country: 'vn' },
  };

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.autocompleteService = new google.maps.places.AutocompleteService();

    if (this.data) {
      this.updateForm();
      this.createForm.patchValue(this.data);
    } else {
      this.createAddForm();
    }

    if (this.copy) {
      this.createForm.patchValue(this.dataCopy);
      this.location = this.dataCopy.location;
      this.startDate.reset();
      this.endDate.reset();
      this.description.markAsDirty();
    }
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  ionViewWillEnter() {
    let autocomplete = new google.maps.places.Autocomplete(
      this.searchAddress.nativeElement,
      this.options
    );
    autocomplete.addListener('place_changed', (result) => {
      let place: google.maps.places.PlaceResult = autocomplete.getPlace();
      if (place.geometry === undefined || place.geometry === null) {
        return;
      }
      this.location =
        place.geometry.location.lat().toString() +
        ',' +
        place.geometry.location.lng().toString();
      this.address.setValue(place.formatted_address);
      this.address.setErrors(null);
      this._cd.detectChanges();
    });
  }

  createAddForm() {
    this.createForm = this._fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      startDate: [
        '',
        [
          Validators.required,
          this.checkValidStartDate,
          this.checkDateBigerThanCurrent,
        ],
      ],
      expectedEndDate: [
        '',
        [
          Validators.required,
          this.checkValidEndDate,
          this.checkDateBigerThanCurrent,
        ],
      ],
      address: ['', [Validators.required]],
      description: ['', [Validators.maxLength(500)]],
    });

    setTimeout(() => {}, 100);
  }

  updateForm() {
    this.createForm = this._fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      startDate: ['', [Validators.required, this.checkValidStartDate]],
      expectedEndDate: ['', [Validators.required, this.checkValidEndDate]],
      address: ['', [Validators.required]],
      description: ['', [Validators.maxLength(500)]],
    });

    setTimeout(() => {}, 100);
  }

  get name() {
    return this.createForm.get('name');
  }

  get startDate() {
    return this.createForm.get('startDate');
  }

  get endDate() {
    return this.createForm.get('expectedEndDate');
  }

  get address() {
    return this.createForm.get('address');
  }

  get description() {
    return this.createForm.get('description');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }

    if (this.data) {
      this.clientState.isBusy = true;
      const payload = {
        id: this.data.id,
        // address: this.address.value.description,
        address: this.address.value,
        description: this.description.value,
        startDate: moment(this.startDate.value).format('yyyy-MM-DD'),
        expectedEndDate: moment(this.endDate.value).format('yyyy-MM-DD'),
        name: this.name.value,
        status: this.data.status,
        users: [],
        location: this.location,
        // location: '10.788625,106.691242',
      };
      this._reusableService.onUpdate(ApiUrl.eventUpdate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess('Cập nhật sự kiện thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    } else {
      this.clientState.isBusy = true;

      let endDate = new Date(this.endDate.value);
      let curDate = new Date();
      curDate.setHours(0, 0, 0, 0);

      this.clientState.isBusy = true;

      const payload = {
        address: this.address.value,
        description: this.description.value,
        startDate: moment(this.startDate.value).format('yyyy-MM-DD'),
        expectedEndDate: moment(this.endDate.value).format('yyyy-MM-DD'),
        name: this.name.value,
        users: [],
        status:
          endDate > curDate
            ? 2
            : endDate.getTime() == curDate.getTime()
            ? 3
            : 0,
        location: this.location,
        // location: '10.788625,106.691242',
      };

      this._reusableService.onCreate(ApiUrl.eventCreate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess('Tạo mới sự kiện thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    }
  }

  displayFnAddress(address: any) {
    return address && address.description ? address.description : '';
  }

  checkValidStartDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate;
      let endDate;
      if (control.parent.controls['expectedEndDate'].value) {
        endDate = moment(
          control.parent.controls['expectedEndDate'].value
        ).format('yyyy-MM-DD');
      }
      if (control.value) {
        startDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (startDate > endDate) {
        return { startDateLessThanEndDate: true };
      }
    } else {
      return null;
    }
  }

  checkDateBigerThanCurrent(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (control.value) {
      const checkDate = moment(control.value).format('yyyy-MM-DD');
      const currentDate = moment().format('yyyy-MM-DD');

      if (checkDate < currentDate) {
        return { dateLessThanCurrentDate: true };
      }
    } else {
      return null;
    }
  }

  checkValidEndDate(control: AbstractControl) {
    if (
      control.value == null ||
      control.value == undefined ||
      control.value == ''
    ) {
      return null;
    } else if (!!control.parent) {
      let startDate, endDate;
      if (control.parent.controls['startDate'].value) {
        startDate = moment(control.parent.controls['startDate'].value).format(
          'yyyy-MM-DD'
        );
      }
      if (control.value) {
        endDate = moment(control.value).format('yyyy-MM-DD');
      }
      if (endDate < startDate) {
        return { endDateLessThanStartDate: true };
      }
    } else {
      return null;
    }
  }

  onTypeAddress() {
    this.address.setErrors({ requiredMatch: true });
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

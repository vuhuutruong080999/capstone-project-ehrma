import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { IonSearchbar, ModalController, AlertController } from '@ionic/angular';
import { Configs } from '../../shared/common/configs/configs';
import { FilterNormalModel } from '../../shared/models/filter/filter.model';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-staff-subsidy',
  templateUrl: './staff-subsidy.component.html',
  styleUrls: ['./staff-subsidy.component.scss'],
})
export class StaffSubsidyComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @Input() id: string;
  @Input() subsidy: any;

  createForm: FormGroup;
  transformCurrency = Configs.TransformCurrency;
  model: any;
  subsidyList = [];
  filter = new FilterNormalModel();
  maxiumPage = 0;
  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.createAddForm();
    this.getSubsies();
  }

  createAddForm() {
    this.createForm = this._fb.group({
      userId: [this.id],
      subsidies: [this.subsidy, [Validators.required]],
    });
  }

  getSubsies() {
    this.clientState.isBusy = true;
    const payload = { ...this.filter };
    this._reusableService.onSearch(ApiUrl.subsidy, payload).subscribe((res) => {
      this.clientState.isBusy = false;
      let { subsidies, ...paging } = res.content;
      this.maxiumPage = Math.ceil(paging.totalRecord / Configs.DefaultPageSize);

      if (this.filter.PageIndex == 1) {
        this.subsidyList = subsidies;
      } else {
        this.subsidyList = this.subsidyList.concat(subsidies);
      }
    });
  }

  onResetFilter() {
    if (!this.subsidyList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.TextSearch = '';
      this.getSubsies();
    }
  }
  onSearch(event: any) {
    this.filter.PageIndex = 1;
    this.filter.TextSearch = event.detail.value;
    this.getSubsies();
  }
  onLoadMoreData() {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.subsidyList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.getSubsies();
      }
    }
  }

  onSubmit() {
    this.clientState.isBusy = true;

    const payload = { ...this.createForm.value };

    this._reusableService
      .onCreate(ApiUrl.subsidy + '/add-user-subsidies', payload)
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss(true);
          this._reusableService.onHandleSuccess('Thay đổi trợ cấp thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

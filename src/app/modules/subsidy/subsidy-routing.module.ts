import { SubsidyListComponent } from './subsidy-list/subsidy-list.component';
import { SubsidyDetailComponent } from './subsidy-detail/subsidy-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'detail/:id', component: SubsidyDetailComponent },
  { path: 'list', component: SubsidyListComponent },
  { path: '', redirectTo: 'list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubsidyRoutingModule {}

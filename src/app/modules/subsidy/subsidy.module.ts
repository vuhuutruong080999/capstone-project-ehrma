import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubsidyRoutingModule } from './subsidy-routing.module';
import { SubsidyListComponent } from './subsidy-list/subsidy-list.component';
import { SubsidyDetailComponent } from './subsidy-detail/subsidy-detail.component';
import { SubsidyCreateComponent } from './subsidy-create/subsidy-create.component';
import { SubsidyUpdateComponent } from './subsidy-update/subsidy-update.component';
import { StaffSubsidyComponent } from './staff-subsidy/staff-subsidy.component';

@NgModule({
  declarations: [
    SubsidyListComponent,
    SubsidyDetailComponent,
    SubsidyCreateComponent,
    SubsidyUpdateComponent,
    StaffSubsidyComponent,
  ],
  imports: [CommonModule, SubsidyRoutingModule, SharedModule],
})
export class SubsidyModule {}

import { Component, Input, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ApiUrl } from '../../shared/services/api-url/api-url';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-subsidy-update',
  templateUrl: './subsidy-update.component.html',
  styleUrls: ['./subsidy-update.component.scss'],
})
export class SubsidyUpdateComponent implements OnInit {
  @Input() sub: any;
  isLoading = false;
  formGroup = this._formBuilder.group({
    name: ['', Validators.required],
    value: ['', Validators.required],
  });
  constructor(
    private _formBuilder: FormBuilder,
    public modalController: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    this.formGroup.patchValue(this.sub);
  }

  onSubmit() {
    this.clientState.isBusy = true;
    const payload = { id: this.sub.id, ...this.formGroup.value };
    this._reusableService.onUpdate(ApiUrl.subsidy, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.modalController.dismiss(true);
        this._reusableService.onHandleSuccess('Cập nhật trợ phí thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
  onDismissModal() {
    this.modalController.dismiss();
  }
}

import { ApiUrl } from './../../shared/services/api-url/api-url';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-subsidy-create',
  templateUrl: './subsidy-create.component.html',
  styleUrls: ['./subsidy-create.component.scss'],
})
export class SubsidyCreateComponent implements OnInit {
  isLoading = false;
  formGroup = this._formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(50)]],
    value: ['', [Validators.required, this.lessThanZero]],
  });
  constructor(
    private _formBuilder: FormBuilder,
    public modalController: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {}

  get name() {
    return this.formGroup.get('name');
  }
  get value() {
    return this.formGroup.get('value');
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      return;
    }

    this.clientState.isBusy = true;
    this._reusableService
      .onCreate(ApiUrl.subsidy, this.formGroup.value)
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalController.dismiss(true);
          this._reusableService.onHandleSuccess('Tạo mới trợ phí thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  lessThanZero(control: AbstractControl) {
    if (control.value && control.value <= 0) {
      return { lessThanZero: true };
    }
    return null;
  }

  async ionViewDidEnter() {
    const modal = await this.modalController.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalController.dismiss();
  }
}

import { SubsidyUpdateComponent } from './../subsidy-update/subsidy-update.component';
import { SubsidyCreateComponent } from './../subsidy-create/subsidy-create.component';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { FilterNormalModel } from './../../shared/models/filter/filter.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ModalController } from '@ionic/angular';
import { JwtTokenHelper } from '../../shared/common/jwt-token-helper/jwt-token-helper';
import { Configs } from '../../shared/common/configs/configs';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';

@Component({
  selector: 'app-subsidy-list',
  templateUrl: './subsidy-list.component.html',
  styleUrls: ['./subsidy-list.component.scss'],
})
export class SubsidyListComponent implements OnInit {
  isMobile = false;
  roleLevel = JwtTokenHelper.level;
  displayedColumns: any[] = [
    { id: 'name', name: 'Tên' },
    { id: 'value', name: 'Trợ phí' },
    { id: 'creator', name: 'Người tạo' },
    { id: 'createdDate', name: 'Ngày tạo' },
    { id: 'action', name: 'Thao tác' },
  ];
  searchForm = this._formBuilder.group({ textSearch: '' });
  showColumns: string[];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter = new FilterNormalModel();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isManagerPayroll = JwtTokenHelper.isManage(Configs.ManagePayrollFeatureId);
  transformCurrency = Configs.TransformCurrency;
  constructor(
    private _formBuilder: FormBuilder,
    public modalController: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
  }

  ionViewWillEnter() {
    this.onSearch();
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    this.clientState.isBusy = true;
    this._reusableService.onSearch(ApiUrl.subsidy, this.filter).subscribe(
      (res) => {
        const { subsidies } = res.content;
        this.dataSource.data = subsidies;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
  onFilter(event?: any) {
    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.searchForm.reset();
    this.onFilter();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SubsidyCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-subsidy',
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
        }
        this.onSearch();
      }
    });
    return await modal.present();
  }

  async onUpdate(element: any) {
    const modal = await this.modalController.create({
      component: SubsidyUpdateComponent,
      backdropDismiss: false,
      cssClass: 'modal-subsidy',
      componentProps: {
        sub: element,
      },
    });

    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.clientState.isBusy = true;
        this.onSearch();
      }
    });

    return await modal.present();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/staff/detail/' + element.id]);
  }
}

import { TaskModule } from './../../task/task.module';
import { MatMomentDatetimeModule } from '@mat-datetimepicker/moment';
import { MatDatetimepickerModule } from '@mat-datetimepicker/core';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { SharedModule } from './../../shared/shared.module';
import { HomeMRoutingModule } from './home-m-routing.module';
import { HomeAttendanceMDetailComponent } from './home-attendance-m-detail/home-attendance-m-detail.component';
import { HomeMComponent } from './home-m.component';
import { NgModule } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HomeAttendanceMListComponent } from './home-attendance-list/home-attendance-list.component';

@NgModule({
  declarations: [
    HomeMComponent,
    HomeAttendanceMListComponent,
    HomeAttendanceMDetailComponent,
  ],
  imports: [
    HomeMRoutingModule,
    SharedModule,
    RoundProgressModule,
    MatMomentDatetimeModule,
    MatDatetimepickerModule,
    TaskModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class HomeMModule {}

import { HomeAttendanceMDetailComponent } from './home-attendance-m-detail/home-attendance-m-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeMComponent } from './home-m.component';

const routes: Routes = [
  {
    path: '',
    component: HomeMComponent,
    pathMatch: 'full',
  },
  {
    path: 'attendance-detail/:id',
    component: HomeAttendanceMDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeMRoutingModule {}

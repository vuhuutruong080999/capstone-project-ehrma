import { TaskDetailLinkComponent } from './../../task/task-detail-link/task-detail-link.component';
import { ModalController } from '@ionic/angular';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { ClientState } from './../../shared/services/client/client-state';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { TaskCreateComponent } from '@task/task-create/task-create.component';

@Component({
  selector: 'app-home-m',
  templateUrl: './home-m.component.html',
  styleUrls: ['./home-m.component.scss'],
})
export class HomeMComponent implements OnInit {
  data: any = {};
  eventList = [];
  userInEvent = [];
  taskInEvent = [];
  budgetInEvent = [];
  userCheckIn = [];
  countUserCheckIn = 0;
  userNotCheckIn = [];
  eventControl = this._fb.control('');
  eventId: string = '';
  isDoneInit = false;
  request = 0;
  selectedEvent: any;
  completeSubtask = [];
  isManageEvent = JwtTokenHelper.isManage(Configs.ManageEventFeatureId);
  isManageRequest = JwtTokenHelper.isManage(Configs.ManageRequestFeatureId);

  constructor(
    private _fb: FormBuilder,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    public modalController: ModalController,
    private _router: Router
  ) {}

  ngOnInit() {
    this.loadEvent();
  }

  loadEvent() {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.eventSearch + '?StatusFilter=3')
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.eventList = res.content.events;
        this.eventControl.setValue(this.eventList[0]?.id);

        this.isDoneInit = true;
        this.changeEvent(this.eventList[0].id);
        if (this.eventList.length > 0) {
          this.eventControl.valueChanges.subscribe((res) => {
            this.changeEvent(res);
          });
        }
      });
  }
  changeEvent(eventId) {
    this.eventId = eventId;
    this.selectedEvent = this.eventList.filter(
      (event) => event.id === eventId
    )[0];
    this.loadStaffInEvent(eventId);
    this.loadTaskInEvent(eventId);
    this.loadBudget(eventId);
    this.loadAttendent(eventId);
  }
  loadStaffInEvent(eventId) {
    this.clientState.isBusy = true;
    this._reusableService
      .onGetById(ApiUrl.eventGetById, eventId)
      .subscribe((res) => {
        this.clientState.isBusy = true;

        this.userInEvent = res.content.users;
      });
  }
  loadTaskInEvent(eventId) {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.taskApi, { eventId: eventId, status: 2 })
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.taskInEvent = res.content.tasks;
        this.completeSubtask = [];
        this.taskInEvent.forEach((task) => {
          this.completeSubtask.push(
            task.subTasks.filter((res) => res.status).length
          );
        });
      });
  }
  loadBudget(eventId) {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.budgetSearch, { eventId: eventId })
      .subscribe((res) => {
        this.clientState.isBusy = false;
        this.budgetInEvent = res.content.budgets;
      });
  }
  loadAttendent(eventId) {
    this.clientState.isBusy = true;
    this._reusableService
      .onSearch(ApiUrl.attendanceEventGet + `/${eventId}`)
      .subscribe((res) => {
        this.clientState.isBusy = false;
        const now = moment(new Date()).format('DD/MM/yyyy');
        this.userCheckIn = res.content.userAttendances.filter(
          (user) => user.attendances[now] != 0
        );
        this.countUserCheckIn = this.userCheckIn.length;
        this.userNotCheckIn = res.content.userAttendances.filter(
          (user) => user.attendances[now] == 0
        );
      });
  }
  onViewRequestPending() {
    this._router.navigate(['/request-pending-a/list']);
  }
  onViewTaskDetail(id: string) {
    this._router.navigate(['/task/processing/detail/' + id]);
  }
  onViewEvent() {
    this._router.navigate(['/participate-event/detail/' + this.eventId]);
  }
  onViewAttendent() {
    this._router.navigate(['attendance-manage/detail/' + this.eventId]);
  }
  async showTask(taskId: string) {
    const modal = await this.modalController.create({
      component: TaskDetailLinkComponent,
      cssClass: 'modal-task',
      componentProps: {
        id: taskId,
        isHome: true,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.loadTaskInEvent(this.eventControl.value);
      }
    });
    return await modal.present();
  }
  async createTask(staffId: string) {
    const modal = await this.modalController.create({
      component: TaskCreateComponent,
      cssClass: 'modal-task',
      componentProps: {
        eventId: this.eventId,
        staff: staffId,
      },
    });
    modal.onDidDismiss().then(() => {
      this.loadEvent();
    });
    return await modal.present();
  }
}

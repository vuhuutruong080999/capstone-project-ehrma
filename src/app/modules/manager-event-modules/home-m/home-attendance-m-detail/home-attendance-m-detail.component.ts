import { Configs } from '../../../shared/common/configs/configs';
import { ApiUrl } from './../../../shared/services/api-url/api-url';
import { ReusableService } from './../../../shared/services/api/reusable.service';
import { ClientState } from './../../../shared/services/client/client-state';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-attendance-m-detail',
  templateUrl: './home-attendance-m-detail.component.html',
  styleUrls: ['./home-attendance-m-detail.component.scss'],
})
export class HomeAttendanceMDetailComponent implements OnInit {
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _navCtrl: NavController
  ) {}

  id: string = '';
  eventId: string = '';
  attendanceModel: any = [];
  isMobile = false;

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    this._route.params.subscribe((params) => {
      this.id = params['id'];
      this.eventId = params['eventId'];
    });

    this.onGetAttendance();
  }

  onGetAttendance() {
    this._reusableService
      .onSearch(ApiUrl.attendanceSearch, {
        UserId: this.id,
        EventId: this.eventId,
        SortColumn: 'checkInDateTime',
      })
      .subscribe(
        (res) => {
          this.attendanceModel = res.content.attendances;
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
          this._router.navigate([`/attendance-manage/detail/${this.eventId}`]);
        }
      );
  }

  onBack() {
    this._navCtrl.navigateBack(`/attendance-manage/detail/${this.eventId}`, {
      animated: false,
    });
  }

  doRefresh(event) {
    this.onGetAttendance();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

import { ApiUrl } from './../../../shared/services/api-url/api-url';
import { ClientState } from './../../../shared/services/client/client-state';
import { ReusableService } from './../../../shared/services/api/reusable.service';

import { ModalController, AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-request-process-a',
  templateUrl: './request-process-a.component.html',
  styleUrls: ['./request-process-a.component.scss'],
})
export class RequestProcessAComponent implements OnInit {
  @Input() status: number;
  @Input() id: any;
  isLoading = false;
  createForm: any;
  categoryList = [];
  model: any;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    console.log(this.id);

    this.createAddForm();
  }

  createAddForm() {
    this.createForm = this._fb.group({
      message: ['', [Validators.maxLength(500)]],
    });
    setTimeout(() => {}, 100);
  }

  get message() {
    return this.createForm.get('message');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;
    const payload = {
      id: this.id,
      status: this.status,
      message: this.message.value,
    };

    this._reusableService
      .onUpdate(ApiUrl.requestAccept + `/${this.id}/accept`, payload, false)
      .subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess(
            this.status == 2
              ? 'Chấp thuận yêu cầu thành công'
              : 'Từ chối yêu cầu thành công'
          );
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss(false);
  }
}

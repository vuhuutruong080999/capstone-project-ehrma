import { RequestPendingADetailComponent } from './request-pending-a-detail/request-pending-a-detail.component';
import { RequestPendingAListComponent } from './request-a-pending-list/request-pending-a-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'list' },
  { path: 'list', component: RequestPendingAListComponent },
  { path: 'detail/:id', component: RequestPendingADetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestPendingARoutingModule {}

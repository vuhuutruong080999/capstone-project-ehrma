import { RequestPendingADetailComponent } from './request-pending-a-detail/request-pending-a-detail.component';
import { RequestProcessAComponent } from './request-process-a/request-process-a.component';
import { RequestPendingAListComponent } from './request-a-pending-list/request-pending-a-list.component';
import { RequestPendingARoutingModule } from './request-a-pending-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    RequestPendingAListComponent,
    RequestPendingADetailComponent,
    RequestProcessAComponent,
  ],
  imports: [SharedModule, RequestPendingARoutingModule],
})
export class RequestPendingAModule {}

import { RequestProcessAComponent } from './../request-process-a/request-process-a.component';
import { ApiUrl } from './../../../shared/services/api-url/api-url';
import { ClientState } from './../../../shared/services/client/client-state';
import { ReusableService } from './../../../shared/services/api/reusable.service';
import { JwtTokenHelper } from './../../../shared/common/jwt-token-helper/jwt-token-helper';
import { ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-pending-a-detail',
  templateUrl: './request-pending-a-detail.component.html',
  styleUrls: ['./request-pending-a-detail.component.scss'],
})
export class RequestPendingADetailComponent implements OnInit {
  model: any = {};
  labels = [
    'Người yêu cầu',
    'Người nhận',
    'Trạng thái',
    'Tiêu đề',
    'Loại đơn',
    'Ngày gửi',
  ];
  id: string;

  userId = JwtTokenHelper.userId;

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private modalCtrl: ModalController,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });
    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
      this.id = this.model.id;
    } else {
      this.clientState.isBusy = true;
      this.onViewDetail();
    }
  }

  onViewDetail() {
    this._reusableService.onGetById(ApiUrl.requestGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/role/list']);
      }
    );
  }

  async onAcceptedRequest() {
    const modal = await this.modalCtrl.create({
      component: RequestProcessAComponent,
      cssClass: 'modal-request-process',
      backdropDismiss: false,
      componentProps: {
        status: 2,
        id: this.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.model = res.data.response;
      }
    });
    return await modal.present();
  }

  async onDeniedRequest() {
    const modal = await this.modalCtrl.create({
      component: RequestProcessAComponent,
      cssClass: 'modal-request-process',
      backdropDismiss: false,
      componentProps: {
        status: 0,
        id: this.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.model = res.data.response;
      }
    });
    return await modal.present();
  }

  onBack() {
    this._navCtrl.navigateBack('/request-pending-a/list', { animated: false });
  }
}

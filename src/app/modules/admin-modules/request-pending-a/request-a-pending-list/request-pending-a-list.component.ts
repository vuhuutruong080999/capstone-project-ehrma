import { RequestProcessAComponent } from './../request-process-a/request-process-a.component';
import { ApiUrl } from './../../../shared/services/api-url/api-url';
import { PagingModel } from './../../../shared/models/api-response/api-response';
import { ClientState } from './../../../shared/services/client/client-state';
import { ReusableService } from './../../../shared/services/api/reusable.service';
import { Router } from '@angular/router';
import { ModalController, IonInfiniteScroll } from '@ionic/angular';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Configs } from 'src/app/modules/shared/common/configs/configs';

@Component({
  selector: 'app-request-pending-a-list',
  templateUrl: './request-pending-a-list.component.html',
  styleUrls: ['./request-pending-a-list.component.scss'],
})
export class RequestPendingAListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  showColumns: string[];
  displayedColumns: any[] = [
    { id: 'title', name: 'Tiêu đề' },
    { id: 'categoryName', name: 'Loại đơn' },
    { id: 'name', name: 'Người yêu cầu' },
    { id: 'acceptedName', name: 'Người xử lý' },
    { id: 'createdDate', name: 'Ngày gửi' },
    { id: 'action', name: 'Thao tác' },
  ];
  searchForm: FormGroup;
  filter: any = {};
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isMobile = false;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.paginator._intl.itemsPerPageLabel = '';

    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.filter.Status = 1;
    this.createSearchForm();
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }

    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      TextSearch: [''],
      OwnerName: [''],
      AcceptName: [''],
    });
  }

  doRefresh(event) {
    this.filter.PageIndex = 1;
    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService.onSearch(ApiUrl.requestSearch, this.filter).subscribe(
      (res) => {
        let { requests, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.dataSource.data = requests;
          } else {
            this.dataSource.data = this.dataSource.data.concat(requests);
          }
        } else {
          this.dataSource.data = requests;
        }

        this.pagingModel = paging;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.paginator.pageIndex = 0;
    this.filter.PageIndex = 1;
    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.searchForm.reset();
    this.onFilter();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/request-pending-a/detail/' + element.id], {
      state: element,
    });
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }

  async onAcceptedRequest(element: any, index: number) {
    const modal = await this.modalCtrl.create({
      component: RequestProcessAComponent,
      backdropDismiss: false,
      cssClass: 'modal-request-process',
      componentProps: {
        status: 2,
        id: element.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
        }
        if (this.pagingModel.totalRecord > Configs.DefaultPageSize) {
          this.onSearch();
        } else {
          this.pagingModel.totalRecord = this.pagingModel.totalRecord - 1;
          this.maxiumPage = Math.ceil(
            this.pagingModel.totalRecord / Configs.DefaultPageSize
          );
          this.dataSource.data.splice(index, 1);
          this.dataSource._updateChangeSubscription();
        }
      }
    });
    return await modal.present();
  }

  async onDeniedRequest(element: any, index: number) {
    const modal = await this.modalCtrl.create({
      component: RequestProcessAComponent,
      backdropDismiss: false,
      cssClass: 'modal-request-process',
      componentProps: {
        status: 0,
        id: element.id,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        if (window.innerWidth < Configs.MobileWidth) {
          this.filter.PageIndex = 1;
        }
        if (this.pagingModel.totalRecord > Configs.DefaultPageSize) {
          this.onSearch();
        } else {
          this.pagingModel.totalRecord = this.pagingModel.totalRecord - 1;
          this.maxiumPage = Math.ceil(
            this.pagingModel.totalRecord / Configs.DefaultPageSize
          );
          this.dataSource.data.splice(index, 1);
          this.dataSource._updateChangeSubscription();
        }
      }
    });
    return await modal.present();
  }
}

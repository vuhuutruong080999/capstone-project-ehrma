import { ApiUrl } from './../../../shared/services/api-url/api-url';
import { ClientState } from './../../../shared/services/client/client-state';
import { ReusableService } from './../../../shared/services/api/reusable.service';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-history-a-detail',
  templateUrl: './request-history-a-detail.component.html',
  styleUrls: ['./request-history-a-detail.component.scss'],
})
export class RequestHistoryADetailComponent implements OnInit {
  model: any = {};
  labels = ['Người yêu cầu', 'Trạng thái', 'Tiêu đề', 'Loại đơn', 'Ngày gửi'];
  id: string;

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
    } else {
      this.clientState.isBusy = true;
      this._route.params.subscribe((params) => {
        this.id = params['id'];
      });

      this.onViewDetail();
    }
  }

  onViewDetail() {
    this._reusableService.onGetById(ApiUrl.requestGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/request-history-a/list']);
      }
    );
  }

  onBack() {
    this._navCtrl.navigateBack('/request-history-a/list', { animated: false });
  }
}

import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { RequestHistoryARoutingModule } from './request-history-a-routing.module';
import { RequestHistoryADetailComponent } from './request-history-a-detail/request-history-a-detail.component';
import { RequestHistoryAListComponent } from './request-history-a-list/request-history-a-list.component';

@NgModule({
  declarations: [RequestHistoryAListComponent, RequestHistoryADetailComponent],
  imports: [SharedModule, RequestHistoryARoutingModule],
})
export class RequestHistoryAModule {}

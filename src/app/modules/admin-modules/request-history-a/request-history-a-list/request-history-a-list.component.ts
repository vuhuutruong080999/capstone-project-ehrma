import { JwtTokenHelper } from './../../../shared/common/jwt-token-helper/jwt-token-helper';
import { ApiUrl } from './../../../shared/services/api-url/api-url';
import { PagingModel } from './../../../shared/models/api-response/api-response';
import { ClientState } from './../../../shared/services/client/client-state';
import { ReusableService } from './../../../shared/services/api/reusable.service';
import { Configs } from 'src/app/modules/shared/common/configs/configs';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-request-history-a-list',
  templateUrl: './request-history-a-list.component.html',
  styleUrls: ['./request-history-a-list.component.scss'],
})
export class RequestHistoryAListComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  checkWidth: any;
  searchForm: FormGroup;
  displayedColumns: any[] = [
    { id: 'title', name: 'Tên chức vụ' },
    { id: 'categoryName', name: 'Trạng thái' },
    { id: 'ownerName', name: 'Người yêu cầu' },
    { id: 'acceptedName', name: 'Người xử lý' },
    { id: 'acceptDate', name: 'Ngày duyệt đơn' },
    { id: 'status', name: 'Trạng thái' },
  ];
  listData: any;

  showColumns: string[];
  componentName = this.constructor.name;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  filter: any = {};
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  maxiumPage: number;
  isMobile = false;

  statusList = Configs.REQUEST_HISTORY_STATUS;

  toggleSearch = false;

  userId = JwtTokenHelper.userId;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _router: Router,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.filter.PageIndex = 1;
    this.paginator._intl.itemsPerPageLabel = '';
    this.filter.History = true;
    this.createSearchForm();
  }

  ionViewWillEnter() {
    this.clientState.isBusy = true;
    if (window.innerWidth < Configs.MobileWidth) {
      this.infiniteScroll.disabled = false;
      this.filter.PageIndex = 1;
    }

    this.onSearch();
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      TextSearch: [''],
      OwnerName: [''],
      AcceptName: [''],
      Status: [''],
    });
  }

  onSearch(event?) {
    if (event) {
      event.target.complete();
    }

    this._reusableService.onSearch(ApiUrl.requestSearch, this.filter).subscribe(
      (res) => {
        let { requests, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          //mobile

          if (this.filter.PageIndex == 1) {
            this.dataSource.data = requests;
          } else {
            this.dataSource.data = this.dataSource.data.concat(requests);
          }
        } else {
          //web
          this.dataSource.data = requests;
        }
        this.pagingModel = paging;

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onFilter(event?: any) {
    this.infiniteScroll.disabled = false;
    this.paginator.pageIndex = 0;
    this.filter.PageIndex = 1;

    if (event) {
      Object.assign(this.filter, { TextSearch: event.detail.value });
    } else {
      this.clientState.isBusy = true;
      Object.assign(this.filter, this.searchForm.value);
    }

    this.onSearch();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  onViewDetail(element: any) {
    this._router.navigate(['/request-history-a/detail/' + element.id], {
      state: element,
    });
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onSearch();
  }

  loadMoreData(event) {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.dataSource.data.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onSearch(event);
      }

      if (this.filter.PageIndex === this.maxiumPage) {
        event.target.disabled = true;
      }
    }
  }

  doRefresh(event) {
    this.filter.PageIndex == 1;

    this.onSearch();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequestHistoryAListComponent } from './request-history-a-list/request-history-a-list.component';
import { RequestHistoryADetailComponent } from './request-history-a-detail/request-history-a-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },

  {
    path: 'list',
    component: RequestHistoryAListComponent,
  },
  {
    path: 'detail/:id',
    component: RequestHistoryADetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestHistoryARoutingModule {}

import { ParticipateEventListComponent } from './participate-event-list/participate-event-list.component';
import { ParticipateEventRoutingModule } from './participate-event-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { ParticipateEventDetailComponent } from './participate-event-detail/participate-event-detail.component';
import { ParticipateEquipmentListComponent } from './participate-equipment-list/participate-equipment-list.component';
import { ParticipateBudgetCreateComponent } from './participate-budget-create/participate-budget-create.component';
import { ParticipateEquipmentCreateComponent } from './participate-equipment-create/participate-equipment-create.component';
import { ParticipateBudgetListComponent } from './participate-budget-list/participate-budget-list.component';

@NgModule({
  declarations: [
    ParticipateEventListComponent,
    ParticipateEventDetailComponent,
    ParticipateEquipmentListComponent,
    ParticipateBudgetCreateComponent,
    ParticipateEquipmentCreateComponent,
    ParticipateBudgetCreateComponent,
    ParticipateBudgetListComponent,
  ],
  imports: [SharedModule, ParticipateEventRoutingModule],
})
export class ParticipateEventModule {}

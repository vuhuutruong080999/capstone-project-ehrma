import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ModalController, IonSearchbar } from '@ionic/angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { LoginComponent } from '@auth/login/login.component';

@Component({
  selector: 'app-participate-equipment-create',
  templateUrl: './participate-equipment-create.component.html',
  styleUrls: ['./participate-equipment-create.component.scss'],
})
export class ParticipateEquipmentCreateComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;

  @Input() eventId: any;
  @Input() equipmentId: any;
  isLoading = false;
  createForm: any;
  model: any;
  filter: any = {};
  categoryList = [];
  maxiumPage: number;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.filter.type = 0;

    this.createAddForm();
  }

  ionViewWillEnter() {
    if (this.equipmentId) {
      this.clientState.isBusy = true;

      this._reusableService
        .onGetById(ApiUrl.equipmentGetById, this.equipmentId)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.model = res.content;
            this.createForm.patchValue(this.model);
            this.categoryName.setValue(this.model.categoryId);
          },
          (error) => {
            this.clientState.isBusy = false;
            this.onDismissModal();
          }
        );
    }
  }

  createAddForm() {
    this.createForm = this._fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(50),
        ],
      ],
      categoryName: ['', [Validators.required]],
      quantity: [
        '',
        [Validators.required, Validators.maxLength(10), this.lessThanZero],
      ],
      description: ['', [Validators.maxLength(500)]],
    });

    this.onGetCategory();
  }

  onSearch(event: any) {
    this.filter.PageIndex = 1;
    this.filter.textSearch = event.detail.value;
    this.onGetCategory();
  }

  onGetCategory(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService
      .onSearch(ApiUrl.categorySearch, this.filter)
      .subscribe((res) => {
        let { categories, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.filter.PageIndex == 1) {
          this.categoryList = categories;
        } else {
          this.categoryList = this.categoryList.concat(categories);
        }
      });
  }

  onSearchCategory(event: any) {
    this.filter.PageIndex = 1;
    this.filter.textSearch = event.detail.value;
    this.onGetCategory();
  }

  onResetFilterCategory() {
    if (!this.categoryList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.textSearch = '';
      this.onGetCategory();
    }
  }

  get name() {
    return this.createForm.get('name');
  }

  get categoryName() {
    return this.createForm.get('categoryName');
  }

  get quantity() {
    return this.createForm.get('quantity');
  }

  get description() {
    return this.createForm.get('description');
  }

  onResetFilter() {
    if (!this.categoryList.length) {
      this.searchbar.value = '';
      this.filter.PageIndex = 1;
      this.filter.textSearch = '';
      this.onGetCategory();
    }
  }

  onCreateCategory() {
    this.clientState.isBusy = true;
    const payload = {
      name: this.searchbar.value,
      type: 0,
    };
    this._reusableService.onCreate(ApiUrl.categoryCreate, payload).subscribe(
      (res) => {
        this.categoryList.unshift(res.content);

        this.categoryName.setValue(this.categoryList[0].id);
        this.searchbar.value = '';

        this._reusableService.onHandleSuccess('Tạo mới nhóm thành công');

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }

    if (this.equipmentId) {
      //update
      this.clientState.isBusy = true;
      const payload = {
        id: this.equipmentId,
        eventId: this.eventId,
        name: this.name.value,
        description: this.description.value,
        quantity: this.quantity.value,
        categoryId: this.categoryName.value,
      };
      this._reusableService.onUpdate(ApiUrl.equipmentUpdate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess(
            'Cập nhật trang thiết bị thành công'
          );
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    } else {
      this.clientState.isBusy = true;
      const payload = {
        eventId: this.eventId,
        name: this.name.value,
        description: this.description.value,
        quantity: this.quantity.value,
        categoryId: this.categoryName.value,
      };

      this._reusableService.onCreate(ApiUrl.equipmentCreate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess(
            'Tạo mới trang thiết bị thành công'
          );
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    }
  }

  onLoadMoreCategory() {
    if (
      this.filter.PageIndex <= this.maxiumPage &&
      this.categoryList.length > 0
    ) {
      if (this.maxiumPage != 1) {
        this.filter.PageIndex = this.filter.PageIndex + 1;
        this.onGetCategory(event);
      }
    }
  }

  lessThanZero(control: AbstractControl) {
    if (isNaN(+control.value) || (control.value && control.value <= 0)) {
      return { lessThanZero: true };
    }
    return null;
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

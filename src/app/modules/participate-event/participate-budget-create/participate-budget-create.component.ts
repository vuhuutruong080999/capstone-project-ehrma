import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ClientState } from './../../shared/services/client/client-state';
import { ModalController, IonSearchbar } from '@ionic/angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-participate-budget-create',
  templateUrl: './participate-budget-create.component.html',
  styleUrls: ['./participate-budget-create.component.scss'],
})
export class ParticipateBudgetCreateComponent implements OnInit {
  @ViewChild('searchbar') searchbar: IonSearchbar;
  @Input() eventId: any;
  @Input() budgetId: any;

  isLoading = false;
  createForm: any;
  model: any;
  categoryList = [];
  budgetType = Configs.BUDGET_TYPE;
  maxiumCategoryPage: number;
  filterCategory: any = {};

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    this.filterCategory.PageIndex = 1;
    this.filterCategory.PageSize = Configs.DefaultPageSize;
    this.filterCategory.type = 1;

    this.createAddForm();
  }

  ionViewWillEnter() {
    if (this.budgetId) {
      this.clientState.isBusy = true;

      this._reusableService
        .onGetById(ApiUrl.budgetGetById, this.budgetId)
        .subscribe(
          (res) => {
            this.clientState.isBusy = false;
            this.model = res.content;
            this.createForm.patchValue(this.model);
            this.categoryName.setValue(this.model.categoryId);
            this.expense.setValue(this.model.expense);
          },
          (error) => {
            this.clientState.isBusy = false;
            this.onDismissModal();
          }
        );
    }
  }

  createAddForm() {
    this.createForm = this._fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(50),
        ],
      ],
      price: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(15),
          this.lessThanZero,
        ],
      ],
      expense: ['', Validators.required],
      categoryName: ['', [Validators.required]],
      description: ['', [Validators.maxLength(500)]],
    });

    this.onGetCategory();
  }

  onGetCategory(event?) {
    if (event) {
      event.target.complete();
    }
    this._reusableService
      .onSearch(ApiUrl.categorySearch, this.filterCategory)
      .subscribe((res) => {
        let { categories, ...paging } = res.content;
        this.maxiumCategoryPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (this.filterCategory.PageIndex == 1) {
          this.categoryList = categories;
        } else {
          this.categoryList = this.categoryList.concat(categories);
        }
      });
  }

  onSearchCategory(event: any) {
    this.filterCategory.PageIndex = 1;
    this.filterCategory.textSearch = event.detail.value;
    this.onGetCategory();
  }

  onResetFilterCategory() {
    if (!this.categoryList.length) {
      this.searchbar.value = '';
      this.filterCategory.PageIndex = 1;
      this.filterCategory.textSearch = '';
      this.onGetCategory();
    }
  }

  get name() {
    return this.createForm.get('name');
  }

  get expense() {
    return this.createForm.get('expense');
  }

  get categoryName() {
    return this.createForm.get('categoryName');
  }

  get price() {
    return this.createForm.get('price');
  }

  get description() {
    return this.createForm.get('description');
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }

    if (this.budgetId) {
      //update
      this.clientState.isBusy = true;
      const payload = {
        id: this.budgetId,
        eventId: this.eventId,
        name: this.name.value,
        price: this.price.value,
        expense: this.expense.value,
        categoryId: this.categoryName.value,
      };
      this._reusableService.onUpdate(ApiUrl.budgetUpdate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });
          this._reusableService.onHandleSuccess('Cập nhật thu chi thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    } else {
      this.clientState.isBusy = true;
      const payload = {
        eventId: this.eventId,
        name: this.name.value,
        price: this.price.value,
        expense: this.expense.value,
        description: this.description.value,
        categoryId: this.categoryName.value,
      };

      this._reusableService.onCreate(ApiUrl.budgetCreate, payload).subscribe(
        (res) => {
          this.clientState.isBusy = false;
          this.modalCtrl.dismiss({ reload: true, response: res.content });

          this._reusableService.onHandleSuccess('Tạo mới thu chi thành công');
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
    }
  }

  onCreateCategory() {
    this.clientState.isBusy = true;
    const payload = {
      name: this.searchbar.value,
      type: 1,
    };
    this._reusableService.onCreate(ApiUrl.categoryCreate, payload).subscribe(
      (res) => {
        this.categoryList.unshift(res.content);

        this.categoryName.setValue(this.categoryList[0].id);
        this.searchbar.value = '';

        this._reusableService.onHandleSuccess('Tạo mới thể nhóm thành công');

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onLoadMoreCategory() {
    if (
      this.filterCategory.PageIndex <= this.maxiumCategoryPage &&
      this.categoryList.length > 0
    ) {
      if (this.maxiumCategoryPage != 1) {
        this.filterCategory.PageIndex = this.filterCategory.PageIndex + 1;
        this.onGetCategory(event);
      }
    }
  }

  lessThanZero(control: AbstractControl) {
    if (isNaN(+control.value) || (control.value && control.value <= 0)) {
      return { lessThanZero: true };
    }
    return null;
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

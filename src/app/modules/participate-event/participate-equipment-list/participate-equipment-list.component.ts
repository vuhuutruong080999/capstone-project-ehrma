import { animations } from './../../shared/animations/animation';
import { ClientState } from './../../shared/services/client/client-state';
import { ReusableService } from './../../shared/services/api/reusable.service';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { ParticipateEquipmentCreateComponent } from '../participate-equipment-create/participate-equipment-create.component';

@Component({
  selector: 'app-participate-equipment-list',
  templateUrl: './participate-equipment-list.component.html',
  styleUrls: ['./participate-equipment-list.component.scss'],
  animations: animations,
})
export class ParticipateEquipmentListComponent implements OnInit {
  @Input() eventId: any;

  equipmentListGroup = [];
  isOpen: string = '';
  filter: any = {};
  equipmentList = [];
  groupNameOpen: string;

  constructor(
    private modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private alertCtrl: AlertController,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    this.onSearch();
  }

  onSearch() {
    this._reusableService
      .onSearch(ApiUrl.equipmentSearch, { EventId: this.eventId })
      .subscribe(
        (res) => {
          this.equipmentList = res.content.equipments;
          this.equipmentListGroup = this.onGetListGroup(
            this.equipmentList,
            'categoryName'
          );

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  onOpenGroup(groupName: any) {
    if (groupName != this.groupNameOpen) {
      this.groupNameOpen = groupName;
    } else {
      this.groupNameOpen = '';
    }
  }

  onOpen(equipmentId: string) {
    if (equipmentId != this.isOpen) {
      this.isOpen = equipmentId;
    } else {
      this.isOpen = '';
    }
  }

  async presentEquipmentModal(equipmentId?: string) {
    const modal = await this.modalCtrl.create({
      component: ParticipateEquipmentCreateComponent,
      backdropDismiss: false,
      cssClass: 'modal-equipment',
      componentProps: {
        eventId: this.eventId,
        equipmentId: equipmentId,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data?.reload) {
        this.onSearch();
        this.equipmentList.unshift(res.data.response);
        this.equipmentListGroup = this.onGetListGroup(
          this.equipmentList,
          'categoryName'
        );
      }
    });
    return await modal.present();
  }

  async onUpdate(equipmentId: string, groupIndex: number, subIndex: number) {
    const modal = await this.modalCtrl.create({
      component: ParticipateEquipmentCreateComponent,
      cssClass: 'modal-equipment',
      backdropDismiss: false,
      componentProps: {
        eventId: this.eventId,
        equipmentId: equipmentId,
      },
    });
    modal.onDidDismiss().then((res) => {
      if (res.data.reload) {
        this.equipmentListGroup[groupIndex].value[subIndex] = res.data.response;
      }
    });
    return await modal.present();
  }

  async onDelete(equipmentId: string, groupIndex: number, subIndex: number) {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: 'Bạn có chắc muốn xóa trang thiết bị này?',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this._reusableService
              .onDelete(ApiUrl.equipmentDelete, equipmentId)
              .subscribe(
                (res) => {
                  this.equipmentListGroup[groupIndex].value.splice(subIndex, 1);

                  if (this.equipmentListGroup[groupIndex].value == 0) {
                    this.equipmentListGroup.splice(groupIndex, 1);
                  }
                  this._reusableService.onHandleSuccess(
                    'Xoá trang thiết bị thành công'
                  );
                },
                (error) => {
                  this.clientState.isBusy = false;
                }
              );
          },
        },
      ],
    });
    return await alert.present();
  }

  onGetListGroup(data: any, groupBy: any) {
    if (data.length) {
      let key;
      let listGroup = data.reduce((acc, obj) => {
        key = obj[groupBy];
        if (!acc[key]) {
          acc[key] = [];
        }
        acc[key].push(obj);
        return acc;
      }, []);

      let returnArr = [];
      for (const [key, value] of Object.entries(listGroup)) {
        let arr = { name: key, value: value };
        returnArr.push(arr);
      }
      return returnArr;
    }
    return data;
  }

  onBack() {
    this._navCtrl.navigateBack('/participate-event/list', { animated: false });
  }
}

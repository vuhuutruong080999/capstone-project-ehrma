import { ModalUserPreviewComponent } from './../../shared/components/modal-user-preview/modal-user-preview.component';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { AttendanceModel } from './../../shared/models/attendance/attendance.model';
import { EventService } from './../../shared/services/api/event.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { ClientState } from './../../shared/services/client/client-state';
import { ActivatedRoute, Router } from '@angular/router';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-participate-event-detail',
  templateUrl: './participate-event-detail.component.html',
  styleUrls: ['./participate-event-detail.component.scss'],
})
export class ParticipateEventDetailComponent implements OnInit {
  userId = JwtTokenHelper.userId;
  id: string;
  segment = 'detail';
  model: any = {};
  labels = [
    'Tên sự kiện',
    'Địa điểm',
    'Ngày diễn ra',
    'Dự kiến kết thúc',
    'Trạng thái',
  ];
  searchForm: FormGroup;
  deleteUrl = ApiUrl.eventDelete;
  transformCurrency = Configs.TransformCurrency;
  isToggleAttendance = false;
  attendanceModel: AttendanceModel = {
    id: null,
    isCheckIn: false,
    isCheckOut: false,
    checkInTime: null,
    checkOutTime: null,
  };
  isCheckIn = false;
  isCheckOut = false;
  avatar = JwtTokenHelper.avatar;

  isManageEvent = JwtTokenHelper.isManage(Configs.ManageEventFeatureId);
  isManageEquipment = JwtTokenHelper.isManage(Configs.ManageEquipmentFeatureId);
  isManageBudget = JwtTokenHelper.isManage(Configs.ManageBudgetFeatureId);

  isAdmin = JwtTokenHelper.isAdmin;

  constructor(
    private _fb: FormBuilder,
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private modalCtrl: ModalController,
    private _geolocation: Geolocation,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    this._route.params.subscribe((params) => {
      this.id = params['id'];
    });

    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
      if (
        this.model.status == 3 &&
        this.model.users.indexOf(this.userId) != -1
      ) {
        this.isToggleAttendance = true;
        this.onCheckAttendance();
      }
    } else {
      this.clientState.isBusy = true;
      this.onViewEventDetail();
    }
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }

  onViewEventDetail() {
    this.clientState.isBusy = true;

    this._reusableService.onGetById(ApiUrl.eventGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        if (
          this.model.status == 3 &&
          this.model.users.indexOf(this.userId) != -1
        ) {
          this.isToggleAttendance = true;
          this.onCheckAttendance();
        }
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/event/list']);
      }
    );
  }

  onCheckAttendance() {
    this._reusableService.onSearch(ApiUrl.attendanceTodayGet).subscribe(
      (res) => {
        this.attendanceModel.id = res.content.id;
        this.attendanceModel.isCheckIn = res.content.isCheckIn;
        this.attendanceModel.isCheckOut = res.content.isCheckOut;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onBack() {
    this._navCtrl.navigateBack('/participate-event/list', { animated: false });
  }

  onProcessAttendance() {
    this.clientState.isBusy = true;
    this._geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
      })
      .then((res) => {
        //current location
        const p1 = {
          lat: res.coords.latitude,
          lng: res.coords.longitude,
        };

        this._reusableService
          .onSearch(ApiUrl.eventLocations + `/${this.userId}/locations`, {
            userId: this.userId,
          })
          .subscribe(
            (res) => {
              let isValidRange = false;

              // res.content.locations.forEach((item) => {
              //   const location = item.split(',');
              //   const p2 = {
              //     lat: +location[0],
              //     lng: +location[1],
              //   };
              //   const distance = this._eventService.getDistance(p1, p2);

              //   if (distance < Configs.DistanceCheckIn) {
              //     isValidRange = true;
              //     return;
              //   }
              // });

              // if (isValidRange) {
              if (true) {
                if (
                  !this.attendanceModel.isCheckIn &&
                  !this.attendanceModel.isCheckOut
                ) {
                  this.onCheckIn(p1);
                } else {
                  this.onCheckOut(p1);
                }
              } else {
                this.clientState.isBusy = false;
                this._reusableService.onHandleFailed('Điểm danh thất bại.');
              }
            },
            (error) => {
              this.clientState.isBusy = false;
            }
          );
      })
      .catch((error) => {
        let message = '';
        if (error.code == 1) {
          message =
            'Không lấy được vị trí hiện tại, vui lòng cho phép ứng dụng truy cập vị trí.';
        } else if (
          !this.attendanceModel.isCheckIn &&
          !this.attendanceModel.isCheckOut
        ) {
          message = 'Vào ca thất bại, vui lòng thử lại sau.';
        } else if (this.attendanceModel.isCheckOut) {
          message = 'Tan ca thất bại, vui lòng thử lại sau.';
        }

        this._reusableService.onHandleFailed(message);

        this.clientState.isBusy = false;
      });
  }

  onCheckIn(location: any) {
    const payload = {
      userId: this.userId,
      eventId: this.id,
      optionCheckIn: location.lat + ',' + location.lng,
    };

    this._reusableService.onCreate(ApiUrl.attendanceCreate, payload).subscribe(
      (res) => {
        this.attendanceModel.id = res.content.id;
        this.attendanceModel.isCheckIn = true;
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess('Vào ca thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onCheckOut(location: any) {
    const payload = {
      id: this.attendanceModel.id,
      optionCheckOut: location.lat + ',' + location.lng,
    };

    this._reusableService.onUpdate(ApiUrl.attendancePut, payload).subscribe(
      (res) => {
        this.attendanceModel.isCheckOut = true;
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess('Tan ca thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  async onPresentPreviewUser(id: string) {
    if (!this.isManageEvent) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ModalUserPreviewComponent,
      cssClass: 'modal-preview-user',
      componentProps: {
        id: id,
      },
    });

    return await modal.present();
  }
}

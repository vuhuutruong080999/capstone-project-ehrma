import { ParticipateEventListComponent } from './participate-event-list/participate-event-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParticipateEventDetailComponent } from './participate-event-detail/participate-event-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },

  {
    path: 'list',
    component: ParticipateEventListComponent,
  },
  { path: 'detail/:id', component: ParticipateEventDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParticipateEventRoutingModule {}

import { SharedPipes } from './share-pipes';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ModalUserInfoComponent } from './components/modal-user-info/modal-user-info.component';
import { ModalUserPreviewComponent } from './components/modal-user-preview/modal-user-preview.component';
import { SharedDirectives } from './shared-directives';
import { NavBarBackComponent } from './../layout/components/nav-bar-back/nav-bar-back.component';
import { NavBarComponent } from './../layout/components/nav-bar/nav-bar.component';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from './material.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarCustomeComponent } from '../layout/components/nav-bar-custom/nav-bar-custome.component';
import { NavBarModalComponent } from '../layout/components/nav-bar-modal/nav-bar-modal.component';
import { SharedComponents } from './shared-components';

const MODULES = [CommonModule, FormsModule, MaterialModule, IonicModule];

const DECLARES = [
  NavBarBackComponent,
  NavBarComponent,
  NavBarCustomeComponent,
  NavBarModalComponent,
  SharedComponents,
  SharedDirectives,
  SharedPipes,
  ModalUserPreviewComponent,
  ModalUserInfoComponent,
  ChangePasswordComponent,
];

@NgModule({
  declarations: [...DECLARES],
  imports: [...MODULES],
  exports: [...MODULES, DECLARES],
})
export class SharedModule {}

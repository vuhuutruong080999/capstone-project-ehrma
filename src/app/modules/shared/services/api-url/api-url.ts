import { environment } from './../../../../../environments/environment.prod';
export class ApiUrl {
  static BaseUrl = environment.serverUrl;
  //Login

  public static login = ApiUrl.BaseUrl + '/authentication';
  public static forgetPassword = ApiUrl.BaseUrl + '/user/forget-password';
  public static defaultPassword = ApiUrl.BaseUrl + '/user/default-password';

  // Role
  public static roleSearch = ApiUrl.BaseUrl + '/role';
  public static roleCreate = ApiUrl.BaseUrl + '/role';
  public static roleUpdate = ApiUrl.BaseUrl + '/role';
  public static roleDelete = ApiUrl.BaseUrl + '/role';
  public static roleGetById = ApiUrl.BaseUrl + '/role';
  public static roleAddMember = ApiUrl.BaseUrl + '/user/roles';
  // Department
  public static departmentSearch = ApiUrl.BaseUrl + '/department';
  public static departmentCreate = ApiUrl.BaseUrl + '/department';
  public static departmentDelete = ApiUrl.BaseUrl + '/department';
  public static departmentGetById = ApiUrl.BaseUrl + '/department';
  public static departmentAddMember = ApiUrl.BaseUrl + '/user/departments';

  // Event
  public static eventSearch = ApiUrl.BaseUrl + '/event';
  public static eventStaffSearch = ApiUrl.BaseUrl + '/event';
  public static eventCreate = ApiUrl.BaseUrl + '/event';
  public static eventUpdate = ApiUrl.BaseUrl + '/event';
  public static eventDelete = ApiUrl.BaseUrl + '/event';
  public static eventGetById = ApiUrl.BaseUrl + '/event';
  public static eventFinish = ApiUrl.BaseUrl + '/event';
  public static eventProcessingStatus = ApiUrl.BaseUrl + '/event';
  public static eventRemoveUser = ApiUrl.BaseUrl + '/event';
  public static eventAddUser = ApiUrl.BaseUrl + '/event';
  public static eventLocations = ApiUrl.BaseUrl + '/event';

  // Notification
  public static notificationSearch = ApiUrl.BaseUrl + '/notification';
  public static notificationCreate = ApiUrl.BaseUrl + '/notification';
  public static notificationUpdate = ApiUrl.BaseUrl + '/notification';
  public static notificationDelete = ApiUrl.BaseUrl + '/notification';
  public static notificationGetById = ApiUrl.BaseUrl + '/notification';

  // Task
  public static taskApi = ApiUrl.BaseUrl + '/task';
  public static numberOfProcessingTask =
    ApiUrl.BaseUrl + '/number-of-processing-task';

  //Budget
  public static budgetSearch = ApiUrl.BaseUrl + '/budget';
  public static budgetCreate = ApiUrl.BaseUrl + '/budget';
  public static budgetDelete = ApiUrl.BaseUrl + '/budget';
  public static budgetUpdate = ApiUrl.BaseUrl + '/budget';
  public static budgetGetById = ApiUrl.BaseUrl + '/budget';

  //Equipment
  public static equipmentSearch = ApiUrl.BaseUrl + '/equipment';
  public static equipmentCreate = ApiUrl.BaseUrl + '/equipment';
  public static equipmentDelete = ApiUrl.BaseUrl + '/equipment';
  public static equipmentUpdate = ApiUrl.BaseUrl + '/equipment';
  public static equipmentGetById = ApiUrl.BaseUrl + '/equipment';

  //Request
  public static myRequestSearch = ApiUrl.BaseUrl + '/request/my-request';
  public static requestSearch = ApiUrl.BaseUrl + '/request';
  public static requestCreate = ApiUrl.BaseUrl + '/request';
  public static requestGetById = ApiUrl.BaseUrl + '/request';
  public static requestUpdate = ApiUrl.BaseUrl + '/request';
  public static requestAccept = ApiUrl.BaseUrl + '/request';

  //Staff
  public static staffApprover = ApiUrl.BaseUrl + '/user/approver';
  public static staffActiveAccount = ApiUrl.BaseUrl + '/user';
  public static departmentUserSearch = ApiUrl.BaseUrl + '/user/department-user';
  public static staffSearch = ApiUrl.BaseUrl + '/user';
  public static staffCreate = ApiUrl.BaseUrl + '/user';
  public static staffUpdate = ApiUrl.BaseUrl + '/user';
  public static staffDelete = ApiUrl.BaseUrl + '/user';
  public static staffGetById = ApiUrl.BaseUrl + '/user';

  //Category
  public static categorySearch = ApiUrl.BaseUrl + '/category';
  public static categoryCreate = ApiUrl.BaseUrl + '/category';
  public static categoryUpdate = ApiUrl.BaseUrl + '/category';
  public static categoryDelete = ApiUrl.BaseUrl + '/category';
  public static categoryGetById = ApiUrl.BaseUrl + '/category';
  //comment
  public static comment = ApiUrl.BaseUrl + '/comment';

  //Attendance
  public static attendanceSearch = ApiUrl.BaseUrl + '/attendance';
  public static attendanceTodayGet = ApiUrl.BaseUrl + '/attendance/today';
  public static attendanceCreate = ApiUrl.BaseUrl + '/attendance';
  public static attendanceUpdate = ApiUrl.BaseUrl + '/attendance/manager';
  public static attendanceEventGet = ApiUrl.BaseUrl + '/attendance/event';
  public static attendancePut = ApiUrl.BaseUrl + '/attendance';
  public static attendanceGetById = ApiUrl.BaseUrl + '/attendance';
  //report
  public static reportProcessing = ApiUrl.BaseUrl + '/report/processing-event';
  public static report = ApiUrl.BaseUrl + '/report';
  //payload
  public static payroll = ApiUrl.BaseUrl + '/payroll';
  //salary
  public static salary = ApiUrl.BaseUrl + '/salary';
  //subsidy
  public static subsidy = ApiUrl.BaseUrl + '/subsidy';
}

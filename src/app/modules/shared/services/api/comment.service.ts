import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  public defaultHeaders = new HttpHeaders();
  constructor(private _http: HttpClient) {}

  private canConsumeForm(consumes: string[]): boolean {
    const form = 'multipart/form-data';
    for (const consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }
  public commentPut(
    apiUrl: string,
    requestParameters: any,
    observe: any = 'body',
    reportProgress: boolean = false,
    options?: { httpHeaderAccept?: undefined }
  ): Observable<any> {
    const files = requestParameters.files;
    const task = requestParameters.task;
    const content = requestParameters.content;
    let headers = this.defaultHeaders;

    let httpHeaderAcceptSelected: string | undefined =
      options && options.httpHeaderAccept;
    if (httpHeaderAcceptSelected === undefined) {
      // to determine the Accept header
      const httpHeaderAccepts: string[] = [];
      httpHeaderAcceptSelected = 'multipart/form-data';
    }
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = ['multipart/form-data'];

    const canConsumeForm = this.canConsumeForm(consumes);

    let formParams: { append(param: string, value: any): any };
    let useForm = false;
    let convertFormParamsToString = false;
    useForm = canConsumeForm;
    formParams = new FormData();

    if (files) {
      if (useForm) {
        files.forEach((element) => {
          formParams =
            (formParams.append('Files', <any>element) as any) || formParams;
        });
      } else {
        formParams =
          (formParams.append('Files', files.join(',')) as any) || formParams;
      }
    }
    if (task !== undefined) {
      formParams = (formParams.append('Task', <any>task) as any) || formParams;
    }
    if (content !== undefined) {
      formParams =
        (formParams.append('Content', <any>content) as any) || formParams;
    }

    let responseType: 'text' | 'json' = 'json';
    if (
      httpHeaderAcceptSelected &&
      httpHeaderAcceptSelected.startsWith('text')
    ) {
      responseType = 'text';
    }

    return this._http.post<any>(
      `${apiUrl}/`,
      convertFormParamsToString ? formParams.toString() : formParams,
      {
        responseType: <any>responseType,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress,
      }
    );
  }
}

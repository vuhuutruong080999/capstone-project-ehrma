import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ApiHelper } from '../api-helper';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  public defaultHeaders = new HttpHeaders();
  constructor(private _http: HttpClient) {}

  public onChangePassword(apiUrl: string, model: any): Observable<any> {
    const params = `/${model.userId}/password`;
    const payLoad: any = {
      ...model,
    };

    return this._http
      .put(apiUrl + params, JSON.stringify(payLoad))
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }
  private canConsumeForm(consumes: string[]): boolean {
    const form = 'multipart/form-data';
    for (const consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }
  public avatarPut(
    apiUrl: string,
    requestParameters: any,
    observe: any = 'body',
    reportProgress: boolean = false,
    options?: { httpHeaderAccept?: undefined }
  ): Observable<any> {
    const id = requestParameters.id;
    if (id === null || id === undefined) {
      throw new Error(
        'Required parameter id was null or undefined when calling avatarPut.'
      );
    }
    const avatar = requestParameters.avatar;

    let headers = this.defaultHeaders;

    let httpHeaderAcceptSelected: string | undefined =
      options && options.httpHeaderAccept;
    if (httpHeaderAcceptSelected === undefined) {
      // to determine the Accept header
      httpHeaderAcceptSelected = 'multipart/form-data';
    }
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = ['multipart/form-data'];

    const canConsumeForm = this.canConsumeForm(consumes);

    let formParams: { append(param: string, value: any): any };
    let useForm = false;
    let convertFormParamsToString = false;
    useForm = canConsumeForm;
    formParams = new FormData();

    if (avatar) {
      if (useForm) {
        avatar.forEach((element) => {
          formParams =
            (formParams.append('Avatar', <any>element) as any) || formParams;
        });
      } else {
        formParams =
          (formParams.append('Avatar', avatar.join(',')) as any) || formParams;
      }
    }

    let responseType: 'text' | 'json' = 'json';
    if (
      httpHeaderAcceptSelected &&
      httpHeaderAcceptSelected.startsWith('text')
    ) {
      responseType = 'text';
    }

    return this._http.put<any>(
      `${apiUrl}/${encodeURIComponent(String(id))}/avatar`,
      convertFormParamsToString ? formParams.toString() : formParams,
      {
        responseType: <any>responseType,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress,
      }
    );
  }
}

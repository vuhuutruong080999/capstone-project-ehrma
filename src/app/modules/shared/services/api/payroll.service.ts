import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiHelper } from '../api-helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PayrollService {
  constructor(private _http: HttpClient) {}

  onUpdateStatus(apiUrl: string, status: number): Observable<any> {
    const params = `?status=${status}`;
    return this._http
      .put(apiUrl + params, {})
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }
}

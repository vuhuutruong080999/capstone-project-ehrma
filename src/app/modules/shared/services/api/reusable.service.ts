import { DialogService } from './../client/dialog.service';
import { MessageType } from './../../models/ionic/ionic.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ApiHelper } from '../api-helper';

@Injectable({
  providedIn: 'root',
})
export class ReusableService {
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  public defaultHeaders = new HttpHeaders();
  constructor(
    private _http: HttpClient,
    private _dialogService: DialogService
  ) {}

  onSearch(apiUrl: string, params?: any): Observable<any> {
    let httpParams = new HttpParams();
    if (params) {
      Object.keys(params).forEach((key) => {
        if (
          (params[key] && params[key].toString().trim() !== '') ||
          (params[key] == 0 && params[key].toString().trim() !== '')
        ) {
          httpParams = httpParams.append(key, params[key]);
        }
      });
    }

    return this._http
      .get(apiUrl, {
        params: httpParams,
      })
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }

  onDelete(apiUrl: string, id: any): Observable<any> {
    const params = `/${id}`;
    return this._http
      .delete(apiUrl + params)
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }

  onGetById(apiUrl: string, id: any) {
    const params = `/${id}`;
    return this._http
      .get(apiUrl + params)
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }

  onCreate(apiUrl: string, model: any): Observable<any> {
    const payLoad: any = {
      ...model,
    };
    return this._http
      .post(apiUrl, JSON.stringify(payLoad))
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }

  onUpdate(
    apiUrl: string,
    model?: any,
    paramId: boolean = true
  ): Observable<any> {
    let params = '';

    if (model.id && paramId) {
      params = `/${model.id}`;
    }

    const payLoad: any = Array.isArray(model) ? model : { ...model };

    return this._http
      .put(apiUrl + params, JSON.stringify(payLoad))
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }

  onHandleSuccess(content: string) {
    let data = {
      content: content,
      type: MessageType.Success,
    };
    this._dialogService.onOpenToast(data);
  }
  onHandleFailed(content: string) {
    let data = {
      content: content,
      type: MessageType.Error,
    };
    this._dialogService.onOpenToast(data);
  }
}

import { map, catchError } from 'rxjs/operators';
import { ApiUrl } from './../api-url/api-url';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiHelper } from '../api-helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  onLogin(loginModel: any): Observable<any> {
    let body = JSON.stringify(loginModel);
    return this.http
      .post(ApiUrl.login, body)
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }
}

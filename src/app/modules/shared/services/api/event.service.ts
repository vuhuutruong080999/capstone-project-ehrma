import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  constructor() {}

  rad(x: number) {
    return (x * Math.PI) / 180;
  }

  getDistance(p1, p2) {
    let R = 6378137;
    let dLat = this.rad(p2.lat - p1.lat);
    let dLong = this.rad(p2.lng - p1.lng);
    let a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.lat)) *
        Math.cos(this.rad(p2.lat)) *
        Math.sin(dLong / 2) *
        Math.sin(dLong / 2);
    let c = 2 * Math.asin(Math.sqrt(a));
    let d = (R * c) / 1000; //in km
    return d;
  }
}

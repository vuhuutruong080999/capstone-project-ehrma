import { map, catchError } from 'rxjs/operators';
import { ApiUrl } from './../api-url/api-url';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiHelper } from '../api-helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private _http: HttpClient) {}

  onDelete(apiUrl: string, type: any): Observable<any> {
    const params = `?type=${type}`;
    return this._http
      .delete(apiUrl + params)
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }
}

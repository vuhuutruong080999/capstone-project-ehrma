import { map, catchError } from 'rxjs/operators';
import { ApiUrl } from './../api-url/api-url';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiHelper } from '../api-helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  constructor(private _http: HttpClient) {}

  changeStatus(apiUrl: string): Observable<any> {
    const payLoad = {};

    return this._http
      .put(apiUrl, JSON.stringify(payLoad))
      .pipe(map(ApiHelper.extractData), catchError(ApiHelper.onFail));
  }
}

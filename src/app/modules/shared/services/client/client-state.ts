import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ClientState {
  _isBusy: boolean;

  _isProgressBarVisible: boolean = false;
  progressTimeout: any = null;
  constructor() {}

  public set isProgressBarVisible(value: boolean) {
    if (value) {
      if (this.progressTimeout) {
        clearTimeout(this.progressTimeout);
      }
      this.progressTimeout = setTimeout(() => {
        this._isProgressBarVisible = value;
      }, 1000);
    } else {
      if (this.progressTimeout) {
        clearTimeout(this.progressTimeout);
      }
      this._isProgressBarVisible = value;
    }
  }

  public set isBusy(value: boolean) {
    this._isBusy = value;
  }

  public get isBusy(): boolean {
    return this._isBusy;
  }
}

import { ToastController } from '@ionic/angular';
import { MessageType } from './../../models/ionic/ionic.model';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Router, NavigationStart } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  toast: HTMLIonToastElement;

  constructor(public dialog: MatDialog, private toastCtrl: ToastController) {}

  onOpenToast = async (data) => {
    try {
      this.toast.dismiss();
    } catch (e) {}

    this.toast = await this.toastCtrl.create({
      duration: 3000,
      message: data && data.content,
      color:
        data.type == MessageType.Success
          ? 'success'
          : data.type == MessageType.Warning
          ? 'warning'
          : 'danger',
      position: 'bottom',
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
        },
      ],
    });
    this.toast.present();
  };
}

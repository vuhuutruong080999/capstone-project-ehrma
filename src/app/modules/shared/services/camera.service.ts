import { Injectable } from '@angular/core';
import { Plugins, CameraResultType } from '@capacitor/core';

const { Camera } = Plugins;
@Injectable({
  providedIn: 'root',
})
export class CameraService {
  constructor() {}
  async takePicture(): Promise<File> {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
    });
    this.convertDataUrlToBlob(image.dataUrl);
    const filename = new Date().getTime();
    return new File([this.convertDataUrlToBlob(image.dataUrl)], filename + '', {
      type: `image/jpeg`,
    });
  }
  convertDataUrlToBlob(dataUrl): Blob {
    const arr = dataUrl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new Blob([u8arr], { type: mime });
  }
}

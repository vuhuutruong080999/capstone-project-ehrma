import { ApiError } from './../models/api-response/api-response';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

export module ApiHelper {
  export function extractData(res) {
    let body = res;
    return body;
  }

  export function onFail(err: HttpErrorResponse | any) {
    return throwError(<ApiError>err.error);
  }
}

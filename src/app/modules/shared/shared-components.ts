import { LoadingComponent } from './components/loading/loading.component';
import { ReusableDetailMainComponent } from './components/reusable-detail-main/reusable-detail-main.component';
import { ReusableDetailIconComponent } from './components/reusable-detail-icon/reusable-detail-icon.component';
import { TableIconComponent } from './components/table-icon/table-icon.component';
import { TableDisplayColumnsComponent } from './components/table-display-columns/table-display-columns.component';
import { TableFilterComponent } from './components';

export const SharedComponents = [
  TableFilterComponent,
  TableDisplayColumnsComponent,
  TableIconComponent,
  ReusableDetailIconComponent,
  ReusableDetailMainComponent,
  LoadingComponent,
];

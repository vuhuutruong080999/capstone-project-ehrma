import * as moment from 'moment';

export module Configs {
  //side bar start
  export const sideBars = [
    {
      isShow: true,
      name: 'Dashboard',
      icon: 'home-outline',
      routerlink: '/home',
    },
    {
      name: 'Thông báo',
      isShow: true,
      icon: 'albums-outline',
      routerlink: '/notification',
    },
    {
      name: 'Sự kiện',
      isShow: true,
      icon: 'calendar-outline',
      child: [
        {
          id: '60ed19bb4ae870a306a23c69',
          isShow: false,
          name: 'Quản lý sự kiện',
          routerlink: '/event',
        },
        {
          isShow: true,
          name: 'Danh sách sự kiện',
          routerlink: '/participate-event',
        },
        {
          id: '60f7102ac5b94f153ad72f6e',
          isShow: false,
          name: 'Quản lý điểm danh',
          routerlink: '/attendance-manage',
        },
        {
          isShow: true,
          name: 'Lịch sử điểm danh',
          routerlink: '/attendance-history',
        },
      ],
    },
    {
      name: 'Công việc',
      isShow: true,
      icon: 'clipboard-outline',
      child: [
        {
          isShow: true,

          name: 'Danh sách công việc',
          routerlink: '/task/list',
        },
        {
          isShow: true,

          name: 'Lịch sử công việc',
          routerlink: '/task/my-history',
        },
      ],
    },
    {
      name: 'Nhân sự',
      isShow: true,
      icon: 'people-outline',
      child: [
        {
          isShow: true,
          name: 'Danh sách nhân viên',
          icon: 'body',
          routerlink: '/staff',
        },
        {
          id: '60ed19bb4ae870a306a23c6d',
          isShow: true,
          name: 'Chức vụ',
          routerlink: '/role',
        },
        {
          id: '60ed19bb4ae870a306a23c6b',
          isShow: true,
          name: 'Phòng ban',
          routerlink: '/department',
        },
      ],
    },
    {
      name: 'Lương',
      isShow: true,
      icon: 'cash-outline',
      child: [
        {
          name: 'Lương của tôi',
          isShow: true,
          routerlink: '/my-salary',
        },
        {
          name: 'Danh sách phiếu lương',
          isShow: true,
          routerlink: '/my-payroll',
        },
        {
          id: '60f28b739100db8dc4f5d127',
          name: 'Quản lý phiếu lương',
          isShow: false,
          routerlink: '/payroll/list',
        },
        {
          id: '60f28b739100db8dc4f5d127',
          name: 'Quản lý lương',
          isShow: false,
          routerlink: '/salary',
        },
        {
          id: '60f28b739100db8dc4f5d127',
          name: 'Trợ cấp',
          isShow: false,
          routerlink: '/subsidy',
        },
      ],
    },
    {
      name: 'Yêu cầu',
      isShow: true,
      icon: 'newspaper-outline',
      child: [
        {
          id: '60ed19bb4ae870a306a23c6f',
          isShow: false,
          name: 'Quản lý yêu cầu',
          routerlink: '/request-pending',
        },
        {
          isShow: true,
          name: 'Danh sách yêu cầu',
          routerlink: '/request',
        },
        {
          id: '60ed19bb4ae870a306a23c6f',
          isShow: false,
          name: 'Lịch sử duyệt yêu cầu',
          routerlink: '/request-history',
        },
      ],
    },
    {
      id: '60f28bcd9100db8dc4f5d128',
      isShow: false,
      name: 'Thống kê',
      icon: 'receipt-outline',
      routerlink: '/report-event',
    },
  ];
  //side bar end

  //side bar admin start
  export const sideBarAdmin = [
    {
      isShow: true,
      name: 'Dashboard',
      icon: 'home-outline',
      routerlink: '/home-m',
    },
    {
      isShow: true,
      name: 'Thông báo',
      icon: 'albums-outline',
      routerlink: '/notification',
    },
    {
      isShow: true,
      name: 'Sự kiện',
      icon: 'calendar-outline',
      child: [
        {
          isShow: true,
          name: 'Quản lý sự kiện',
          routerlink: '/event',
        },
        {
          isShow: true,
          name: 'Quản lý điểm danh',
          routerlink: '/attendance-manage',
        },
      ],
    },
    {
      isShow: true,
      name: 'Công việc',
      icon: 'clipboard-outline',
      child: [
        {
          isShow: true,
          name: 'Danh sách công việc',
          routerlink: '/task/list',
        },
        {
          isShow: true,
          name: 'Quản lý công việc',
          routerlink: '/task/history',
        },
      ],
    },

    {
      id: '',
      isShow: true,
      name: 'Nhân sự',
      icon: 'people-outline',
      child: [
        {
          isShow: true,
          name: 'Danh sách nhân viên',
          routerlink: '/staff',
        },
        {
          isShow: true,
          name: 'Chức vụ',
          routerlink: '/role',
        },
        {
          isShow: true,
          name: 'Phòng ban',
          routerlink: '/department',
        },
      ],
    },
    {
      isShow: true,
      name: 'Yêu cầu',
      icon: 'newspaper-outline',
      child: [
        {
          isShow: true,
          name: 'Danh sách yêu cầu',
          routerlink: '/request-pending-a',
        },
        {
          isShow: true,
          name: 'Lịch sử duyệt yêu cầu',
          routerlink: '/request-history-a',
        },
      ],
    },
    {
      isShow: true,
      name: 'Lương',
      icon: 'cash-outline',
      child: [
        {
          name: 'Quản lý phiếu lương',
          isShow: true,
          routerlink: '/payroll/list',
        },
        {
          name: 'Quản lý lương',
          isShow: true,
          routerlink: '/salary',
        },
        {
          name: 'Trợ cấp',
          isShow: true,
          routerlink: '/subsidy',
        },
      ],
    },

    {
      isShow: true,
      name: 'Khác',
      icon: 'receipt-outline',
      child: [
        {
          isShow: true,
          name: 'Thể loại',
          routerlink: '/category',
        },
        {
          isShow: true,
          name: 'Thống kê',
          routerlink: '/report-event',
        },
      ],
    },
  ];
  //side bar admin end

  // feature id
  export const ManageStaffFeatureId = '60f7c21fe1feaf0e36cd1260';
  export const ManageTaskFeatureId = '60fade3dd7dc98034b570e0a';
  export const ManageDepartmentFeatureId = '60ed19bb4ae870a306a23c6b';
  export const ManageRoleFeatureId = '60ed19bb4ae870a306a23c6d';
  export const ManageAttendanceFeatureId = '60f7102ac5b94f153ad72f6e';
  export const ManagePayrollFeatureId = '60f28b739100db8dc4f5d127';
  export const ManageEventFeatureId = '60ed19bb4ae870a306a23c69';
  export const ManageReportEventFeatureId = '60f28bcd9100db8dc4f5d128';
  export const ManageEquipmentFeatureId = '60ed19bb4ae870a306a23c6e';
  export const ManageBudgetFeatureId = '60ed19bb4ae870a306a23c6c';
  export const ManageNotificationFeatureId = '60ed19bb4ae870a306a23c71';
  export const ManageRequestFeatureId = '60ed19bb4ae870a306a23c6f';

  // list faeture for custom role
  export const LIST_FEATURES = [
    {
      label: 'Nhân sự',
      feature: [
        {
          name: 'Quản lý nhân viên',
          type: 'Nhân sự',
          id: '60f7c21fe1feaf0e36cd1260',
          checked: false,
        },
        {
          name: 'Quản lý phòng ban',
          type: 'Nhân sự',
          id: '60ed19bb4ae870a306a23c6b',
          checked: false,
        },
        {
          name: 'Quản lý chức vụ',
          type: 'Nhân sự',
          id: '60ed19bb4ae870a306a23c6d',
          checked: false,
        },
        {
          name: 'Quản lý điểm danh',
          type: 'Nhân sự',
          id: '60f7102ac5b94f153ad72f6e',
          checked: false,
        },

        {
          name: 'Quản lý lương',
          type: 'Nhân sự',
          id: '60f28b739100db8dc4f5d127',
          checked: false,
        },
      ],
    },
    {
      label: 'Sự kiện',
      feature: [
        {
          name: 'Quản lý sự kiện',
          type: 'Sự kiện',
          id: '60ed19bb4ae870a306a23c69',
          checked: false,
        },
        {
          name: 'Xem thống kê sự kiện',
          type: 'Sự kiện',
          id: '60f28bcd9100db8dc4f5d128',
          checked: false,
        },
        {
          name: 'Quản lý trang thiết bị',
          type: 'Sự kiện',
          id: '60ed19bb4ae870a306a23c6e',
          checked: false,
        },
        {
          name: 'Quản lý thu chi',
          type: 'Sự kiện',
          id: '60ed19bb4ae870a306a23c6c',
          checked: false,
        },
      ],
    },
    {
      label: 'Khác',
      feature: [
        {
          name: 'Quản lý thông báo',
          type: 'Khác',
          id: '60ed19bb4ae870a306a23c71',
          checked: false,
        },
        {
          name: 'Quản lý công việc',
          type: 'Khác',
          id: '60fade3dd7dc98034b570e0a',
          checked: false,
        },
        {
          name: 'Duyệt yêu cầu',
          type: 'Khác',
          id: '60ed19bb4ae870a306a23c6f',
          checked: false,
        },
      ],
    },
  ];

  export const status = [
    {
      value: 0,
      label: 'Không hoạt động',
    },
    {
      value: 1,
      label: 'Hoạt động',
    },
  ];

  export const EVENT_STATUS = [
    {
      value: 3,
      label: 'Đang diễn ra',
    },
    {
      value: 2,
      label: 'Sắp diễn ra',
    },
    {
      value: 1,
      label: 'Đã kết thúc',
    },
    {
      value: 0,
      label: 'Huỷ',
    },
  ];

  export const CHECK_OUT_STATUS = [
    {
      value: true,
      label: 'Có',
    },
    {
      value: false,
      label: 'Không',
    },
  ];

  export const ROLE_TYPE = [
    {
      value: 0,
      label: 'Nhân viên thường',
    },
    {
      value: 1,
      label: 'Trưởng nhóm',
    },
    {
      value: 2,
      label: 'Nhân viên quản lý',
    },
  ];

  export const CATEGORY_TYPE = [
    {
      value: 0,
      label: 'Trang thiết bị',
    },
    {
      value: 1,
      label: 'Thu chi',
    },
    {
      value: 2,
      label: 'Yêu cầu',
    },
  ];
  export const PAYROLL_STATUS = [
    {
      value: 0,
      label: 'Chờ xác nhận',
    },
    {
      value: 1,
      label: 'Đã xác nhận',
    },
    {
      value: 2,
      label: 'Đã nhận',
    },
  ];
  export const BUDGET_TYPE = [
    {
      value: true,
      label: 'Chi',
    },
    {
      value: false,
      label: 'Thu',
    },
  ];

  export const REQUEST_STATUS = [
    {
      value: 0,
      label: 'Từ chối',
    },
    {
      value: 1,
      label: 'Đang xủ lý',
    },
    {
      value: 2,
      label: 'Chấp thuận',
    },
  ];

  export const REQUEST_HISTORY_STATUS = [
    {
      value: 0,
      label: 'Từ chối',
    },

    {
      value: 2,
      label: 'Chấp thuận',
    },
  ];
  export const adminRoleId = '60f2976ce3764964bbc6b8e8';
  export const PageIndex = 1;
  export const DefaultPageSize = 10;
  export const PageSizeList = [5, 10, 20, 50];
  export const MobileWidth = 768;

  export const FileMaximumSize = 5; // MB
  export const DivideItemNumber = 6;

  export const FileExtensions = ['docx', 'doc', 'pdf'];
  export const FileExtensionsContainer = [/*'xls',*/ 'xlsx'];
  export const ImageExtensions = ['png', 'jpg', 'gif', 'bmp', 'jpeg'];
  export const AllImageExtensions = 'image/*';
  export const FileMaximunSize = 1; // MB

  export const DistanceCheckIn = 10; //km

  export const GENDERS = [
    { value: 0, label: 'Nam' },
    { value: 1, label: 'Nữ' },
  ];

  const PREFIX: string = '';
  export const DECIMAL_SEPARATOR: string = '.';
  export const THOUSANDS_SEPARATOR: string = '.';
  export const BLANK: string = '';
  const SUFFIX: string = 'đ';
  const PADDING = '000000';

  export function TransformCurrency(
    value: string | number,
    fractionSize: number = 0
  ): string {
    let [integer, fraction = ''] = (value || '').toString().split('.');

    fraction =
      fractionSize > 0
        ? DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
        : '';

    integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, THOUSANDS_SEPARATOR);

    return PREFIX + integer + fraction + SUFFIX;
  }

  export const DATE_FORMATS = {
    parse: {
      dateInput: 'DD/MM/YYYY',
    },
    display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MM YYYY',
      dateA11yLabel: 'DD/MM/YYYY',
      monthYearA11yLabel: 'MM YYYY',
    },
  };

  export const MONTH_FORMAT = {
    parse: {
      dateInput: 'MM/YYYY',
    },
    display: {
      dateInput: 'MM/YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
  };

  export function isValidDate(
    input: any,
    format: string = 'DD/MM/YYYY'
  ): boolean {
    return input instanceof Date || moment(input, format).isValid();
  }

  export function GetDateFormatted(
    dateInput: Date | string,
    format: string = 'DD/MM/YYYY'
  ) {
    return dateInput ? moment(dateInput).format(format) : '';
  }

  export function ParseToDate(
    dateInput: Date | string,
    format: string = 'DD/MM/YYYY'
  ) {
    return dateInput ? moment(dateInput, format).format() : '';
  }

  export function cloneArrayOrObject(
    arrOrObj: Array<any> | Object
  ): Array<any> | Object {
    return JSON.parse(JSON.stringify(arrOrObj));
  }

  export function validateFile(
    file: File,
    acceptFileExtensions: string[],
    maximumSize: number
  ): string {
    if (!file) {
      return 'File không được để trống.';
    }

    if (file.size > maximumSize * 1024 * 1024) {
      return `Kích thước file không được vượt quá ${maximumSize} MB.`;
    }

    if (file.name !== null && file.name !== '' && file.name !== undefined) {
      let fileExt = file.name.replace(/^.*\./, '');
      if (
        acceptFileExtensions &&
        acceptFileExtensions.length == 1 &&
        acceptFileExtensions[0] == AllImageExtensions
      ) {
        var pattern = /image-*/;
        if (!file.type.match(pattern)) {
          return `File cho phép tải lên: ${acceptFileExtensions.toString()}`;
        }
      } else if (
        acceptFileExtensions &&
        acceptFileExtensions.length > 0 &&
        !acceptFileExtensions.some(
          (x) => x.toUpperCase() == fileExt.toUpperCase()
        )
      ) {
        return `File cho phép tải lên: ${acceptFileExtensions.toString()}`;
      }
    }

    return '';
  }

  export function BytesToSize(bytes: number): string {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return 'n/a';
    const i = parseInt(
      Math.floor(Math.log(bytes) / Math.log(1024)).toString(),
      10
    );
    if (i === 0) return `${bytes} ${sizes[i]})`;
    return `${(bytes / 1024 ** i).toFixed(1)} ${sizes[i]}`;
  }

  export const MY_FORMAT = {
    parse: {
      dateInput: 'MM/YYYY',
    },
    display: {
      dateInput: 'MM/YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
  };
}

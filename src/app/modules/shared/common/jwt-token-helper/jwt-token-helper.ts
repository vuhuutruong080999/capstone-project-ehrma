import { LoginComponent } from '@auth/login/login.component';
import * as CryptoJS from 'crypto-js';
import decode from 'jwt-decode';
import { Configs } from '../configs/configs';

export class JwtTokenHelper {
  static base64url = (source: any): any => {
    // Encode in classical base64
    let encodedSource = CryptoJS.enc.Base64.stringify(source);

    // Remove padding equal characters
    encodedSource = encodedSource.replace(/=+$/, '');

    // Replace characters according to base64url specifications
    encodedSource = encodedSource.replace(/\+/g, '-');
    encodedSource = encodedSource.replace(/\//g, '_');

    return encodedSource;
  };

  public static CreateUnsignedToken = (
    data: any,
    expiredTime?: number
  ): string => {
    let header = {
      alg: 'HS256',
      typ: 'JWT',
    };
    let exp = expiredTime
      ? expiredTime
      : Math.floor(Date.now() / 1000) + 60 * 60;
    let stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
    let encodedHeader = JwtTokenHelper.base64url(stringifiedHeader);
    let jwtData = { ...data, exp: exp };
    let stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(jwtData));
    let encodedData = JwtTokenHelper.base64url(stringifiedData);

    let token = encodedHeader + '.' + encodedData;

    return token;
  };

  public static CreateSigningToken = (
    data: any,
    expiredTime?: number
  ): string => {
    let token = JwtTokenHelper.CreateUnsignedToken(data, expiredTime);
    let secret = 'My very confidential secret!';

    let signature = CryptoJS.HmacSHA256(token, secret);
    signature = JwtTokenHelper.base64url(signature);

    let signedToken = token + '.' + signature;
    return signedToken;
  };

  public static DecodeToken = (token: string): any => {
    if (token == null) {
      return null;
    }
    try {
      let tokenPayload = decode(token);

      if (tokenPayload) {
        return tokenPayload;
      }
    } catch (error) {
      return null;
    }
  };

  public static GetUserInfo = () => {
    let userInfoToken = localStorage.getItem('user');
    let userInfo = JwtTokenHelper.DecodeToken(userInfoToken);
    if (userInfo) {
      return { ...userInfo };
    }
    return null;
  };

  public static get avatar(): string | null {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return (userInfo && userInfo.avatar) || null;
  }

  public static get userId(): string | null {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return (userInfo && userInfo.id) || null;
  }

  public static get fullName(): string | null {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return (userInfo && userInfo.fullName) || null;
  }

  public static get departmentId(): string | null {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return (userInfo && userInfo.departmentId) || null;
  }

  public static get roleId(): string | null {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return (userInfo && userInfo.roleId) || null;
  }

  public static get features(): any[] | null {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return (userInfo && userInfo.features) || null;
  }

  public static get level(): any | null {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return (userInfo && userInfo.level) || null;
  }

  public static get isAdmin(): boolean {
    let userInfo = JwtTokenHelper.GetUserInfo();
    return userInfo.roleId == Configs.adminRoleId;
  }

  public static isManage(featureId: string) {
    let userFeatures = JwtTokenHelper.GetUserInfo().features || [];
    if (userFeatures.indexOf(featureId) != -1) {
      return true;
    }
    return false;
  }
}

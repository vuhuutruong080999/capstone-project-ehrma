export class AttendanceModel {
  id: string;
  isCheckIn: boolean = false;
  isCheckOut: boolean = false;
  checkInTime: any;
  checkOutTime: any;
}

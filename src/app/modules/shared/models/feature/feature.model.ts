export interface Feature {
  id?: string;
  name?: string;
  type?: any;
}

export interface Features {
  label: string;
  feature: Feature[];
}

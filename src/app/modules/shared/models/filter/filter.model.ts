import { Configs } from '../../common/configs/configs';

export class FilterBaseModel {
  PageIndex: number = 1;
  PageSize: number = Configs.DefaultPageSize;
  SortColumn?: string;
  SortDirection?: SortDirectionEnum;
}

export enum SortDirectionEnum {
  Ascending = 0,
  Descending = 1,
}

export class FilterNormalModel extends FilterBaseModel {
  TextSearch: string;
}

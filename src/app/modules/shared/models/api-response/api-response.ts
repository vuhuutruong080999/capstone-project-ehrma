export class PagingModel {
  textSearch: string;
  sortColumn: string;
  sortDirection?: string;
  pageIndex: number;
  pageSize: number;
  totalRecord: number;
}

export class ApiListingModel extends PagingModel {
  constructor() {
    super();
  }
  items: Array<any>;
}

export interface ApiError {
  status: number;
  title: string;
  errorMessage: string;
  message: string;
  error: string;
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'calculateTime' })
export class CalculateTimePipe implements PipeTransform {
  transform(checkInTime: any, checkOutTime: any): any {
    let h = new Date(checkInTime).getHours();
    let m = new Date(checkInTime).getMinutes();

    let checkOut = new Date(checkOutTime);
    checkOut.setHours(checkOut.getHours() - h);
    checkOut.setMinutes(checkOut.getMinutes() - m);

    return checkOut;
  }
}

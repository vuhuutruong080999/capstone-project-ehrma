import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'totalTime' })
export class TotalTimePipe implements PipeTransform {
  transform(dateCheckIn: any, dateCheckOut: any): any {
    console.log(dateCheckIn);
    console.log(dateCheckOut);
    let hourCheckIn = new Date(dateCheckIn).getHours();
    let minutesCheckIn = new Date(dateCheckIn).getMinutes();
    let hourCheckOut = new Date(dateCheckOut).getHours();
    let minutesCheckOut = new Date(dateCheckOut).getMinutes();

    let hour = +hourCheckOut - +hourCheckIn;
    let minutes = +minutesCheckOut - +minutesCheckIn;

    return hour + ':' + minutes;
  }
}

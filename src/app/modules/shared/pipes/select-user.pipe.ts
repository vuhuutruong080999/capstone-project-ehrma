import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'selectUser' })
export class SelectUserPipe implements PipeTransform {
  transform(userId: string, userList: any): boolean {
    return userList.findIndex((user) => user.id == userId) != -1;
  }
}

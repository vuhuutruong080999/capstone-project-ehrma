import { OnlyNumber, DecimalDirective } from './directives';

export const SharedDirectives = [OnlyNumber, DecimalDirective];

import { ChangePasswordComponent } from './../change-password/change-password.component';
import { getDataURLFromFile } from 'src/app/modules/layout/utils';
import { ApiUrl } from './../../services/api-url/api-url';
import { UserService } from './../../services/api/user.service';
import { ClientState } from './../../services/client/client-state';
import { ReusableService } from './../../services/api/reusable.service';
import { JwtTokenHelper } from './../../common/jwt-token-helper/jwt-token-helper';
import { ModalController } from '@ionic/angular';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

const acceptedImageTypes = ['image/png', 'image/jpeg'];

@Component({
  selector: 'app-modal-user-info',
  templateUrl: './modal-user-info.component.html',
  styleUrls: ['./modal-user-info.component.scss'],
})
export class ModalUserInfoComponent implements OnInit {
  @ViewChild('imageInput') imageInput: ElementRef<HTMLInputElement>;
  avatarURL: string | ArrayBuffer;
  icon: File;
  model: any = {};
  id = JwtTokenHelper.userId;
  isMobile = false;
  roleLevel = JwtTokenHelper.level;

  constructor(
    private _reusableService: ReusableService,
    private _userService: UserService,
    public clientState: ClientState,
    private _modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.onViewDetail();
  }

  onViewDetail() {
    // this.clientState.isBusy = true;
    this._reusableService.onGetById(ApiUrl.staffGetById, this.id).subscribe(
      (res) => {
        // this.clientState.isBusy = false;
        this.model = res.content;
        this.avatarURL = this.model.avatar;
      },
      (error) => {
        this.clientState.isBusy = false;
        this.onDismissModal();
      }
    );
  }

  async onChangePassword() {
    const modal = await this._modalCtrl.create({
      component: ChangePasswordComponent,
      backdropDismiss: false,

      cssClass: 'modal-change-password',
      componentProps: {
        id: this.id,
      },
    });
    return await modal.present();
  }

  async imageInputChange(event): Promise<void> {
    this.clientState._isBusy = true;
    let files: File = event.target.files;
    const file = files[0];
    if (!acceptedImageTypes.includes(file.type)) {
      return;
    }
    this.icon = file;
    const imageDataURL = await getDataURLFromFile(file);
    this.avatarURL = imageDataURL;
    this.model.avatar = this.icon;
    this.onSubmit();
  }

  onSubmit() {
    this.clientState.isBusy = true;
    const payload = {
      id: this.id,
      avatar: [this.model.avatar],
    };
    this._userService.avatarPut(ApiUrl.staffUpdate, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess(
          'Cập nhật ảnh đại diện thành công'
        );
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
  onDismissModal() {
    this._modalCtrl.dismiss();
  }
}

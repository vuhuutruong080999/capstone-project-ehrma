import { Component, Input } from '@angular/core';

@Component({
  selector: 'reusable-detail-main',
  templateUrl: './reusable-detail-main.component.html',
  styleUrls: ['./reusable-detail-main.component.scss'],
})
export class ReusableDetailMainComponent {
  @Input() labels: string[];

  @Input() data: any[];
}

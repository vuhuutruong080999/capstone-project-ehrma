import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'table-icon',
  templateUrl: './table-icon.component.html',
  styleUrls: ['./table-icon.component.scss'],
})
export class TableIconComponent implements OnInit {
  @Input() element: any;
  @Input() isShowDetail: boolean = true;
  @Input() isShowDelete: boolean = true;
  @Input() isShowUpdate: boolean = true;
  @Input() vercical: boolean = true;
  @Output() onViewDetailEvent = new EventEmitter<any>();
  @Output() onDeleteEvent = new EventEmitter<any>();
  @Output() onUpdateEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  onDelete(element: any) {
    if (this.isShowDelete) {
      this.onDeleteEvent.emit(element);
    }
  }

  onViewDetail(element: any) {
    if (this.isShowDetail) {
      this.onViewDetailEvent.emit(element);
    }
  }

  onUpdate(element: any) {
    if (this.isShowUpdate) {
      this.onUpdateEvent.emit(element);
    }
  }
}

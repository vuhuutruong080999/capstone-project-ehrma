import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-display-columns',
  templateUrl: './table-display-columns.component.html',
})
export class TableDisplayColumnsComponent implements OnInit {
  @Input() tableName: string;
  @Input() baseColumns: any;
  @Input() showColumns: string[];
  @Output() onColumnsChange: EventEmitter<any> = new EventEmitter<any>();

  cols: string[];
  constructor() {}

  ngOnInit(): void {
    this.getTableColumn();
  }

  getTableColumn() {
    let item = localStorage.getItem(this.tableName);
    if (item) {
      this.showColumns = JSON.parse(item);
      this.onColumnsChange.emit(this.showColumns);
    } else {
      this.cols = [];

      this.baseColumns.forEach((element) => {
        this.cols.push(element.id);
      });

      this.showColumns = this.cols;

      this.onColumnsChange.emit(this.showColumns);
    }
  }

  saveTableDisplay(tableName: string, displayColumn: any) {
    localStorage.setItem(tableName, JSON.stringify(displayColumn));
  }

  onChangeColumn(columnName: string, index: number) {
    let i = this.showColumns.indexOf(columnName);
    if (i != -1) {
      this.showColumns.splice(i, 1);
    } else {
      this.showColumns.splice(index, 0, columnName);
    }
    this.saveTableDisplay(this.tableName, this.showColumns);
    this.onColumnsChange.emit(this.showColumns);
  }
}

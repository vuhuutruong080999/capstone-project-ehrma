import { ApiUrl } from './../../services/api-url/api-url';
import { ReusableService } from './../../services/api/reusable.service';
import { UserService } from './../../services/api/user.service';
import { ClientState } from './../../services/client/client-state';
import { Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { ModalController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  @Input() id: number;
  isLoading = false;
  model: any;
  changePasswordForm: FormGroup;
  visibleOldPassword = false;
  visibleNewPassword = false;
  visibleConfirmNewPassword = false;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    private _userService: UserService,
    public clientState: ClientState,
    private alertCtrl: AlertController
  ) {}

  ngOnInit(): void {
    this.changePasswordForm = this._fb.group({
      oldPassword: ['', Validators.required],
      newPassword: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          this.noWhitespaceValidator,
        ],
      ],
      confirmNewPassword: [
        '',
        [Validators.required, this.matchValues('newPassword')],
      ],
    });
  }

  get oldPassword() {
    return this.changePasswordForm.get('oldPassword');
  }

  get newPassword() {
    return this.changePasswordForm.get('newPassword');
  }

  get confirmNewPassword() {
    return this.changePasswordForm.get('confirmNewPassword');
  }

  onSubmit() {
    if (this.changePasswordForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;

    const payload = {
      ...this.changePasswordForm.value,
      userId: this.id,
    };

    this._userService.onChangePassword(ApiUrl.staffSearch, payload).subscribe(
      (res) => {
        this.clientState.isBusy = false;
        this.modalCtrl.dismiss(true);
        this._reusableService.onHandleSuccess('Cập nhật mật khẩu thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  noWhitespaceValidator(control: AbstractControl) {
    if (
      (control.value as string).startsWith(' ') ||
      (control.value as string).endsWith(' ')
    ) {
      return { whitespace: true };
    } else {
      return null;
    }
  }

  matchValues(matchTo: string): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        control.value === control.parent.controls[matchTo].value
        ? null
        : { notMatch: true };
    };
  }

  async ionViewDidEnter() {
    const modal = await this.modalCtrl.getTop();
    const backdrop = modal.querySelector(
      'ion-backdrop'
    ) as HTMLIonBackdropElement;

    backdrop.classList.add('cursor-pointer');

    backdrop.onmousedown = async () => {
      const alert = await this.alertCtrl.create({
        header: 'Xác nhận',
        message: 'Bạn có chắc muốn huỷ thay đổi?',
        buttons: [
          {
            text: 'Huỷ',
            role: 'cancel',
          },
          {
            text: 'Xác nhận',
            handler: () => {
              this.onDismissModal();
            },
          },
        ],
      });
      return await alert.present();
    };
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { animations } from '../../animations/animation';

@Component({
  selector: 'table-filter',
  styleUrls: ['./table-filter.component.scss'],
  templateUrl: './table-filter.component.html',
  animations: animations,
})
export class TableFilterComponent implements OnInit {
  @Input() showSearchField: boolean = false;

  constructor() {}

  ngOnInit() {}

  toggleSearch() {
    this.showSearchField = !this.showSearchField;
  }
}

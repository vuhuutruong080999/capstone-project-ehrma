import { Router } from '@angular/router';
import { ApiUrl } from './../../services/api-url/api-url';
import { ModalController } from '@ionic/angular';
import { ClientState } from './../../services/client/client-state';
import { ReusableService } from './../../services/api/reusable.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal-user-preview',
  templateUrl: './modal-user-preview.component.html',
  styleUrls: ['./modal-user-preview.component.scss'],
})
export class ModalUserPreviewComponent implements OnInit {
  @Input() id: string;
  avatarURL: string | ArrayBuffer;
  icon: File;
  model: any = {};
  isMobile = false;

  constructor(
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private modalCtrl: ModalController,
    private _router: Router
  ) {}

  ngOnInit() {
    // this.clientState.isBusy = true;

    this.onGetStaff();
  }

  onGetStaff() {
    this._reusableService.onGetById(ApiUrl.staffGetById, this.id).subscribe(
      (res) => {
        // this.clientState.isBusy = false;
        this.model = res.content;
        this.avatarURL = this.model.avatar;
      },
      (error) => {
        this.clientState.isBusy = false;
        this.modalCtrl.dismiss();
      }
    );
  }

  onViewDetail() {
    this.modalCtrl.dismiss();

    this._router.navigate(['/staff/detail/' + this.id]);
  }

  onDismissModal() {
    this.modalCtrl.dismiss();
  }
}

import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-loading',
  styleUrls: ['./loading.component.scss'],
  templateUrl: './loading.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class LoadingComponent {
  @Input() show = false;
  @Input() isModal = false;
}

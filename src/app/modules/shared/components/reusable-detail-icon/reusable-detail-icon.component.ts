import { ClientState } from './../../services/client/client-state';
import { ReusableService } from './../../services/api/reusable.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'reusable-detail-icon',
  templateUrl: './reusable-detail-icon.component.html',
})
export class ReusableDetailIconComponent {
  @Input() id: any;
  @Input() goBackUrl: string;
  @Input() deleteUrl: string;
  @Input() deleteParam: string;
  @Input() deleteMsg: string;
  @Input() deleteSuccessMsg: string;
  @Input() isShowBack: boolean = true;
  @Input() isShowDelete: boolean = true;
  @Input() isShowUpdate: boolean = true;
  @Output() onUpdateEvent = new EventEmitter<any>();

  constructor(
    private navCtrl: NavController,
    private _reusableService: ReusableService,
    private _alertCtrl: AlertController,
    private _clientState: ClientState
  ) {}

  onGoBack() {
    this.navCtrl.navigateBack([this.goBackUrl], { animated: false });
  }

  async onDelete() {
    const alert = await this._alertCtrl.create({
      header: 'Xác nhận',
      message: this.deleteMsg,
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this._clientState.isBusy = true;
            this._reusableService.onDelete(this.deleteUrl, this.id).subscribe(
              (res) => {
                this._clientState.isBusy = false;
                this.navCtrl.navigateBack([this.goBackUrl]);
                this._reusableService.onHandleSuccess(this.deleteSuccessMsg);
              },
              (error) => {
                this._clientState.isBusy = false;
              }
            );
          },
        },
      ],
    });
    return await alert.present();
  }

  onUpdate() {
    this.onUpdateEvent.emit();
  }
}

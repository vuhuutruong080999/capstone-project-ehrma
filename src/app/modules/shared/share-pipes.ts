import { SelectUserPipe, TotalTimePipe, CalculateTimePipe } from './pipes';

export const SharedPipes = [SelectUserPipe, TotalTimePipe, CalculateTimePipe];

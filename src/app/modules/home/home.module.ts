import { TaskModule } from './../task/task.module';
import { HomeAttendanceDetailComponent } from './home-attendance-detail/home-attendance-detail.component';
import { HomeAttendanceListComponent } from './home-attendance-list/home-attendance-list.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

@NgModule({
  declarations: [
    HomeComponent,
    HomeAttendanceListComponent,
    HomeAttendanceDetailComponent,
  ],
  imports: [HomeRoutingModule, SharedModule, RoundProgressModule, TaskModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class HomeModule {}

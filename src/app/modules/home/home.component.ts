import { EventService } from './../shared/services/api/event.service';
import { TaskDetailLinkComponent } from './../task/task-detail-link/task-detail-link.component';
import { AlertController, ModalController } from '@ionic/angular';
import { AttendanceModel } from './../shared/models/attendance/attendance.model';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtTokenHelper } from './../shared/common/jwt-token-helper/jwt-token-helper';
import { ApiUrl } from './../shared/services/api-url/api-url';
import { ClientState } from './../shared/services/client/client-state';
import { ReusableService } from './../shared/services/api/reusable.service';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../shared/common/configs/configs';
import { timer } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  userId = JwtTokenHelper.userId;
  eventList = [];
  taskList = [];
  filter: any = {};
  maxiumPage: number;
  isMobile = false;
  eventControl = this._fb.control('');
  eventId: string = '';
  selectedEvent: any;
  completeSubtask: number = 0;
  isCheckIn = false;
  isCheckOut = false;
  attendanceModel: AttendanceModel = {
    id: null,
    isCheckIn: false,
    isCheckOut: false,
    checkInTime: null,
    checkOutTime: null,
  };
  counter = timer(0, 5000);
  clock = '';
  currentTime = null;
  isAdmin = JwtTokenHelper.isAdmin;
  isManageEvent: boolean = JwtTokenHelper.isManage(
    Configs.ManageEventFeatureId
  );
  isToggleAttendance = false;
  hourCheckIn: number;
  myId = JwtTokenHelper.userId;

  constructor(
    private _fb: FormBuilder,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    public modalController: ModalController,
    private _geolocation: Geolocation,
    private alertCtrl: AlertController,
    private _eventService: EventService
  ) {}

  ngOnInit(): void {
    if (this.isAdmin || this.isManageEvent) {
      this._router.navigate(['/home-m']);
      return;
    }
    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
    this.filter.UserId = this.userId;
    this.filter.StatusFilter = 3;
    this.onSearchEvent();
  }

  onSearchEvent(event?) {
    if (event) {
      event.target.complete();
    }
    const payload = { ...this.filter, userId: this.myId };
    this._reusableService.onSearch(ApiUrl.eventSearch, payload).subscribe(
      (res) => {
        let { events, ...paging } = res.content;
        this.maxiumPage = Math.ceil(
          paging.totalRecord / Configs.DefaultPageSize
        );
        if (window.innerWidth < Configs.MobileWidth) {
          if (this.filter.PageIndex == 1) {
            this.eventList = events;
          } else {
            this.eventList = this.eventList.concat(events);
          }
        } else {
          this.eventList = events;
        }
        //set selected first item
        this.selectedEvent = this.eventList[0];

        //check attendance

        if (
          this.selectedEvent.status == 3 &&
          this.selectedEvent.users.findIndex(
            (user) => user.id == this.userId
          ) != -1
        ) {
          this.isToggleAttendance = true;
          this.onCheckAttendance();
        }
        this.onSearchTask();
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onCheckAttendance() {
    this._reusableService.onSearch(ApiUrl.attendanceTodayGet).subscribe(
      (res) => {
        this.attendanceModel.id = res.content.id;
        this.attendanceModel.isCheckIn = res.content.isCheckIn;
        this.attendanceModel.isCheckOut = res.content.isCheckOut;
        this.attendanceModel.checkInTime = res.content.checkInTime;
        this.attendanceModel.checkOutTime = res.content.checkOutTime;

        this.hourCheckIn = new Date(
          this.attendanceModel.checkInTime
        ).getHours();

        if (
          this.attendanceModel.isCheckIn &&
          !this.attendanceModel.isCheckOut
        ) {
          let h = new Date(this.attendanceModel.checkInTime).getHours();
          let m = new Date(this.attendanceModel.checkInTime).getMinutes();

          this.counter.subscribe(() => {
            this.onGetTime(h, m);
          });
        }
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onGetTime(h, m) {
    this.currentTime = new Date();
    this.currentTime.setHours(this.currentTime.getHours() - h);
    this.currentTime.setMinutes(this.currentTime.getMinutes() - m);
    let hour = this.currentTime.getHours();
    let minutes = this.currentTime.getMinutes();

    if (+hour < 10) {
      hour = '0' + hour;
    }
    if (+minutes < 10) {
      minutes = '0' + minutes;
    }

    this.clock = hour + ':' + minutes;
  }

  onSearchTask() {
    const payload = {
      eventNullable: false,
      EventId: this.selectedEvent.id,
      PageIndex: 1,
      PageSize: 10,
      status: 2,
      staffId: this.userId,
    };

    this._reusableService.onSearch(ApiUrl.taskApi, payload).subscribe(
      (res) => {
        this.taskList = res.content.tasks;
        this.taskList.forEach((task) => {
          task.completeTask = task.subTasks.filter((sub) => sub.status).length;
        });

        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  onChangeEvent(event: any) {
    this.selectedEvent = event;
    this.onSearchTask();
  }

  onViewEvent() {
    this._router.navigate(
      ['/participate-event/detail/' + this.selectedEvent.id],
      {
        state: this.selectedEvent,
      }
    );
  }

  onViewAttendance() {
    this._router.navigate([
      '/attendance-history/detail/' + this.selectedEvent.id,
    ]);
  }

  getColorTask(completeTask: number, subTask: number) {
    const persent = (completeTask / subTask) * 100;
    if (persent <= 50) {
      return '#E86969';
    } else if (persent > 50 && persent <= 75) {
      return '#FEE281';
    } else {
      return '#A1E26D';
    }
  }

  calculateTotalTime() {
    let h = new Date(this.attendanceModel.checkInTime).getHours();
    let m = new Date(this.attendanceModel.checkInTime).getMinutes();

    let checkOutTime = new Date(this.attendanceModel.checkOutTime);
    checkOutTime.setHours(checkOutTime.getHours() - h);
    checkOutTime.setMinutes(checkOutTime.getMinutes() - m);

    return checkOutTime;
  }

  onViewTaskDetail(id: string) {
    this._router.navigate(['/task/processing/detail/' + id]);
  }

  async onClickAttendance() {
    const alert = await this.alertCtrl.create({
      header: 'Xác nhận',
      message: this.attendanceModel.isCheckIn
        ? 'Bạn có chắc muốn tan ca? '
        : 'Bạn có chắc muốn vào ca? ',
      buttons: [
        {
          text: 'Huỷ',
          role: 'cancel',
        },
        {
          text: 'Xác nhận',
          handler: () => {
            this.onProcessAttendance();
          },
        },
      ],
    });
    return await alert.present();
    //ahihi
  }

  onProcessAttendance() {
    this.clientState.isBusy = true;
    this._geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
      })
      .then((res) => {
        //current location
        const p1 = {
          lat: res.coords.latitude,
          lng: res.coords.longitude,
        };

        this._reusableService
          .onSearch(ApiUrl.eventLocations + `/${this.userId}/locations`, {
            userId: this.userId,
          })
          .subscribe(
            (res) => {
              let isValidRange = false;

              res.content.locations.forEach((item) => {
                const location = item.split(',');
                const p2 = {
                  lat: +location[0],
                  lng: +location[1],
                };
                const distance = this._eventService.getDistance(p1, p2);

                if (distance < Configs.DistanceCheckIn) {
                  isValidRange = true;
                  return;
                }
              });

              if (isValidRange) {
                if (
                  !this.attendanceModel.isCheckIn &&
                  !this.attendanceModel.isCheckOut
                ) {
                  this.onCheckIn(p1);
                } else {
                  this.onCheckOut(p1);
                }
              } else {
                this.clientState.isBusy = false;
                this._reusableService.onHandleFailed('Điểm danh thất bại.');
              }
            },
            (error) => {
              this.clientState.isBusy = false;
            }
          );
      })
      .catch((error) => {
        let message = '';
        if (error.code == 1) {
          message =
            'Không lấy được vị trí hiện tại, vui lòng cho phép ứng dụng truy cập vị trí.';
        } else if (
          !this.attendanceModel.isCheckIn &&
          !this.attendanceModel.isCheckOut
        ) {
          message = 'Vào ca thất bại, vui lòng thử lại sau.';
        } else if (this.attendanceModel.isCheckOut) {
          message = 'Tan ca thất bại, vui lòng thử lại sau.';
        }

        this._reusableService.onHandleFailed(message);

        this.clientState.isBusy = false;
      });
  }

  onCheckIn(location: any) {
    const payload = {
      userId: this.userId,
      eventId: this.selectedEvent.id,
      optionCheckIn: location.lat + ',' + location.lng,
    };

    this._reusableService.onCreate(ApiUrl.attendanceCreate, payload).subscribe(
      (res) => {
        // this.attendanceModel.id = res.content.id;
        // this.attendanceModel.isCheckIn = true;
        // this.attendanceModel.checkInTime = res.content.checkInDateTime;
        this.onCheckAttendance();
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess('Vào ca thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }

  async showTask(taskId: string) {
    this.clientState._isBusy = true;
    const modal = await this.modalController.create({
      component: TaskDetailLinkComponent,
      cssClass: 'modal-task',
      componentProps: {
        id: taskId,
        isHome: true,
      },
    });
    modal.onDidDismiss().then(() => {
      this.onSearchTask();
    });
    return await modal.present();
  }
  onCheckOut(location: any) {
    const payload = {
      id: this.attendanceModel.id,
      optionCheckOut: location.lat + ',' + location.lng,
    };

    this._reusableService.onUpdate(ApiUrl.attendancePut, payload).subscribe(
      (res) => {
        this.attendanceModel.isCheckOut = true;
        this.attendanceModel.checkOutTime = res.content.checkOutDateTime;
        this.clientState.isBusy = false;
        this._reusableService.onHandleSuccess('Tan ca thành công');
      },
      (error) => {
        this.clientState.isBusy = false;
      }
    );
  }
}

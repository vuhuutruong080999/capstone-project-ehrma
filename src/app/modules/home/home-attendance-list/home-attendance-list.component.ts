import { IonInfiniteScroll } from '@ionic/angular';
import { MatPaginator } from '@angular/material/paginator';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ClientState } from './../../shared/services/client/client-state';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PagingModel } from './../../shared/models/api-response/api-response';
import { MatTableDataSource } from '@angular/material/table';
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ChangeDetectorRef,
  ViewChild,
  AfterContentChecked,
} from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-home-attendance-list',
  templateUrl: './home-attendance-list.component.html',
  styleUrls: ['./home-attendance-list.component.scss'],
})
export class HomeAttendanceListComponent
  implements OnInit, OnChanges, AfterContentChecked {
  @Input() eventId: string;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(MatPaginator, { static: false }) set paginator(
    content: MatPaginator
  ) {
    if (content) {
      content._intl.itemsPerPageLabel = '';
    }
  }

  displayedColumns: any[] = [{ id: 'name', name: 'Tên nhân viên' }];
  filter: any = {};
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  pageSizeList = Configs.PageSizeList;
  pagingModel = new PagingModel();
  searchForm: FormGroup;
  isMobile = false;
  showColumns: any[] = [];
  maxiumPage: number;
  userAttendances: any = [];
  listDate: any = [];
  isDoneInit = false;
  isFilter = false;

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    public clientState: ClientState,
    private _reusableService: ReusableService,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }
    this.createSearchForm();

    this.filter.PageIndex = 1;
    this.filter.PageSize = Configs.DefaultPageSize;
  }

  ngOnChanges() {
    if (this.eventId) {
      this.onResetInit();
      this.onGetAttendance();
    }
  }

  onResetInit() {
    if (localStorage.getItem('attendance-home')) {
      localStorage.removeItem('attendance-home');
    }

    this.listDate = [];

    this.displayedColumns = [{ id: 'name', name: 'Tên nhân viên' }];
    this.showColumns = [];

    this.isDoneInit = false;
  }

  createSearchForm() {
    this.searchForm = this._fb.group({
      textSearch: [''],
    });
  }
  ngAfterContentChecked() {
    this._cd.detectChanges();
  }

  onGetAttendance(event?) {
    if (event) {
      event.target.complete();
    }

    this.filter.eventId = this.eventId;
    this._reusableService
      .onSearch(ApiUrl.attendanceEventGet + `/${this.eventId}`, this.filter)
      .subscribe(
        (res) => {
          let { userAttendances, ...paging } = res.content;
          this.userAttendances = userAttendances;

          this.maxiumPage = Math.ceil(
            paging.totalRecord / Configs.DefaultPageSize
          );
          if (window.innerWidth < Configs.MobileWidth) {
            if (this.filter.PageIndex == 1) {
              this.dataSource.data = userAttendances;
            } else {
              this.dataSource.data = this.dataSource.data.concat(
                userAttendances
              );
            }
          } else {
            this.dataSource.data = userAttendances;
          }

          if (userAttendances.length && !this.isFilter) {
            this.getDateColumn();
          } else {
            this.isDoneInit = true;
          }

          this.pagingModel = paging;

          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }

  getDateColumn() {
    for (const [key, value] of Object.entries(
      this.userAttendances[0].attendances
    )) {
      this.listDate.push({ id: key, name: key });
      this.displayedColumns.push({ id: key, name: key });
    }

    this.isDoneInit = true;
  }

  onGetValueDate(attendances: any, index: number): any {
    return Object.values(attendances)[index];
  }

  onFilter(event?: any) {
    this.isFilter = true;
    this.paginator.pageIndex = 0;
    this.filter.PageIndex = 1;

    if (event) {
      this.filter.TextSearch = event.detail.value;
    } else {
      // this.clientState.isBusy = true;
      this.filter.TextSearch = this.searchForm.value.textSearch;
    }

    this.onGetAttendance();
  }

  onResetFilter() {
    this.clientState.isBusy = true;
    // this.infiniteScroll.disabled = false;
    this.searchForm.reset();
    this.onFilter();
  }

  pageChange(event: any) {
    this.clientState.isBusy = true;
    this.filter.PageIndex = event.pageIndex + 1;
    this.filter.PageSize = event.pageSize;
    this.onGetAttendance();
  }

  loadMoreData(event) {}

  onViewDetail(row: any) {
    let isCheckIn = false;
    for (const [key, value] of Object.entries(row.attendances)) {
      if (value != 0) {
        isCheckIn = true;
        break;
      }
    }
    if (isCheckIn) {
      this._router.navigate([`/home/attendance-detail/${row.id}`]);
    } else {
      this._reusableService.onHandleFailed('Không tồn tại điểm danh nào');
    }
  }

  ngOnDestroy() {
    localStorage.removeItem('attendance-manage-detail');
  }
}

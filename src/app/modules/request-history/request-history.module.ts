import { SharedModule } from './../shared/shared.module';
import { RequestHistoryDetailComponent } from './request-history-detail/request-history-detail.component';
import { RequestHistoryListComponent } from './request-history-list/request-history-list.component';
import { NgModule } from '@angular/core';

import { RequestHistoryRoutingModule } from './request-history-routing.module';

@NgModule({
  declarations: [RequestHistoryListComponent, RequestHistoryDetailComponent],
  imports: [SharedModule, RequestHistoryRoutingModule],
})
export class RequestHistoryModule {}

import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientState } from './../../shared/services/client/client-state';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';

@Component({
  selector: 'app-request-history-detail',
  templateUrl: './request-history-detail.component.html',
  styleUrls: ['./request-history-detail.component.scss'],
})
export class RequestHistoryDetailComponent implements OnInit {
  model: any = {};
  isMobile = false;
  labels = ['Người yêu cầu', 'Trạng thái', 'Tiêu đề', 'Loại đơn', 'Ngày gửi'];
  id: any;

  constructor(
    private _route: ActivatedRoute,
    private _reusableService: ReusableService,
    public clientState: ClientState,
    private _router: Router,
    private _navCtrl: NavController
  ) {}

  ngOnInit() {
    if (window.innerWidth < Configs.MobileWidth) {
      this.isMobile = true;
    }

    if (this._router.getCurrentNavigation().extras.state) {
      this.model = this._router.getCurrentNavigation().extras.state;
    } else {
      this.clientState.isBusy = true;
      this._route.params.subscribe((params) => {
        this.id = params['id'];
      });

      this.onViewDetail();
    }
  }

  doRefresh(event) {
    this.onViewDetail();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  onViewDetail() {
    this._reusableService.onGetById(ApiUrl.requestGetById, this.id).subscribe(
      (res) => {
        this.model = res.content;
        this.clientState.isBusy = false;
      },
      (error) => {
        this.clientState.isBusy = false;
        this._router.navigate(['/request-history/list']);
      }
    );
  }

  onBack() {
    this._navCtrl.navigateBack('/request-history/list', { animated: false });
  }
}

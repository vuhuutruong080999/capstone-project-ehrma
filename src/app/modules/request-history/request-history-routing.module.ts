import { RequestHistoryDetailComponent } from './request-history-detail/request-history-detail.component';
import { RequestHistoryListComponent } from './request-history-list/request-history-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },

  {
    path: 'list',
    component: RequestHistoryListComponent,
  },
  {
    path: 'detail/:id',
    component: RequestHistoryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestHistoryRoutingModule {}

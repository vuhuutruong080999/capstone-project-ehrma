import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientState } from './../../shared/services/client/client-state';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  phone: string;
  formGroup: FormGroup;
  loginForm: any;
  isLoginError: boolean;
  isLoading: boolean = true;
  returnUrl: string;

  isError: boolean;
  isSuccess: boolean;
  successMessage: string = '';
  errorMessage: string = '';
  messages = {
    success:
      'Mật khẩu mới đã được gửi! Vui lòng kiểm tra số điện thoại đăng kí với tài khoản.',
    unknownError: 'Đã có lỗi xảy ra, vui lòng thử lại.',
  };

  constructor(
    private _route: ActivatedRoute,
    public clientState: ClientState,
    private _reusableService: ReusableService
  ) {}

  ngOnInit(): void {
    this._route.queryParams.subscribe((params) => {
      this.phone = params['phone'];
      if (this.phone) {
        this.onSendPassword();
      } else {
        this.isError = true;
        this.errorMessage = this.messages.unknownError;
      }
    });
  }

  onSendPassword() {
    this.clientState.isBusy = true;

    this._reusableService
      .onSearch(ApiUrl.defaultPassword, { phone: this.phone })
      .subscribe(
        (res) => {
          this.isSuccess = true;
          this.successMessage = this.messages.success;
          this.clientState.isBusy = false;
        },
        (error) => {
          this.isError = true;
          this.errorMessage = this.messages.unknownError;
          this.clientState.isBusy = false;
        }
      );
  }
}

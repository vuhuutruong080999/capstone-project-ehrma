import { ModalController } from '@ionic/angular';
import { ClientState } from './../../shared/services/client/client-state';
import { JwtTokenHelper } from './../../shared/common/jwt-token-helper/jwt-token-helper';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../shared/services/api/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Configs } from '../../shared/common/configs/configs';
import { ActiveAccountComponent } from '@auth/active-account/active-account.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  loginForm: any;
  isLoginError: boolean;
  errorMessage: string = '';
  isLoading: boolean = true;
  returnUrl: string;
  messageResetPassword: string = '';
  visiblePassword = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _router: Router,
    private _route: ActivatedRoute,
    public clientState: ClientState,
    private _modalCtrl: ModalController
  ) {
    if (this._router.getCurrentNavigation().extras.state) {
      this.messageResetPassword = this._router.getCurrentNavigation().extras.state.success;
    }
  }

  ngOnInit(): void {
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

    this.loginForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onLogin() {
    if (this.loginForm.invalid) {
      return;
    }

    this.clientState.isBusy = true;

    this._authService.onLogin(this.loginForm.value).subscribe(
      (res) => {
        if (res && res.content.token) {
          //storage token
          this.onStorageToken(res.content.token);
          const userId = JwtTokenHelper.userId;
          const userRole = JwtTokenHelper.roleId;

          if (this.returnUrl == '/') {
            if (userRole == Configs.adminRoleId) {
              this.clientState.isBusy = false;
              this._router.navigate(['/home-m']);
              return;
            }
          } //end navigate home-m when admin

          //change password first time
          if (!res.content.verifyAccount) {
            this.onChangePassword(userId);
          }
          this.clientState.isBusy = false;
          this._router.navigate([this.returnUrl]);
        } else {
          this.isLoginError = true;
        }
      },
      (error) => {
        this.isLoginError = true;
        this.errorMessage = error.message;
        this.clientState.isBusy = false;
      }
    );
  }

  onStorageToken(token: string) {
    let tokenInfo = JwtTokenHelper.DecodeToken(token);
    let userInfo = {
      id: tokenInfo.id,
      level: tokenInfo.roleLevel,
      fullName: tokenInfo.fullName,
      avatar: tokenInfo.avatar,
      departmentName: tokenInfo.departmentName,
      departmentId: tokenInfo.departmentId,
      roleId: tokenInfo.roleId,
      roleName: tokenInfo.roleName,
      roleLevel: tokenInfo.roleLevel,
      features: tokenInfo.features,
    };

    localStorage.setItem('user', JwtTokenHelper.CreateSigningToken(userInfo));
    localStorage.setItem('token', token);
  }

  async onChangePassword(id: string) {
    const modal = await this._modalCtrl.create({
      component: ActiveAccountComponent,
      cssClass: 'modal-active-account',
      componentProps: {
        id: id,
      },
    });
    return await modal.present();
  }
}

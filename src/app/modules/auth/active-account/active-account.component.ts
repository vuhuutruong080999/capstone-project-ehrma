import { ApiUrl } from './../../shared/services/api-url/api-url';
import { Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ReusableService } from '../../shared/services/api/reusable.service';
import { ClientState } from '../../shared/services/client/client-state';
import { UserService } from '../../shared/services/api/user.service';

@Component({
  selector: 'app-active-account',
  templateUrl: './active-account.component.html',
  styleUrls: ['./active-account.component.scss'],
})
export class ActiveAccountComponent implements OnInit {
  @Input() id: string;
  isLoading = false;
  model: any;
  changePasswordForm: FormGroup;
  visibleOldPassword = false;
  visibleNewPassword = false;
  visibleConfirmNewPassword = false;

  constructor(
    private _fb: FormBuilder,
    public modalCtrl: ModalController,
    private _reusableService: ReusableService,
    public clientState: ClientState
  ) {}

  ngOnInit(): void {
    this.changePasswordForm = this._fb.group({
      newPassword: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          this.noWhitespaceValidator,
        ],
      ],
      confirmNewPassword: [
        '',
        [Validators.required, this.matchValues('newPassword')],
      ],
    });
  }

  get newPassword() {
    return this.changePasswordForm.get('newPassword');
  }

  get confirmNewPassword() {
    return this.changePasswordForm.get('confirmNewPassword');
  }

  onSubmit() {
    if (this.changePasswordForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;

    this._reusableService
      .onUpdate(ApiUrl.staffActiveAccount + `/${this.id}/active-account`, {
        password: this.newPassword.value,
      })
      .subscribe(
        (res) => {
          this.modalCtrl.dismiss(true);
          this._reusableService.onHandleSuccess('Cập nhật mật khẩu thành công');
          this.clientState.isBusy = false;
        },
        (error) => {
          this.clientState.isBusy = false;
        }
      );
  }
  onDismissModal() {
    this.modalCtrl.dismiss();
  }

  noWhitespaceValidator(control: AbstractControl) {
    if (
      (control.value as string).startsWith(' ') ||
      (control.value as string).endsWith(' ')
    ) {
      return { whitespace: true };
    } else {
      return null;
    }
  }

  matchValues(matchTo: string): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        control.value === control.parent.controls[matchTo].value
        ? null
        : { notMatch: true };
    };
  }
}

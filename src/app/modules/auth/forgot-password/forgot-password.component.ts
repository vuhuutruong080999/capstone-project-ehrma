import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiUrl } from './../../shared/services/api-url/api-url';
import { ReusableService } from './../../shared/services/api/reusable.service';
import { ClientState } from './../../shared/services/client/client-state';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  formGroup: FormGroup;
  forgotPasswordForm: any;
  isError: boolean;
  isSuccess: boolean;
  errorMessage: string = null;
  successMessage: string = null;
  messages = {
    success:
      'Mã xác nhận đã được gửi! Vui lòng kiểm tra số điện thoại đăng kí với tài khoản.',
    unknownError: 'Đã có lỗi xảy ra, vui lòng thử lại.',
  };

  constructor(
    private _formBuilder: FormBuilder,
    public clientState: ClientState,
    private _reusableService: ReusableService,
    private _navCtrl: NavController,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.forgotPasswordForm = this._formBuilder.group({
      phone: ['', [Validators.required]],
    });
  }

  get phone() {
    return this.forgotPasswordForm.get('phone');
  }

  onForgotPassword() {
    if (this.forgotPasswordForm.invalid) {
      return;
    }
    this.clientState.isBusy = true;

    this._reusableService
      .onCreate(ApiUrl.forgetPassword, this.forgotPasswordForm.value)
      .subscribe(
        (res) => {
          this.isError = false;

          this.isSuccess = true;
          this.clientState.isBusy = false;
          this._router.navigate(['/auth/login'], {
            state: this.messages,
          });
        },
        (error) => {
          this.isError = true;
          this.errorMessage = this.messages.unknownError;
          this.clientState.isBusy = false;
        }
      );
  }

  onBack() {
    this._navCtrl.navigateBack('/auth/login/', {
      animated: false,
    });
  }
}

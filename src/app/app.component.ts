import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { Router } from '@angular/router';

import { Plugins, StatusBarStyle, Capacitor } from '@capacitor/core';
import { Component, NgZone } from '@angular/core';
import { Platform } from '@ionic/angular';
const { StatusBar } = Plugins;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private deepLinks: Deeplinks,
    private zone: NgZone,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (Capacitor.isPluginAvailable('StatusBar')) {
        StatusBar.setStyle({ style: StatusBarStyle.Light });
        StatusBar.setBackgroundColor({ color: '#ffffff' });
      }
      if (this.platform.is('cordova')) {
        this.onSetUpDeepLinks();
      }
    });
  }

  onSetUpDeepLinks() {
    this.deepLinks
      .routeWithNavController(this.router, {
        '/request-pending/detail/:id': '',
        '/event/detail/:id': '',
        '/auth/reset-password/:id': '',
        '/task/detail/:id': '',
      })
      .subscribe((match) => {
        this.zone.run(() => {
          this.router.navigateByUrl(match.$link.path);
        });
      });
  }
}

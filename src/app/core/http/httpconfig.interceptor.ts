import { AuthenticationService } from './../authentication/authentication.service';
import { MessageType } from './../../modules/shared/models/ionic/ionic.model';
import { DialogService } from './../../modules/shared/services/client/dialog.service';
import { ApiError } from './../../modules/shared/models/api-response/api-response';
import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError, shareReplay } from 'rxjs/operators';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(
    private _dialogService: DialogService,
    private authenticationService: AuthenticationService
  ) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem('token');

    if (this.authenticationService.isAuthenticated()) {
      request = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + token),
      });
    }

    if (
      !request.headers.has('Content-Type') &&
      !(request.body instanceof FormData)
    ) {
      request = request.clone({
        headers: request.headers.set('Content-Type', 'application/json'),
      });
    }

    request = request.clone({
      headers: request.headers.set('Accept', 'application/json'),
    });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        const apiError = <ApiError>error.error;
        if (
          ((apiError && apiError.message) ||
            (apiError && apiError.title) ||
            error.status <= 0) &&
          !error.url.includes('authentication')
        ) {
          const data = {
            content:
              apiError.message ||
              apiError.title ||
              'Máy chủ đang bảo trì, vui lòng thử lại sau',
            type: MessageType.Error,
          };
          this._dialogService.onOpenToast(data);
        }
        return throwError(error);
      })
    );
  }
}

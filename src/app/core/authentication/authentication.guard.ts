import { AuthenticationService } from './authentication.service';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationGuard implements CanActivate {
  constructor(private router: Router, private auth: AuthenticationService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!this.auth.isAuthenticated()) {
      localStorage.clear();
      this.router.navigate(['/auth'], {
        queryParams: { returnUrl: state.url },
        replaceUrl: true,
      });
      return false;
    }
    return true;
  }
}

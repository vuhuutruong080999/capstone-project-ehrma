import { AuthenticationService } from './authentication.service';
import { JwtTokenHelper } from './../../modules/shared/common/jwt-token-helper/jwt-token-helper';
import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(public router: Router, private auth: AuthenticationService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const token = localStorage.getItem('token');
    if (token == null || !this.auth.isAuthenticated()) {
      this.onRemoveToken();
      this.router.navigate(['/auth'], {
        queryParams: { returnUrl: state.url },
      });
      return;
    }

    const expectedId = <Array<any>>route.data.expectedId || [];
    const expectedFeatures = <Array<any>>route.data.expectedFeatures || [];

    const userRole = JwtTokenHelper.roleId;

    if (expectedId.some((item) => item == userRole)) {
      return true;
    } else {
      const features = JwtTokenHelper.features || [];
      if (!expectedFeatures.some((f) => features.indexOf(f) != -1)) {
        // if (state.url.includes('home')) {
        //   this.router.navigate(['/notification/list']);
        //   return false;
        // }
        this.router.navigate(['access-denied'], {
          queryParams: { returnUrl: state.url },
        });
        return false;
      }
    }
    return true;
  }

  onRemoveToken = () => {
    localStorage.clear();
  };
}

import { JwtHelperService } from '@auth0/angular-jwt';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private _redirectUrl: string;

  constructor(public jwtHelper: JwtHelperService) {}

  public isAuthenticated = (): boolean => {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  };

  public set redirectUrl(value: string) {
    this._redirectUrl = value;
  }

  public get redirectUrl(): string {
    return this._redirectUrl;
  }
}
